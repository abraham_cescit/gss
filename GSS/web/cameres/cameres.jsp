<%-- 
    Document   : cameres
    Created on : 23-sep-2020, 11:00:48
    Author     : Abraham M
--%>

<%@page import="com.cescit.security.Rols"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="util.commons.Utils"%>
<%@page import="controladors.base.Idioma"%>
<%@page import="java.util.ArrayList" %>
<%@page import="models.*" %>
<%@page import="models.services.*" %>
<% Idioma idioma = (Idioma) request.getAttribute("idioma");%>
<!--API VIDEOLOGIC-->
<% String urlAPI = Utils.getProperty("URL");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script>
    
    // =======================================
    // VARIABLES GLOBALS
    // =======================================
    
    var principalLayout, gridCameres;
    var formCamera, winVideo;
    
    // CONTROLADORS
    var URL_DESA_CAMERA = "camera-insert";
    var URL_BUSCAR_CAMERA = "cameres-get";
    var URL_ESBORRA_CAMERA = "camera-delete";
    
    // =======================================
    // DEFINICIÓN FORMULARIOS
    // =======================================
    var contentFormEdicio = [
        {type: "settings", position: "label-left", labelWidth: WIDTH_LABEL_FORM, inputWidth: "auto", offsetLeft: OFFSET_LEFT_FORM},
        {type: "hidden", name: "idCamera", label: "<%=idioma.get("ID_CAMERA")%>", inputWidth: INPUT_BIG},
        {type: "input", name: "nom", label: "<%=idioma.get("NOM")%>", inputWidth: INPUT_BIG, readonly: true},
        {type: "input", name: "posicio", label: "<%=idioma.get("POSICIO")%>", inputWidth: INPUT_BIG, required: true},
        {type: "input", name: "descripcio", label: "<%=idioma.get("DESCRIPCIO")%>", inputWidth: INPUT_BIG, required: true},
        {type: "input", name: "marca", label: "<%=idioma.get("MARCA")%>", inputWidth: INPUT_BIG, required: true},
        {type: "input", name: "model", label: "<%=idioma.get("MODEL")%>", inputWidth: INPUT_BIG, required: true},
        {type: "input", name: "protocol", label: "<%=idioma.get("PROTOCOL")%>", inputWidth: INPUT_BIG, required: true},
        {type: "button", name: "desa", value: "<%=idioma.get("DESA")%>", offsetTop: OFFSET_LEFT_FORM}
    ];
    
    var contentFormCamera = [
        {type: "settings", position: "label-left", labelWidth: "auto", inputWidth: "auto", offsetLeft: 20},
        {type: "container", name:"contenidorVideo", inputWidth: 640, inputHeight: 480}
    ];
    
    // =======================================
    // DEFINICI�N GRID
    // =======================================
    var contentGridVehicles = {
        image_path: "codebase-pro/imgs/",
        columns: [
            {label: "<%= idioma.get("ID_CAMERA")%>", id: "idCamera", width: '80', type:"ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("NOM")%>", id: "nom", width: '100', type:"ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("POSICIO")%>", id: "posicio", width: '60', type:"ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("DESCRIPCIO")%>", id: "descripcio", width: '*', type:"ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("MARCA")%>", id: "marca", width: '90', type:"ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("MODEL")%>", id: "model", width: '90', type:"ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("PROTOCOL")%>", id: "protocol", width: '*', type:"ro", sort: "str", align: "left"}
        ],
        multiselect: false
    };

    function doOnLoad() {
        principalLayout = new dhtmlXLayoutObject({
            parent: "layoutCameres",
            pattern: "2U",
            cells: [
                {id: "a", text: "<%= idioma.get("LLISTAT_DE_CAMERES")%>"},
                {id: "b", text: "<%= idioma.get("EDICIO_DE_CAMERA")%>"}
            ]
        });
        
        principalLayout.cells("b").setWidth(350);
        
        gridCameres = principalLayout.cells("a").attachGrid(contentGridVehicles);
        gridCameres.attachHeader("#text_filter,#text_filter,#combo_filter,#text_filter,#combo_filter,#text_filter,#text_filter");
        gridCameres.attachEvent("onRowDblClicked",doOnRowDblClicked);
        gridCameres.setColumnHidden(0, true);
        gridCameres.load(URL_BUSCAR_CAMERA);
        inicialitzarGrid(gridCameres);
        formCamera = principalLayout.cells("b").attachForm();
        formCamera.loadStruct(contentFormEdicio);
        
        function doOnRowDblClicked(rowId){
            var proto = gridCameres.cells(rowId, gridCameres.getColIndexById("protocol")).getValue();
            getImatgeCamera(proto);
        }
        
        function getImatgeCamera(camera) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var opt = this.response;
                    if (opt == "ERROR") {
                        dhtmlx.message({type: "error", text: "<%= idioma.get("ERROR_VISUALITZAR_CAMERA")%>", expire: 10000});
                    } else {
                        var url = opt["url"];
                        obrirWindowVideo(url);
                    }
                }
            };

            xhttp.open("GET", camera , true);
            xhttp.responseType = "json";
            xhttp.send();
        }
        
        function obrirWindowVideo(url){
            winVideo = new dhtmlXWindows().createWindow("winVideo", 0, 0, 690, 610);
            winVideo.setText("<%= idioma.get("TITOL_VIDEO")%>");
            
            inicializarWindowWithClose(winVideo);
            formCamera = winVideo.attachForm();
            formCamera.loadStruct(contentFormCamera);
            var container = formCamera.getContainer("contenidorVideo");
            container.innerHTML = '<img id="camera" width=" '+ 640 + '" height="'+ 480 + '" src="' + "<%=urlAPI%>" + url + '" frameborder="0" allowfullscreen></img>';
            
        }
        
        <%
            String rutaexcel = Utils.getProperty("RUTA_EXCEL");
        %>
        
        var toolBar = principalLayout.cells("a").attachToolbar();
        toolBar.setIconset("awesome");
        toolBar.addButton("exportar", 1, "Exportar", "fas fa-file-excel", null);
        toolBar.setIconSize(18);
        toolBar.setAlign("right");
        toolBar.attachEvent("onClick", function (id) {
            if (id == "exportar") {
//                gridCameres.toExcel("<%=rutaexcel%>");
            }
        });

        formCamera.bind(gridCameres);
        formCamera.attachEvent("onButtonClick", function (nom) {
            if (nom == "desa") {
                enviar();
            }
        });

        function enviar() {

            if (formCamera.validate()) {
                principalLayout.progressOn();
                formCamera.send(URL_DESA_CAMERA, "POST", function (r) {
                    dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: getAjax(r, TEMPS)});
                    gridCameres.clearAndLoad(URL_BUSCAR_CAMERA, function () {
                        principalLayout.progressOff();
                        formCamera.clear();
                    });
                });
            }
        }
        
        toolBar = principalLayout.cells("b").attachToolbar();
        toolBar.setIconset("awesome");
        toolBar.addButton("nou", 1, "", "fas fa-plus", null);
        toolBar.setItemToolTip("nou", "<%= idioma.get("CREAR_CAMERA_TOOLTIP")%>");
        toolBar.addButton("esborrar", 2, "", "fas fa-trash", null);
        toolBar.setItemToolTip("esborrar", "<%= idioma.get("ESBORRAR_CAMERA_TOOLTIP")%>");
        toolBar.setIconSize(18);
        toolBar.setAlign("left");
        
        toolBar.attachEvent("onClick", function (id) {
            if (id == "nou") {
                formCamera.clear();
            }
            if (id == "esborrar") {
                var id = gridCameres.getSelectedRowId();
                if (id != null) {
                    dhtmlx.confirm({
                        title: "<%= idioma.get("CONFIRMAR")%>",
                        type: "confirm-warning",
                        text: "<%= idioma.get("CONFIRMACIO_ESBORRAT_CAMERA")%>",
                        ok: "<%= idioma.get("ACCEPTAR")%>",
                        cancel: "<%= idioma.get("CANCELAR")%>",
                        callback: function (r) {
                            if (r) {
                                window.dhx.ajax.post(URL_ESBORRA_CAMERA, "id=" + id, function (r) {
                                    var resultat = getAjax(r, RESULTAT);
                                    if (resultat == ESBORRAT_EXIT) {
                                        gridCameres.clearAndLoad(URL_BUSCAR_CAMERA, function () {
                                            gridCameres.filterByAll();
                                        });
                                        formCamera.clear();
                                        dhtmlx.message({text: '<%= idioma.get("ESBORRAT_EXIT")%>', expire: "6000 "});
                                    }
                                });
                            }
                        }
                    });
                } else {
                    dhtmlx.message({
                        text: "<%= idioma.get("ELIMINAR_SELECCIO")%>",
                        expire: "10000"
                    });
                }
            }
        });
        
    }
    document.addEventListener("DOMContentLoaded", doOnLoad);
</script>

<div id="layoutCameres" style="width:99.8%;height:90%;overflow:hidden" ></div>




