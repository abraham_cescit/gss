/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.servlet.http.HttpServletRequest;
import util.commons.Utils;

/**
 *
 * @author pc103
 */
public class UsuarisPrivilegis {
    
    private Integer id;
    private Integer tipus;
    private String classe;

    public UsuarisPrivilegis() {
    }

    public UsuarisPrivilegis(Integer id, Integer tipus, String classe) {
        this.id = id;
        this.tipus = tipus;
        this.classe = classe;
    }
    
    public UsuarisPrivilegis(HttpServletRequest request) {
        this.id = Utils.getParameterInteger(request, "id");
        this.tipus = Utils.getParameterInteger(request, "tipus");
        this.classe = Utils.getParameterString(request, "controlador");
    }

    public int getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public int getTipus() {
        return tipus;
    }

    public void setTipus(Integer tipus) {
        this.tipus = tipus;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }
    
}
