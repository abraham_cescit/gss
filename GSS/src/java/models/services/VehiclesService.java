/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.services;

import controladors.base.Idioma;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Usuaris;
import models.Vehicles;
import persistencia.Bdades;

/**
 *
 * @author Abraham M
 */
public class VehiclesService {
    
    private static String NOM_TAULA = "vehicle";

    public static ArrayList<Vehicles> getVehicles() {
        ArrayList<Vehicles> r = new ArrayList<Vehicles>();

        String q = "SELECT idvehicle, matricula, tipus, tara FROM vehicle";

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                r.add(VehiclesService.obtenir(
                        rs.getInt("idvehicle"),
                        rs.getString("matricula"),
                        rs.getString("tipus"),
                        rs.getDouble("tara")));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(VehiclesService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }

    public static boolean insertVehicle(Vehicles v) {
        String sql = "INSERT INTO vehicle (matricula,tipus,tara) VALUES (?,?,?)";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        boolean r = false;
        int id = 0;

        try {

            int i = 1;
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstm.setString(i++, v.getMatricula());
            pstm.setString(i++, v.getTipus());
            pstm.setDouble(i++, v.getTara());
            pstm.executeUpdate();
            rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }

            if (id != 0) {
                r = true;
                HistoricCanvisService.insertHistoric(id, HistoricCanvisService.TIPUS_INSERT, NOM_TAULA, v.toString());
            }

        } catch (SQLException sqle) {
            Logger.getLogger(VehiclesService.class.getName()).log(Level.SEVERE, null, sqle);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }

        if (r) {

        }

        return r;
    }

    public static boolean updateVehicle(Vehicles v) {
        boolean r = false;
        String sql = "UPDATE vehicle SET matricula = ?," 
                + "tipus = ?," 
                + "tara = ?" 
                + " WHERE idvehicle = ?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            Integer i = 1;
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(i++, v.getMatricula());
            pstm.setString(i++, v.getTipus());
            pstm.setDouble(i++, v.getTara());
            pstm.setInt(i++, v.getIdVehicle());
            pstm.executeUpdate();

            r = true;
            if (true) {
                HistoricCanvisService.insertHistoric(v.getIdVehicle(), HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, v.toString());
            }

        } catch (SQLException ex) {
            Logger.getLogger(VehiclesService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    public static boolean deleteVehicle(int idVehicle) {
        boolean r = false;

        String sql = "DELETE FROM vehicle WHERE idvehicle=?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idVehicle);
            pstm.executeUpdate();
            r = true;
            HistoricCanvisService.insertHistoric(idVehicle, HistoricCanvisService.TIPUS_DELETE, NOM_TAULA, "Vehicle donat de baixa");
        } catch (SQLException ex) {
            Logger.getLogger(VehiclesService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    /**
     * Comprova si existeix a la base de dades un usuari amb aquest login
     *
     * @param login Login de l'usuari a comprovar.
     * @return true si existeix i false si no existeix a la base de dades.
     */
    public static boolean existeix(String matricula) {
        boolean r = false;
        String sql = "select matricula from vehicle where matricula=?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, matricula);
            rs = pstm.executeQuery();
            r = rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(VehiclesService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    public static Vehicles getVehiclePerMatricula(String matricula) {
        Vehicles r = new Vehicles();

        String sql = "SELECT idvehicle, matricula, tipus, tara FROM vehicle WHERE matricula = ?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, matricula);
            rs = pstm.executeQuery();
            while (rs.next()) {
                r.setIdVehicle(rs.getInt("idvehicle"));
                r.setMatricula(rs.getString("matricula"));
                r.setTipus(rs.getString("tipus"));
                r.setTara(rs.getDouble("tara"));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(VehiclesService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    public static Usuaris findEmailUsuari(int idUsuari) {
        Usuaris u = new Usuaris();
        String sql = "select email from usuari where idusuari=? ";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idUsuari);
            pstm.executeQuery();
            rs = pstm.getResultSet();
            if (rs.first()) {
                u.setEmail(rs.getString("email"));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(VehiclesService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return u;
    }

    public static ArrayList<Usuaris> findNomUsuari(String idioma) {
        idioma = Idioma.CAT;
        ArrayList<Usuaris> u = new ArrayList<Usuaris>();

        String sql = "select idusuari,nom,login "
                + "from usuari ";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            while (rs.next()) {
                Usuaris us = new Usuaris();
                us.setIdUsuari(rs.getInt("idusuari"));
                us.setNom(rs.getString("nom"));
                us.setLogin(rs.getString("login"));
                u.add(us);
            }

        } catch (SQLException sqle) {
            Logger.getLogger(VehiclesService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }

        return u;
    }

    public static Vehicles obtenir(Integer idVehicle, String matricula, String tipus, Double tara) {
        return new Vehicles(idVehicle, matricula, tipus, tara);
    }
    
}
