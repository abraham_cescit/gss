function menosde10(data){ 
    if(data.length < 10 && data.length > 0) 
        return true;
    else
        return false;
}

function menosde100(data){ 
    if(data.length < 100 && data.length > 0) 
        return true;
    else
        return false;
}

function notNullNotBlank(parametro){
    if(parametro != null && parametro != "")
        return true;
    else
        return false;
}

function ValidChars(parametro) {
    if(/%|&|#/.test(parametro))
        return false;
    else
        return true;
}

 function telfSpaces(data) {
    if(/^(\s*[0-9]+\s*)+$/.test(data))
        return true;
    else
        return false;
}