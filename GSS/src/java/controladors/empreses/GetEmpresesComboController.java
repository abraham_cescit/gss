/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.empreses;

import controladors.base.CITServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Empreses;
import models.services.EmpresesService;

/**
 *
 * @author Abraham M
 */
public class GetEmpresesComboController extends CITServlet {
    
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {
                setXML();

                ArrayList<Empreses> llista = EmpresesService.getEmpreses();
                
                PrintWriter out = response.getWriter();
                out.print("<?xml version='1.0' encoding='UTF-8'?>");
                out.print("<complete>");
                out.print("<option value=''></option>");
                for (Empreses e : llista) {
                    out.print("<option value='" + e.getIdEmpresa() + "'>" + e.getNom() +"</option>");
                }
                out.print("</complete>");
                
                super.processRequest(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GetEmpresesComboController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}
