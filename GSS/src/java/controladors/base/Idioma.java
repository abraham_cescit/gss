package controladors.base;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Set;

/**
 * Representa el diccionari de l'idioma que s'ha fet disponible  per al controlador
 * i per la vista.
 * 
 * @author jose
 */
public class Idioma {
    public static final String CAT="CAT";
    public static final String ES="ES";
    public static final String EN="EN";
    
    
    
    /**
     * Idiomes disponibles
     */
    private static String[] disponibles=null;
    
    
    /**
     * Codi de l'idioma
     */
    public String lang;
    /**
     * Parells clau-texte emmagatzemats com a Properties
     */
    public Properties textes=new Properties();
    
    public Idioma(){}
    
    /**
     * 
     * @param lang Codi de l'idioma
     */
    public Idioma(String lang){
        lang=lang.toUpperCase();
        this.lang=lang;
        
        afegir("base");
        
       
    }
    
    /**
     * CArregar un arxiu de diccionari. S'afegira al diccionari que ja te aquest objecte.
     * 
     * @param arxiu Nm del diccionari sense ruta (Veure CITServlet.addDiccionari
     */
    public final void afegir(String arxiu){
        try{
            InputStream fis=Thread.currentThread().getContextClassLoader().getResourceAsStream("idiomes/"+lang+"/"+arxiu+".xml");
            textes.loadFromXML(fis);
            fis.close();
        }
        catch(IOException ioe){
            try{
                InputStream fis2=Thread.currentThread().getContextClassLoader().getResourceAsStream("idiomes/"+lang+"/"+arxiu+".xml");
                textes.loadFromXML(fis2);
                fis2.close();
            }
            catch(IOException ioe2){
                System.err.println(ioe2);
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Obte el texte en aquest idioma corresponent a aquesta clau.
     * 
     * @param clau Clau del texte a recuperar
     * @return Texte que correspon a la clau.
     */
    public String get(String clau){
        String t = textes.getProperty(clau);
        if(t==null) t="";
        return t;
    }
    
    public static String[] getIdiomesDisponibles(){
        if(disponibles==null){
            Properties disp=new Properties();
            try{
                InputStream fis2=Thread.currentThread().getContextClassLoader().getResourceAsStream("idiomes/disponibles.xml");
                disp.loadFromXML(fis2);
                fis2.close();
                disponibles=new String[disp.size()];
                Enumeration e=disp.keys();
                int i=0;
                while(e.hasMoreElements()){
                    String k=(String)e.nextElement();
                    disponibles[i]=k;
                    i++;
                }
            }
            catch(IOException ioe2){
                ioe2.printStackTrace();
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
        return disponibles;
    }
    
    public String substituir(String c){
        String r=c;
        
        Set claus=textes.keySet();
        for(Object k : claus){
            String clau="<!--"+(String)k+"-->";
            r=r.replace(clau,textes.getProperty((String)k));
        }
        return r;
    }
    
}
