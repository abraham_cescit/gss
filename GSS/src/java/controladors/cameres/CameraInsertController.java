/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.cameres;

import controladors.base.CITServlet;
import static controladors.base.CITServlet.TIPUS_BREU_XML;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Cameres;
import models.services.CameresService;

/**
 *
 * @author Abraham M
 */
public class CameraInsertController extends CITServlet {

    public static final String DESAT_EXIT = "DESAT_EXIT";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {
                request.setCharacterEncoding("UTF-8");
                setTipus(TIPUS_BREU_XML);
                addDiccionari("cameres");

                Cameres camera = new Cameres(request);
                try {

                    if (camera.getIdCamera() == null) {
                        CameresService.insertarCamera(camera);
                    } else {
                        CameresService.updateCamera(camera);
                    }

                    afegirRespostaBreu(RESULTAT, "ok");
                    afegirRespostaBreu(MISSATGE, getTexte("CAMERA_CREAT_EXIT"));
                    afegirRespostaBreu(TEMPS, "6000");

                } catch (Exception expa) {
                    afegirRespostaBreu(RESULTAT, "error");
                    afegirRespostaBreu(MISSATGE, getTexte("CAMERA_CREAT_ERROR"));
                    afegirRespostaBreu(TEMPS, "0");
                    Logger.getLogger(CameraInsertController.class.getName()).log(Level.SEVERE, null, expa);
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(CameraInsertController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CameraInsertController.class.getName()).log(Level.SEVERE, null, ex);
        }

        super.processRequest(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
