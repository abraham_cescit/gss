<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="controladors.base.Idioma"  %>
<% Idioma idioma = (Idioma) request.getAttribute("idioma");%>
<!DOCTYPE html>
<style>
    body {
        background-color: #1a3471;
        background-position: center center;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
    }
    #divlogin {
        width:400px;
        background-color:white;
        padding:50px;
        border-radius: 10px;
        margin-top:100px;
        margin-left:33%;
        text-align:center;
    }
</style>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= idioma.get("ACCES_DENEGAT")%></title>
    </head>
    <body>
        <div id="divlogin" class="container">
            <h1><%= idioma.get("ACCES_DENEGAT")%></h1>
            <p><%= idioma.get("NO_TENS_PERMISOS")%> </p>                
            <a href="login"><%= idioma.get("TORNAR_A_LOGIN")%></a>
        </div>
    </body>
</html>
