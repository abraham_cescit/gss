/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.servlet.http.HttpServletRequest;
import util.commons.Utils;

/**
 *
 * @author Abraham M
 */
public class Empreses {
    
    public Integer idEmpresa;
    public String nom;
    public String idFiscal;
    public String adreca;
    public String cpostal;
    public String poblacio;
    public String pais;
    public String telefon;
    public String contacte;
    public String email;
    public String web;

    public Empreses(Integer idEmpresa, String nom, String idFiscal, String adreca, String cpostal, String poblacio, String pais, String telefon, String contacte, String email, String web) {
        this.idEmpresa = idEmpresa;
        this.nom = nom;
        this.idFiscal = idFiscal;
        this.adreca = adreca;
        this.cpostal = cpostal;
        this.poblacio = poblacio;
        this.pais = pais;
        this.telefon = telefon;
        this.contacte = contacte;
        this.email = email;
        this.web = web;
    }

    public Empreses(HttpServletRequest request) {
        this.idEmpresa = Utils.getParameterInteger(request, "idEmpresa");
        this.nom = Utils.getParameterString(request, "nom");
        this.idFiscal = Utils.getParameterString(request, "idFiscal");
        this.adreca = Utils.getParameterString(request, "adreca");
        this.cpostal = Utils.getParameterString(request, "cpostal");
        this.poblacio = Utils.getParameterString(request, "poblacio");    
        this.pais = Utils.getParameterString(request, "pais");    
        this.telefon = Utils.getParameterString(request, "telefon");    
        this.contacte = Utils.getParameterString(request, "contacte");
        this.email = Utils.getParameterString(request, "email");
        this.web = Utils.getParameterString(request, "web");
    }

    public Empreses() {
        
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getIdFiscal() {
        return idFiscal;
    }

    public void setIdFiscal(String idFiscal) {
        this.idFiscal = idFiscal;
    }

    public String getAdreca() {
        return adreca;
    }

    public void setAdreca(String adreca) {
        this.adreca = adreca;
    }

    public String getCpostal() {
        return cpostal;
    }

    public void setCpostal(String cpostal) {
        this.cpostal = cpostal;
    }

    public String getPoblacio() {
        return poblacio;
    }

    public void setPoblacio(String poblacio) {
        this.poblacio = poblacio;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getContacte() {
        return contacte;
    }

    public void setContacte(String contacte) {
        this.contacte = contacte;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }
    
    @Override
    public String toString() {
        String s = "Id:  " + idEmpresa
                + "\nNom: " + nom
                + "\nCIF/NIF: " + idFiscal
                + "\nAdreca: " + adreca
                + "\nCodi Postal: " + cpostal
                + "\nPoblacio: " + poblacio
                + "\nPais: " + pais
                + "\nTelèfon: " + telefon
                + "\nContacte: " + contacte
                + "\nEmail: " + email
                + "\nWeb: " + web;
        return s;
    }
    
}
