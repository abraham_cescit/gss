package controladors.base;

/**
 * Per us intern de CITServlet . Representa un parell nom-valor per a enviar al navegador com
 * a resposta auna peticio AJAX del DHTMLX
 * 
 * @author jose
 */
class ValorRespostaBreu {
    public String nom;
    public String valor;
    
    /**
     * 
     * @param nom
     * @param valor 
     */
    public ValorRespostaBreu(String nom,String valor){
        this.nom=nom;
        this.valor=valor;
    }
}
