/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cescit.security;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import models.services.UsuariService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SaltedAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author fernando
 */
public class CustomSecurityRealm extends JdbcRealm {

    private static final Logger log = LoggerFactory.getLogger(CustomSecurityRealm.class);
    protected String jndiDataSourceName;

    public CustomSecurityRealm() {

    }

    public String getJndiDataSourceName() {
        return jndiDataSourceName;
    }

    public void setJndiDataSourceName(String jndiDataSourceName) {
        this.jndiDataSourceName = jndiDataSourceName;
        this.dataSource = getDataSourceFromJNDI();
    }

    private DataSource getDataSourceFromJNDI() {
        try {
            InitialContext ic = new InitialContext();
            return (DataSource) ic.lookup(jndiDataSourceName);
        } catch (NamingException e) {
            log.error("JNDI error while retrieving " + jndiDataSourceName, e);
            throw new AuthorizationException(e);
        }
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        UsernamePasswordToken userPassToken = (UsernamePasswordToken) token;
        String username = userPassToken.getUsername();

        // Null username is invalid
        if (username == null) {
            log.debug("Username is null.");
            return null;
        }

        String password = UsuariService.getContrasenya(username);
        String salt = UsuariService.getSalt(username);
        SaltedAuthenticationInfo info = new CustomSaltedAuthentificationInfo(username, password, salt);

        return info;
    }
}
