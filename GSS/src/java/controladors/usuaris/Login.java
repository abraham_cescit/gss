/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.usuaris;

import controladors.base.CITServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

/**
 *
 * @author jose
 */
// @WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends CITServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws java.io.IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String r = INDEFINIDA;
        setRequest(request);
        setTitol("Login");

        if (request.getParameter("login") != null) {
            String login = request.getParameter("login");
            String password = request.getParameter("password");
            try {
                Subject currentUser = SecurityUtils.getSubject();
                UsernamePasswordToken token = new UsernamePasswordToken(login, password);

                try {
                    currentUser.login(token);
                    //Detección del tipo de dispositivo
                    if (request.getHeader("User-Agent").indexOf("Mobile") != -1) {
                        //response.sendRedirect("comandes");
                        response.sendRedirect("registrediari");
                    } else {
                        response.sendRedirect("registrediari");
                    }
                } catch (UnknownAccountException uae) {
                    response.sendRedirect("login?error=1");
                } catch (IncorrectCredentialsException ice) {
                    response.sendRedirect("login?error=1");
                } catch (LockedAccountException lae) {
                    response.sendRedirect("login?error=1");
                } catch (AuthenticationException ae) {
                    response.sendRedirect("login?error=1");
                }

            } catch (Exception ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            setMissatge("Login incorrecte.");
            setVista("/usuaris/login.jsp");
            try {
                inicialitzar(request, response, this, false);
            } catch (SQLException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }
            setCap("headerbuit.jsp");
            addCSS("css/bootstrap.css");
            // addDiccionari("usuaris");
            super.processRequest(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws java.io.IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws java.io.IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
