/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.permisos;

import controladors.base.CITServlet;
import controladors.base.Idioma;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// model de permisos quan es creii
import models.HistoricCanvis;

/**
 *
 * @author Sònia
 */
public class PermisosController extends CITServlet{
    
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {
                addDiccionari("permisos");
                Idioma idioma = getIdioma();
                setTitol(idioma.get("TITLE_HTML"));
                setVista("/administracio/permisos.jsp");
                super.processRequest(request, response);
            }else{
                setVista("/base/noteacces.jsp");
                super.processRequest(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HistoricCanvis.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}