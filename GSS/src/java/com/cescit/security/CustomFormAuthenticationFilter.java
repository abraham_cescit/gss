package com.cescit.security;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import models.Usuaris;
import models.UsuarisConexions;
import models.services.UsuarisConexionsService;
import models.services.UsuariService;
import org.apache.shiro.SecurityUtils;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import util.commons.LoggerOut;

public class CustomFormAuthenticationFilter extends FormAuthenticationFilter {

    @Override
    protected void setFailureAttribute(ServletRequest request, AuthenticationException ae) {
        String message = ae.getMessage();
        request.setAttribute(getFailureKeyAttribute(), message);
    }

    @Override
    protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request, ServletResponse response) throws Exception {
        
        //Guardar datos de sesión
        Subject currentUser = SecurityUtils.getSubject();
        Session session = currentUser.getSession();
        
        Usuaris u = UsuariService.getDadesSessio(token.getPrincipal().toString());
        session.setAttribute("lang", u.idioma);
        session.setAttribute("iduser", u.idUsuari);
        session.setAttribute("nom", u.nom);
        session.setAttribute("tipus", u.getTipus());
        
        //Guardar conexión en BD
        String pais = response.getLocale().getDisplayCountry();
        String ip = request.getRemoteAddr();
        UsuarisConexions uc = new UsuarisConexions();
        uc.setPais(pais);
        uc.setIp(ip);
        uc.setLogin(u.login);
        if(!UsuarisConexionsService.insertConexio(uc))
            System.out.print("Error al insertar la conexión del usuario "+u.login+"."+"IP: "+ip+"; Pais: "+pais);

        return true;
    }

}
