/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.commons;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author pc103
 */
public class LoggerOut {
    
    public static void sendLogger(HttpServletRequest request, Exception e, Class clase, String detalles) {
        int user = 0;
        String nomUser = "";
        if (request != null) {
            HttpSession sesio = request.getSession();
            user = Integer.parseInt(sesio.getAttribute("iduser").toString());
            nomUser = sesio.getAttribute("nom").toString();
        } 
        Logger logger = Logger.getLogger(clase.getName());
        String message = "\n\n=================================================\n";
        message += "SE HA DETECTADO UN ERROR\n";
        message += "=================================================\n\n";
        message += "-> Usuario : " + nomUser;
        message += "\n-> Hora : " +  new Date();
        message += "\n-> Detalles : " + detalles;
        message += "\n";
        logger.log(Level.SEVERE, message, e); 
    }
    
    public static void sendLoggerSimple(String detalles){
        String message = "\n\n=================================================\n";
        message += "SE HA DETECTADO UN ERROR\n";
        message += "=================================================\n\n";
        message += "\n-> Hora : " +  new Date();
        message += "\n-> Detalles : " + detalles;
        message += "\n";
        System.out.print(message);
    }
    
}
