package models;

import java.sql.Date;

public class HistoricCanvis {

    private Integer id;
    private Date data;
    private Integer idtaula;
    private String nomtaula;
    private Integer idusuari;
    private String tipus;
    private String valors;

    public HistoricCanvis(Integer id, Date data, Integer idtaula, String nomtaula, Integer idusuari, String tipus, String valors) {
        this.id = id;
        this.data = data;
        this.idtaula = idtaula;
        this.nomtaula = nomtaula;
        this.idusuari = idusuari;
        this.tipus = tipus;
        this.valors = valors;
    }

    public HistoricCanvis() {
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getIdtaula() {
        return idtaula;
    }

    public void setIdtaula(Integer idtaula) {
        this.idtaula = idtaula;
    }

    public String getNomtaula() {
        return nomtaula;
    }

    public void setNomtaula(String nomtaula) {
        this.nomtaula = nomtaula;
    }

    public int getIdusuari() {
        return idusuari;
    }

    public void setIdusuari(Integer idusuari) {
        this.idusuari = idusuari;
    }

    public String getTipus() {
        return tipus;
    }

    public void setTipus(String tipus) {
        this.tipus = tipus;
    }

    public String getValors() {
        return valors;
    }

    public void setValors(String valors) {
        this.valors = valors;
    }

}
