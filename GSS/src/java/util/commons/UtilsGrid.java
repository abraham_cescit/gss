/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.commons;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author fernando
 */
public class UtilsGrid extends UtilsBase {

    public static String toXMLPK(String a) {
        String str = "";
        str = str.concat("<row id='").concat(a).concat("'>");
        return str;
    }

    public static String toXMLPK(Integer a) {
        String str = "";
        if (a != null) {
            str = str.concat("<row id='").concat(a.toString()).concat("'>");
        }
        return str;
    }

    public static String toXML(String a) {
        String str = "";
        if (a == null) {
            a = "";
        }
        str = str.concat("<cell><![CDATA[").concat(a).concat("]]></cell>");
        return str;
    }
    
    public static String toXML(Boolean a) {
        return "<cell><![CDATA[" + (a ? "1" : "0") + "]]></cell>";
    }
        
    public static String toXMLAlert(String a) {
        String str = "";
        if (a == null) {
            a = "";
        }
        str = str.concat("<cell style='color:#d86d0f'><![CDATA[").concat(a).concat("]]></cell>");
        return str;
    }

    public static String toXMLEdit() {
        String str = "";
        str = str.concat("<cell style='border-bottom:1px solid #7F052A;background-color:#f9e79f'><![CDATA[").concat("]]></cell>");
        return str;
    }

    public static String toXMLMedium(String a) {
        String str = "";
        if (a == null) {
            a = "";
        }
        str = str.concat("<cell style='font-size: 12px;'><![CDATA[").concat(a).concat("]]></cell>");
        return str;
    }

    public static String toXMLSmall(String a) {
        String str = "";
        if (a == null) {
            a = "";
        }
        str = str.concat("<cell style='font-size: 4px;'><![CDATA[").concat(a).concat("]]></cell>");
        return str;
    }

    public static String toXMLBold(String a) {
        String str = "";
        if (a == null) {
            a = "";
        }
        str = str.concat("<cell style='font-weight: bold;'><![CDATA[").concat(a).concat("]]></cell>");
        return str;
    }

    public static String toXMLDateUtil(java.util.Date a) {
        String str = "";
        String b = "";
        if (a == null) {
            b = "";
        } else {
            b = a.toString();
        }
        str = str.concat("<cell><![CDATA[").concat(b).concat("]]></cell>");
        return str;
    }
    
    public static String toXMLTimestamp(Timestamp a) {
        String str = "";
        String b = "";
        if (a == null) {
            b = "";
        } else {
            b = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(a);
        }
        str = str.concat("<cell><![CDATA[").concat(b).concat("]]></cell>");
        return str;
    }
    
    public static String toXMLTimestampData(Timestamp a) {
        String str = "";
        String b = "";
        if (a == null) {
            b = "";
        } else {
            b = new SimpleDateFormat("dd-MM-yyyy").format(a);
        }
        str = str.concat("<cell><![CDATA[").concat(b).concat("]]></cell>");
        return str;
    }
    
    public static String toXMLTimestampHoraMinut(Timestamp a) {
        String str = "";
        String b = "";
        if (a == null) {
            b = "";
        } else {
            b = new SimpleDateFormat("HH:mm").format(a);
        }
        str = str.concat("<cell><![CDATA[").concat(b).concat("]]></cell>");
        return str;
    }

    public static String toXMLDateFull(java.util.Date a) {
        String str = "";
        String b = "";
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        if (a == null) {
            b = "";
        } else {
            b = format.format(a);
        }
        str = str.concat("<cell><![CDATA[").concat(b).concat("]]></cell>");
        return str;
    }

    public static String toXMLDateSQL(java.sql.Date a) {
        String str = "";
        String b = "";
        if (a == null) {
            b = "";
        } else {
            b = a.toString();
        }
        str = str.concat("<cell><![CDATA[").concat(b).concat("]]></cell>");
        return str;
    }

    public static String toXMLINT(Integer a) {
        String str = "";
        if (a != null && !a.equals("")) {
            str = str.concat("<cell><![CDATA[").concat(a.toString()).concat("]]></cell>");
        } else {
            str = str.concat("<cell><![CDATA[]]></cell>");
        }
        return str;
    }

    public static String toXMLDouble(Double a) {
        String str = "";

        if (a != null && !a.equals("")) {
            str = str.concat("<cell><![CDATA[").concat((Math.rint(a * 1000) / 1000) + "").concat("]]></cell>");
        } else {
            str = str.concat("<cell><![CDATA[]]></cell>");
        }
        return str;
    }

    public static String toXMLDoubleBold(Double a) {
        String str = "";

        if (a != null && !a.equals("")) {
            str = str.concat("<cell  style='font-weight: bold;'><![CDATA[").concat((Math.rint(a * 1000) / 1000) + "").concat("]]></cell>");
        } else {
            str = str.concat("<cell><![CDATA[]]></cell>");
        }
        return str;
    }

    public static String toXMLDoubleColorNoBold(Double a) {
        String str = "";
        if (a == 0.0 || a == -0.0) {
            str = str.concat("<cell><![CDATA[").concat((Math.rint(a * 1000) / 1000) + "").concat("]]></cell>");
        } else if (a != null && !a.equals("") && a >= 0.0) {
            str = str.concat("<cell  style='color: #239b56 ;'><![CDATA[").concat((Math.rint(a * 1000) / 1000) + "").concat("]]></cell>");
        } else if (a != null && !a.equals("") && a <= 0.0) {
            str = str.concat("<cell  style='color:  #e74c3c ;'><![CDATA[").concat((Math.rint(a * 1000) / 1000) + "").concat("]]></cell>");
        } else {
            str = str.concat("<cell><![CDATA[]]></cell>");
        }
        return str;
    }
    
    public static String toXMLINTEditWindows(Integer a) {
        String str = "";
        if (a != null && !a.equals("")) {
            str = str.concat("<cell style='border-bottom:1px solid  #7F052A'><![CDATA[").concat(a.toString()).concat("]]></cell>");
        } else {
            str = str.concat("<cell style='border-bottom:1px solid  #7F052A'><![CDATA[]]></cell>");
        }
        return str;
    }
    
    public static String toXMLDoubleEditWindows(Double a) {
        String str = "";

        if (a != null && !a.equals("")) {
            str = str.concat("<cell style='border-bottom:1px solid  #7F052A;'><![CDATA[").concat((Math.rint(a * 1000) / 1000) + "").concat("]]></cell>");
        } else {
            str = str.concat("<cell style='border-bottom:1px solid  #7F052A'><![CDATA[]]></cell>");
        }
        return str;
    }
    
    public static String toXMLEditWindows(String a) {
        String str = "";
        if (a == null) {
            a = "";
        }
        str = str.concat("<cell style='border-bottom:1px solid  #7F052A'><![CDATA[").concat(a).concat("]]></cell>");
        return str;
    }

    public static String toXMLDoubleColor(Double a) {
        String str = "";

        if (a != null && !a.equals("") && a >= 0.0) {
            str = str.concat("<cell  style='font-weight: bold;color: #239b56 ;'><![CDATA[").concat((Math.rint(a * 1000) / 1000) + "").concat("]]></cell>");
        } else if (a != null && !a.equals("") && a <= 0.0) {
            str = str.concat("<cell  style='font-weight: bold;color:  #e74c3c ;'><![CDATA[").concat((Math.rint(a * 1000) / 1000) + "").concat("]]></cell>");
        } else {
            str = str.concat("<cell><![CDATA[]]></cell>");
        }
        return str;
    }
    
    public static String toXMLDoubleColorEditWindows(Double a) {
         String str = "";

        if (a != null && !a.equals("") && a >= 0.0) {
            str = str.concat("<cell  style='border-bottom:1px solid  #7F052A;font-weight: bold;color: #239b56 ;'><![CDATA[").concat((Math.rint(a * 1000) / 1000) + "").concat("]]></cell>");
        } else if (a != null && !a.equals("") && a <= 0.0) {
            str = str.concat("<cell  style='border-bottom:1px solid  #7F052A;font-weight: bold;color:  #e74c3c ;'><![CDATA[").concat((Math.rint(a * 1000) / 1000) + "").concat("]]></cell>");
        } else {
            str = str.concat("<cell><![CDATA[]]></cell>");
        }
        return str;
    }
    
    public static String toXMLDoubleRedEditWindows(Double a) {
         String str = "";

        if (a != null && !a.equals("")) {
            str = str.concat("<cell  style='border-bottom:1px solid  #7F052A;font-weight: bold;color:  #e74c3c ;'><![CDATA[").concat((Math.rint(a * 1000) / 1000) + "").concat("]]></cell>");
        } else {
            str = str.concat("<cell><![CDATA[]]></cell>");
        }
        return str;
    }
    public static String toXMLColor(String a, String color) {
        String str = "";
        if (a == null) {
            a = "";
        }
        str = str.concat("<cell style='font-weight: bold;color:  " + color + "'><![CDATA[").concat(a).concat("]]></cell>");
        return str;
    }
    public static String toXMLDoubleGreenEditWindows(Double a) {
         String str = "";

         if (a != null && !a.equals("")) {
            str = str.concat("<cell  style='border-bottom:1px solid  #7F052A;font-weight: bold;color:  #239b56 ;'><![CDATA[").concat((Math.rint(a * 1000) / 1000) + "").concat("]]></cell>");
        } else {
            str = str.concat("<cell><![CDATA[]]></cell>");
        }
        return str;
    }

    public static String toXMLCheck(Boolean a) {
        String str = "";
        if (a == true) {
            str = str.concat("<cell style='font-size: 34px;'><![CDATA[1]]></cell>");
        } else {
            str = str.concat("<cell style='font-size: 34px'><![CDATA[0]]></cell>");
        }
        return str;
    }

    public static String toXMLIcon(String icono, String titulo) {
        String str = "";
        if (icono != null && !icono.equals("")) {
            str = str.concat("<cell title='").concat(titulo).concat("'><![CDATA[<i class='").concat(icono).concat("' style='margin-right:5px;font-size: 18px;font-weight: 400;'></i>]]></cell>");
        } else {
            str = str.concat("<cell><![CDATA[]]></cell>");
        }
        return str;
    }

    public static String toXMLCombo(HashMap<Integer, String> a) {
        String str = "";
        if (!a.isEmpty()) {
            Map.Entry<Integer, String> entry = a.entrySet().iterator().next();
            String value = entry.getValue();
            str = "<cell xmlcontent='1' type='combo' editable='1'>" + value;
            str += "<option value=''>Edit</option>";
            str += "<option value=''></option>";
            for (Integer key : a.keySet()) {
                str += "<option value='" + key + "'>" + a.get(key) + "</option>";
            }
            str = str.concat("</cell>");
        } else {
            str = "<cell xmlcontent='1' type='combo' editable='1'>";
        str += "<option value=''>Edit</option>";
        str += "<option value=''></option>";
        str = str.concat("</cell>");
        }
        return str;
    }


    public static String toXMLend() {
        String str = "";
        str = str.concat("</row>");
        return str;
    }

}
