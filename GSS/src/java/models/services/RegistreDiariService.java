/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.services;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Imatges;
import models.dto.RegistreDiariDTO;
import persistencia.Bdades;

/**
 *
 * @author Abraham M
 */
public class RegistreDiariService {
    
    private static String NOM_TAULA = "entsor";

    public static boolean insertEntradaRegistre(RegistreDiariDTO registre) {

        boolean result = false;
        Connection con = null;
        CallableStatement cstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            cstm = con.prepareCall("{call insertarRegistre(?,?,?,?,?,?,?,?,?,?,?)}");

            int i = 1;
            cstm.registerOutParameter(i++, Types.INTEGER);
            cstm.setInt(i++, registre.getIdEspera());
            cstm.setInt(i++, registre.getIdEmpresa());
            cstm.setInt(i++, registre.getIdVehicle());
            cstm.setTimestamp(i++, registre.getEntrada());
            cstm.setDouble(i++, registre.getPesEntrada());
            for(int j = 0; j < registre.getFotosArray().size(); j++){
                cstm.setString(i++, registre.getFotosArray().get(j).getUrl());
            }
            cstm.execute();
            cstm.getInt(1);
            result = !cstm.wasNull();
        } catch (SQLException ex) {
            Logger.getLogger(RegistreDiariService.class.getName()).log(Level.SEVERE, null, ex);
            result = false;
        } finally {
            Bdades.closeConnections(con, cstm, rs);
        }
        return result;
    }
    
    public static ArrayList<RegistreDiariDTO> getEntrades() {
        ArrayList<RegistreDiariDTO> r = new ArrayList<RegistreDiariDTO>();

        String q = "SELECT reg.idregistre, reg.idempresa, e.nom, e.idfiscal, e.telefon, "
                + "reg.idvehicle, v.matricula, reg.entrada, reg.pesentrada FROM entsor reg "              
                + "JOIN empresa e ON reg.idempresa = e.idempresa JOIN vehicle v ON reg.idvehicle = v.idvehicle "
                + "WHERE date(reg.entrada) = date(now()) AND reg.sortida IS NULL";
        
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                RegistreDiariDTO diari = new RegistreDiariDTO();
                diari.setIdRegistre(rs.getInt("reg.idregistre"));
                diari.setIdEmpresa(rs.getInt("reg.idempresa"));
                diari.setNomEmpresa(rs.getString("e.nom"));
                diari.setIdFiscal(rs.getString("e.idfiscal"));
                diari.setTelefon(rs.getString("e.telefon"));
                diari.setIdVehicle(rs.getInt("reg.idvehicle"));
                diari.setMatricula(rs.getString("v.matricula"));
                diari.setEntrada(rs.getTimestamp("reg.entrada"));
                diari.setPesEntrada(rs.getDouble("reg.pesentrada"));
                r.add(diari);
            }
        } catch (SQLException sqle) {
            Logger.getLogger(RegistreDiariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }
    
    public static boolean insertSortidaRegistre(RegistreDiariDTO registre) {

        boolean result = false;
        Connection con = null;
        CallableStatement cstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            cstm = con.prepareCall("{call insertarSortidaRegistre(?,?,?,?,?)}");

            int i = 1;
            cstm.registerOutParameter(i++, Types.INTEGER);
            cstm.setInt(i++, registre.getIdRegistre());
            cstm.setTimestamp(i++, registre.getSortida());
            cstm.setDouble(i++, registre.getPesSortida());
            for(int j = 0; j < registre.getFotosArray().size(); j++){
                cstm.setString(i++, registre.getFotosArray().get(j).getUrl());
            }
            
            cstm.execute();
            cstm.getInt(1);
            result = !cstm.wasNull();
        } catch (SQLException ex) {
            Logger.getLogger(RegistreDiariService.class.getName()).log(Level.SEVERE, null, ex);
            result = false;
        } finally {
            Bdades.closeConnections(con, cstm, rs);
        }
        return result;
    }
    
    public static ArrayList<RegistreDiariDTO> getSortides() {
        ArrayList<RegistreDiariDTO> r = new ArrayList<RegistreDiariDTO>();

        String q = "SELECT reg.idregistre, reg.idempresa, e.nom, e.idfiscal, e.telefon, "
                + "reg.idvehicle, v.matricula, reg.entrada, reg.pesentrada, reg.sortida, reg.pessortida, "
                + "reg.temps, reg.difpes FROM entsor reg "              
                + "JOIN empresa e ON reg.idempresa = e.idempresa JOIN vehicle v ON reg.idvehicle = v.idvehicle "
                + "WHERE date(reg.entrada) = date(now()) AND reg.sortida IS NOT NULL";
        
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                r.add(RegistreDiariService.obtenirSortida(
                        rs.getInt("idregistre"),
                        rs.getInt("idempresa"),
                        rs.getString("nom"),
                        rs.getString("idfiscal"),
                        rs.getString("telefon"),                        
                        rs.getInt("idvehicle"),
                        rs.getString("matricula"),
                        rs.getTimestamp("entrada"),
                        rs.getDouble("pesentrada"),
                        rs.getTimestamp("sortida"),
                        rs.getDouble("pessortida"),
                        rs.getInt("temps"),
                        rs.getDouble("difpes")));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(RegistreDiariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }
    
    public static ArrayList<RegistreDiariDTO> getHistoric() {
        ArrayList<RegistreDiariDTO> r = new ArrayList<RegistreDiariDTO>();

        String q = "SELECT reg.idregistre, reg.idempresa, e.nom, e.idfiscal, e.telefon, "
                + "reg.idvehicle, v.matricula, reg.entrada, reg.pesentrada, reg.sortida, reg.pessortida, "
                + "reg.temps, reg.difpes FROM entsor reg "              
                + "JOIN empresa e ON reg.idempresa = e.idempresa JOIN vehicle v ON reg.idvehicle = v.idvehicle "
                + "WHERE reg.sortida IS NOT NULL ORDER BY reg.entrada";
        
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                r.add(RegistreDiariService.obtenirSortida(
                        rs.getInt("idregistre"),
                        rs.getInt("idempresa"),
                        rs.getString("nom"),
                        rs.getString("idfiscal"),
                        rs.getString("telefon"),                        
                        rs.getInt("idvehicle"),
                        rs.getString("matricula"),
                        rs.getTimestamp("entrada"),
                        rs.getDouble("pesentrada"),
                        rs.getTimestamp("sortida"),
                        rs.getDouble("pessortida"),
                        rs.getInt("temps"),
                        rs.getDouble("difpes")));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(RegistreDiariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }
    
    public static ArrayList<RegistreDiariDTO> getHistoricDates(Date datainici, Date datafinal) {
        ArrayList<RegistreDiariDTO> r = new ArrayList<RegistreDiariDTO>();

        String sql = "SELECT reg.idregistre, reg.idempresa, e.nom, e.idfiscal, e.telefon, "
                + "reg.idvehicle, v.matricula, reg.entrada, reg.pesentrada, reg.sortida, reg.pessortida, "
                + "reg.temps, reg.difpes FROM entsor reg "              
                + "JOIN empresa e ON reg.idempresa = e.idempresa JOIN vehicle v ON reg.idvehicle = v.idvehicle "
                + "WHERE reg.entrada >= ? AND reg.entrada <= ? AND reg.sortida IS NOT NULL ORDER BY reg.entrada";
        
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            pstm = con.prepareStatement(sql);
            int i = 1;
            pstm.setTimestamp(i++, new Timestamp(datainici.getTime()));
            pstm.setTimestamp(i++, new Timestamp(datafinal.getTime()));
            rs = pstm.executeQuery();
            while (rs.next()) {
                r.add(RegistreDiariService.obtenirSortida(
                        rs.getInt("idregistre"),
                        rs.getInt("idempresa"),
                        rs.getString("nom"),
                        rs.getString("idfiscal"),
                        rs.getString("telefon"),                        
                        rs.getInt("idvehicle"),
                        rs.getString("matricula"),
                        rs.getTimestamp("entrada"),
                        rs.getDouble("pesentrada"),
                        rs.getTimestamp("sortida"),
                        rs.getDouble("pessortida"),
                        rs.getInt("temps"),
                        rs.getDouble("difpes")));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(RegistreDiariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    public static boolean updateRegistre (RegistreDiariDTO registre) {
        boolean r = false;
        String sql = "UPDATE entsor SET " 
                + "idempresa = ?," 
                + "idvehicle = ?," 
                + "entrada = ?," 
                + "pesentrada = ?" 
                + "WHERE idregistre = ?";
        
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            Integer i = 1;
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(i++, registre.getIdEmpresa());
            pstm.setInt(i++, registre.getIdVehicle());
            pstm.setTimestamp(i++, registre.getEntrada());
            pstm.setDouble(i++, registre.getPesEntrada());
            pstm.setInt(i++, registre.getIdRegistre());
            pstm.executeUpdate();

            r = true;
            if (true) {
                HistoricCanvisService.insertHistoric(registre.getIdRegistre(), HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, registre.toString());
            }

        } catch (SQLException ex) {
            Logger.getLogger(RegistreDiariService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }
    
    public static ArrayList<Imatges> getImatgesbyRegistre(Integer idRegistre) {
        ArrayList<Imatges> r = new ArrayList<Imatges>();

        String sql = "SELECT idimatge,idregistre,tipus,url FROM imatges WHERE idregistre = ?";
        
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            Integer i = 1;
            con = Bdades.getDataSource().getConnection();

            pstm = con.prepareStatement(sql);
            pstm.setInt(i++, idRegistre);
            // pstm.executeUpdate();
            rs = pstm.executeQuery();
            while (rs.next()) {
                Imatges img = new Imatges();
                img.setIdFoto(rs.getInt("idimatge"));
                img.setIdRegistre(rs.getInt("idregistre"));
                img.setTipus(rs.getString("tipus"));
                img.setUrl(rs.getString("url"));
                r.add(img);
            }
        } catch (SQLException sqle) {
            Logger.getLogger(RegistreDiariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    public static boolean updateImatgesByRegistre(Integer idImatge, String url) {
        boolean r = false;

        String sql = "UPDATE imatges SET url = ? WHERE idimatge = ?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, url);
            pstm.setInt(2, idImatge);
            pstm.executeUpdate();
            r = true;
            HistoricCanvisService.insertHistoric(idImatge, HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, "Imatge actualitzada");
        } catch (SQLException ex) {
            Logger.getLogger(RegistreDiariService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }
    
    public static Integer ultimRegistre() {
        Integer result = null;
        String sql = "SELECT MAX(idregistre) AS registre FROM entsor";

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                result = rs.getInt("registre");
            }
        } catch (SQLException ex) {
            Logger.getLogger(RegistreDiariService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Bdades.closeConnections(con, stmt, rs);
        }
     
        return result;
    }

//    public static RegistreDiari obtenir(Integer idRegistre, Integer idEmpresa, 
//            Integer idVehicle, Date entrada, Double pesEntrada) {
//        return new RegistreDiari(idRegistre, idEmpresa, idVehicle, entrada, pesEntrada);
//    }
    
    public static RegistreDiariDTO obtenir(Integer idRegistre, Integer idEmpresa, 
            String nomEmpresa, String idFiscal, String telefon, Integer idVehicle, 
            String matricula, Timestamp entrada, Double pesEntrada) {
        return new RegistreDiariDTO(idRegistre, idEmpresa, nomEmpresa, idFiscal, telefon, idVehicle, matricula, entrada, pesEntrada);
    }
    
    public static RegistreDiariDTO obtenirSortida(Integer idRegistre, Integer idEmpresa, 
            String nomEmpresa, String idFiscal, String telefon, Integer idVehicle, 
            String matricula, Timestamp entrada, Double pesEntrada, Timestamp sortida, 
            Double pesSortida, Integer temps, Double pesDiferencia) {
        return new RegistreDiariDTO(idRegistre, idEmpresa, nomEmpresa, idFiscal, 
                telefon, idVehicle, matricula, entrada, pesEntrada, sortida, pesSortida, 
                temps, pesDiferencia);
    }
}
