/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.vehicles;

import controladors.base.CITServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Vehicles;
import models.services.VehiclesService;
import util.commons.UtilsGrid;

/**
 *
 * @author Abraham M
 */
public class VehiclesGetController extends CITServlet {

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {
                response.setContentType("text/xml;charset=UTF-8");
                response.setContentType("application/xml");
                addDiccionari("vehicles");
                setNua();

                ArrayList<Vehicles> llista = VehiclesService.getVehicles();

                PrintWriter out = response.getWriter();
                out.print("<?xml version='1.0' encoding='UTF-8'?>");
                out.print("<rows>");
                for (Vehicles v : llista) {
                    out.print(UtilsGrid.toXMLPK(v.getIdVehicle() + ""));
                    out.print(UtilsGrid.toXML(v.getIdVehicle() + ""));
                    out.print(UtilsGrid.toXML(v.getMatricula() + ""));
                    out.print(UtilsGrid.toXML(v.getTipus() + ""));
                    out.print(UtilsGrid.toXML(v.getTara() + ""));
                    out.print(UtilsGrid.toXMLend());
                }
                out.print("</rows>");

                super.processRequest(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(VehiclesGetController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}