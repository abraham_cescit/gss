/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.vehicles;

import controladors.base.CITServlet;
import static controladors.base.CITServlet.TIPUS_BREU_XML;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.services.VehiclesService;

/**
 *
 * @author Abraham M
 */
public class VehicleDeleteController extends CITServlet {

    public static final String ESBORRAT_EXIT="ESBORRAT_EXIT";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String resposta=INDEFINIDA;
        try {
            if (inicialitzar(request, response, this)) {
                request.setCharacterEncoding("UTF-8");
                setTipus(TIPUS_BREU_XML);
                addDiccionari("vehicles");

                String stIdVehicle=request.getParameter("id");
                try{
                    if(stIdVehicle!=null && !stIdVehicle.equals("") & Integer.parseInt(stIdVehicle)>0){
                        int idVehicle=Integer.parseInt(stIdVehicle);
                        if(VehiclesService.deleteVehicle(idVehicle)){
                            resposta=ESBORRAT_EXIT;
                        }
                        else{
                            resposta=ERROR;
                        }
                    }
                }
                catch(NumberFormatException nfe){
                    resposta=ERROR;
                }
                finally{
                    afegirRespostaBreu(RESULTAT,resposta);
                    afegirRespostaBreu(MISSATGE,getTexte(resposta));
                    super.processRequest(request, response);
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(VehicleDeleteController.class.getName()).log(Level.SEVERE, null, ex);
        }

        super.processRequest(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
