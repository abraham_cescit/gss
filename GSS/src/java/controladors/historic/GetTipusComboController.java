/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.historic;

import controladors.base.CITServlet;
import controladors.usuaris.UsuarisGetController;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.services.HistoricCanvisService;

/**
 *
 * @author Sònia
 */
public class GetTipusComboController extends CITServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {
                
                setXML();
                
                ArrayList<String> llistaTipus = new ArrayList<String>();
                
                llistaTipus = HistoricCanvisService.getTipus();
                
                PrintWriter out = response.getWriter();
                out.print("<?xml version='1.0' encoding='UTF-8'?>");
                out.print("<complete>");
                out.print("<option value=\"\"></option>");
                for (String Tipus : llistaTipus){
                    out.print("<option value=\""+Tipus+"\">"+Tipus+"</option>");
                }
                out.print("</complete>");
                
                super.processRequest(request, response);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarisGetController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
