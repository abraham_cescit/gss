/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.usuaris;

import com.cescit.security.Rols;
import controladors.base.CITServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Usuaris;
import models.services.UsuariService;
import util.commons.UtilsGrid;

/**
 *
 * @author Abraham M
 */
public class UsuarisRecordatoriController extends CITServlet {

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {
                response.setContentType("text/xml;charset=UTF-8");
                response.setContentType("application/xml");
                addDiccionari("usuaris");
                setNua();

                ArrayList<Usuaris> llista = UsuariService.getUsuarisRecordatori();

                PrintWriter out = response.getWriter();
                out.print("<?xml version='1.0' encoding='UTF-8'?>");
                out.print("<rows>");
                if ( llista.size() > 0 ){
                    for (Usuaris u : llista) {
                        out.print(UtilsGrid.toXMLPK(u.getIdUsuari() + ""));
                        out.print(UtilsGrid.toXML(u.getIdUsuari() + ""));
                        out.print(UtilsGrid.toXML(u.getNom() + ""));
                        out.print(UtilsGrid.toXML(u.getEmail() + ""));
                        out.print(UtilsGrid.toXML(u.getTelefon() + ""));
                        out.print(UtilsGrid.toXMLend());
                    }
                } else {
                    out.print(UtilsGrid.toXML("No hi ha usuaris" + ""));
                }
                out.print("</rows>");

                super.processRequest(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarisRecordatoriController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
