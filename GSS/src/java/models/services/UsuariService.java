package models.services;

import controladors.base.Idioma;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import models.Usuaris;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Sha256Hash;
import persistencia.Bdades;
import util.commons.LoggerOut;
// *****

/**
 *
 * @author Sònia
 *
 * Representa un usuari basic
 */
public class UsuariService {
    
    private static String NOM_TAULA = "usuaris";

    /**
     * Deuvelve la contraseña encriptada de un usuario para compararla con la
     * que llega para iniciar sesión
     *
     * @param login nombre de usuario
     * @return Devuelve la contraseña encriptada en formato String
     */
    public static String getContrasenya(String login) {
        String contrasenya = "";
        String sql = "select contrasenya from usuaris where login=? ";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, login);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            if (rs.first()) {
                contrasenya = rs.getString("contrasenya");
            }

        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return contrasenya;
    }

    /**
     * Método para devolver el salt de un usuario. Se utiliza para iniciar
     * sesión en CustomSecurityRealm
     *
     * @param login nombre de usuario
     * @return Devuelve el salt de un usuario en formato String
     */
    public static String getSalt(String login) {
        String salt = "";
        String sql = "select salt from usuaris where login=? ";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, login);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            if (rs.first()) {
                salt = rs.getString("salt");
            }

        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return salt;
    }

    public static boolean insertarUsuari(Usuaris u) throws Exception {

        boolean r = false;
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        String sql = "insert into usuaris "
                   + "(idempresa,login,nom,tipus,email,telefon,datanaixement,databaixa,idioma,contrasenya,salt) "
                   + "values ('1',?,?,?,?,?,?,?,?,?,?)";

        try {
            Object salt = generarSalt();
            String contrasenya = encriptarContrasenya(u.getContrasenya(), salt);
            
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            int i = 1;
            pstm.setString(i++, u.getLogin());
            pstm.setString(i++,u.getNom());
            pstm.setInt(i++, u.getTipus());
            pstm.setString(i++, u.getEmail());
            pstm.setString(i++,u.getTelefon());
            pstm.setDate(i++,u.getDatanaixement());
            pstm.setDate(i++, u.getDataBaixa());
            pstm.setString(i++,u.getIdioma());
            pstm.setString(i++, contrasenya);
            pstm.setString(i++, salt.toString());
            pstm.executeUpdate();
            r = true;
            
            HistoricCanvisService.insertHistoric(u.getIdUsuari(), HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, u.toString());
            
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }
    
    public static boolean insertarUsuariExpress (Usuaris u) throws Exception {

        boolean r = false;
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        String sql = "insert into usuaris "
                   + "(idempresa,login,nom,email,telefon,datanaixement,idioma,salt) "
                   + "values ('1',?,?,?,?,?,?,?)";

        try {
            Object salt = generarSalt();
            
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            int i = 1;
            pstm.setString(i++, u.getTelefon());
            pstm.setString(i++,u.getNom());
            pstm.setString(i++, u.getEmail());
            pstm.setString(i++,u.getTelefon());
            pstm.setDate(i++,u.getDatanaixement());
            pstm.setString(i++, u.getIdioma());
            pstm.setString(i++, salt.toString());
            pstm.executeUpdate();
            r = true;
            
            HistoricCanvisService.insertHistoric(u.getIdUsuari(), HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, u.toString());
            
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }
    
    public static boolean updateUsuari(Usuaris u) {
        boolean r = false;
        String sql = "update usuaris set "
                   + "login = ?,"
                   + "nom = ?,"
                   + "tipus = ?,"
                   + "email = ?,"
                   + "telefon = ?,"
                   + "datanaixement = ?,"
                   + "databaixa = ?,"
                   + "idioma = ? "
                   + "where idUsuari = ?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            int i = 1;
            pstm.setString(i++, u.getLogin());
            pstm.setString(i++,u.getNom());
            pstm.setInt(i++, u.getTipus());
            pstm.setString(i++, u.getEmail());
            pstm.setString(i++,u.getTelefon());
            pstm.setDate(i++,u.getDatanaixement());
            pstm.setDate(i++,u.getDataBaixa());
            pstm.setString(i++,u.getIdioma());
            pstm.setInt(i++, u.getIdUsuari());
            pstm.executeUpdate();
            
            if(!u.getContrasenya().equals("")) setPassword(u.getIdUsuari(),u.getContrasenya());
            
            r = true; 
            
            HistoricCanvisService.insertHistoric(u.getIdUsuari(), HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, u.toString());
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarisService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }
    
    public static boolean updateNouUsuari(Usuaris u) {
        boolean r = false;
        String sql = "update usuaris set "
                   + "login = ?,"
                   + "nom = ?,"
                   + "tipus = ?,"
                   + "email = ?,"
                   + "telefon = ?,"
                   + "datanaixement = ?,"
                   + "databaixa = ?,"
                   + "idioma = ? "
                   + "where idUsuari = ?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            int i = 1;
            pstm.setString(i++, u.getLogin());
            pstm.setString(i++,u.getNom());
            pstm.setInt(i++, u.getTipus());
            pstm.setString(i++, u.getEmail());
            pstm.setString(i++,u.getTelefon());
            pstm.setDate(i++,u.getDatanaixement());
            pstm.setDate(i++,u.getDataBaixa());
            pstm.setString(i++,u.getIdioma());
            pstm.setInt(i++, u.getIdUsuari());
            pstm.executeUpdate();
            
            // if(!u.getContrasenya().equals("")) setPassword(u.getIdUsuari(),u.getContrasenya());
            
            r = true; 
            
            HistoricCanvisService.insertHistoric(u.getIdUsuari(), HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, u.toString());
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarisService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    /**
     * Devuelve los datos necesarios para la sesión
     *
     * @param login nombre de usuario
     * @return Devuelve un Usuari con idioma, idusuari, nom y tipus
     */
    public static Usuaris getDadesSessio(String login) {
        Usuaris u = new Usuaris();
        String sql = "select idioma, idusuari, nom, tipus from usuaris where login=? ";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, login);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            if (rs.first()) {
                u.setIdioma(rs.getString("idioma"));
                u.setIdUsuari(rs.getInt("idusuari"));
                u.setNom(rs.getString("nom"));
                u.setTipus(rs.getInt("tipus"));
                u.setLogin(login);
            }

        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return u;
    }

    public String generarToken(Usuaris u) throws SQLException {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        String cs = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        Random ra = new Random(System.currentTimeMillis());
        String cont = "";
        String sql = "insert into recuperaciocontrasenya "
                + "(token,idusuari) values (?,?)";

        con = Bdades.getDataSource().getConnection();
        pstm = con.prepareStatement(sql);
        try {
            pstm = con.prepareStatement(sql);
        } catch (SQLException sqle) {
            System.out.println("Usuari - generarToken - Error de SQL: " + sqle.getMessage());
            sqle.printStackTrace();
        }
        boolean fet = false;
        while (!fet) {// ha d'intentar insertar el token fins que genere un no repetit
            try {
                cont = "";
                for (int i = 0; i < 50; i++) {
                    int n = ra.nextInt(cs.length());
                    cont = cont + cs.charAt(n);
                }
                int iterador = 1;
                pstm.setString(iterador++, cont);
                pstm.setInt(iterador++, u.getIdUsuari());
                pstm.executeUpdate();
                fet = true;
            } catch (SQLException sqle) {
                LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - generarToken - Error de SQL: " + sqle.getMessage());
            } finally {
                Bdades.closeConnections(con, pstm, rs);
            }
        }

        return cont;
    }

    public static String generarContrasenya() {
        String cs = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!$%&*_+=-.,;:<";
        Random ra = new Random(System.currentTimeMillis());
        String cont = "";
        for (int i = 0; i < 10; i++) {
            int n = ra.nextInt(cs.length());
            cont = cont + cs.charAt(n);
        }
        return cont;

    }

    /**
     * Es un metode "factory" Obte el usuari de la classe a la que pertanya.
     * Operari, Admin, etc.
     *
     * @param login nom de login de l'usuari
     * @return L'objecte Usuari corresponent si existeix i null si no existeix
     */
    // no S'utilitza
    public static Usuaris obtenir(String login) {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        Usuaris u = new Usuaris();

        String sql = "select idusuari,login,nom,tipus,email,telefon,datanaixement,idioma from usuaris where login=? ";

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, login);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            if (rs.next()) {
                u.setIdUsuari(rs.getInt("idusuari"));
                u.setLogin(rs.getString("login"));
                u.setNom(rs.getString("nom"));
                u.setTipus(rs.getInt("tipus"));
                u.setEmail(rs.getString("email"));
                u.setTelefon(rs.getString("telefon"));
                u.setDatanaixement(rs.getDate("datanaixement"));
                u.setIdioma(rs.getString("idioma"));
            }
        } catch (SQLException sqle) {
            LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - obtenir - Error de SQL: " + sqle.getMessage());
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return u;
    }
    
    /**
     * Es un metode "factory" Obte el usuari de la classe a la que pertanya.
     * Operari, Admin, etc.
     *
     * @param login nom de login de l'usuari
     * @return L'objecte Usuari corresponent si existeix i null si no existeix
     */
    // no S'utilitza
    public static Usuaris getUserById(Integer idusuari) {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        Usuaris u = new Usuaris();

        String sql = "select idusuari,login,nom,tipus,email,telefon,datanaixement,idioma,contrasenya from usuaris where idusuari=? ";

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idusuari);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            if (rs.next()) {
                u.setIdUsuari(rs.getInt("idusuari"));
                u.setLogin(rs.getString("login"));
                u.setNom(rs.getString("nom"));
                u.setTipus(rs.getInt("tipus"));
                u.setEmail(rs.getString("email"));
                u.setTelefon(rs.getString("telefon"));
                u.setDatanaixement(rs.getDate("datanaixement"));
                u.setContrasenya(rs.getString("contrasenya"));
                u.setIdioma(rs.getString("idioma"));
            }
        } catch (SQLException sqle) {
            LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - obtenir - Error de SQL: " + sqle.getMessage());
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return u;
    }
    
    public static Integer getLastId() {
        
        Integer id = null;
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;
        
        String sql = "select max(idusuari) as idusuari from usuaris";

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(sql);
            rs = stm.getResultSet();
            while (rs.next()) {
                id = rs.getInt("idusuari");
            }
            
        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return id;
    }


    public int getIdUsuari(Usuaris u) {
        return u.getIdUsuari();
    }

    public static Usuaris obtenir(Integer idUsuari, String login, String nom, Integer tipus, String email, String telefon, Date datanaixement, Date databaixa, String idioma) {
        return new Usuaris(idUsuari, login, nom, tipus, email, telefon, datanaixement, databaixa, idioma);
    }

    private static String encriptar(String password) {
        String sha1 = "";
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(password.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return sha1;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }

    /**
     * Actualitza la base de dades amb les dades en les propietats d'aquest
     * objecte.
     * <p>
     * El camp clau es idUsuari
     * @param u usuari
     * @return true si s'ha actualitzat amb exit i false si no.
     */
    public boolean actualitzar(Usuaris u) {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        boolean r = false;

        String sql = "update usuaris set "
                + "email=?,"
                + "nom=?,"
                + "tipus=?,"
                + "idioma=? "
                + "where idusuari=?";

        try {
            int i = 1;
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(i++, u.getEmail());
            pstm.setString(i++, u.getNom());
            pstm.setInt(i++, u.getTipus());
            pstm.setString(i++, u.getIdioma());
            pstm.setInt(i++, u.getIdUsuari());
            pstm.executeUpdate();
            r = true;
        } catch (SQLException sqle) {
            LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - actualitzar - Error de SQL: " + sqle.getMessage());
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    /**
     * Esborra l'usuari de la base de dades
     *
     * @param u usuari
     * @return true si s'ha esborrat amb exit i false si no
     */
    public boolean esborrar(Usuaris u) {
        return esborrar(u.getIdUsuari());
    }

    /**
     * Esborra l'usuari amb aquest idUsuari de la base de dades
     *
     * @param idUsuari es el idUsuari de l'usuari a esborrar.
     *
     * @return true si s'ha esborrat amb exit i false si no
     */
    public static boolean esborrar(int idUsuari) {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        boolean r = false;

        String sql = "delete from usuaris where idusuari=?";

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idUsuari);
            pstm.executeUpdate();
            r = true;
        } catch (SQLException sqle) {
            LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - esborrar - Error de SQL: " + sqle.getMessage());
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    /**
     * Estableix la contrasenya de l'usuri amb aquest idUsuari.
     * <p>
     * Cal notar que malgrat que el que s'emmagatzema a la base de dades es
     * realment un hash de la contrasenya calculat amb sha256 y codificat en
     * hexadecimal, el proces del seu calcul es transparent i el fa la base de
     * dades.
     *
     * @param idUsuari es el idUsuari del usuari la contrasenya del qual es vol
     * canviar.
     * @param contrasenya Nova contrasenya
     */
    public static void setPassword(int idUsuari, String contrasenya) {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        String sql = "update usuaris set contrasenya=?, salt=? where idusuari=?";

        try {
            Object salt = generarSalt();
            String password = encriptarContrasenya(contrasenya, salt);
            
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            int i = 1;
            pstm.setString(i++, password);
            pstm.setString(i++, salt.toString());
            pstm.setInt(i++, idUsuari);
            pstm.executeUpdate();
        } catch (SQLException sqle) {
            LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - setPassword - Error de SQL: " + sqle.getMessage());
            //TODO
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
    }

    /**
     * Comprova si existeix a la base de dades un usuari amb aquest login
     *
     * @param login Login de l'usuari a comprovar.
     * @return true si existeix i false si no existeix a la base de dades.
     */
    public static boolean existeix(String login) {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        boolean r = false;

        String sql = "select login from usuaris where login=?";

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, login);
            rs = pstm.executeQuery();
            r = rs.next();
        } catch (SQLException sqle) {
            LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - existeix - Error de SQL: " + sqle.getMessage());
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    /**
     * Comprova que les dades d'acces son correctes.
     *
     * @param login Nom de login de l'usuari a autenticar
     * @param contrasenya Password de l'usuari a autenticar
     * @return true si les dades d'autenticacio son correctes i false si no ho
     * son
     */
    // no S'utilitza
    public static boolean autenticar(String login, String contrasenya) {

        java.util.logging.Logger logger = java.util.logging.Logger.getLogger(UsuariService.class.getName());

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        boolean r = false;

        if (!contrasenya.trim().equals("")) {
            String q = "select autenticar_usuari(?,?) as autenticat";
            try {
                con = Bdades.getDataSource().getConnection();
                pstm = con.prepareStatement(q);
                int i = 1;
                pstm.setString(i++, login);
                pstm.setString(i++, contrasenya);
                pstm.execute();
                rs = pstm.getResultSet();
                //Comentat perque ha deixat de funcionar
                // rs.next();
                //  r = rs.getBoolean("autenticat");
                r = rs.first();

            } catch (SQLException sqle) {
                LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - autenticar - Error de SQL: " + sqle.getMessage());
            } finally {
                Bdades.closeConnections(con, pstm, rs);
            }
        }
        return r;
    }

    public static ArrayList<Usuaris> getUsuaris() {
        ArrayList<Usuaris> r = new ArrayList<Usuaris>();

        String q = "select idusuari,login,nom,tipus,email,telefon,datanaixement,databaixa,idioma from usuaris ";

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                r.add(UsuariService.obtenir(
                        rs.getInt("idusuari"),
                        rs.getString("login"),
                        rs.getString("nom"),
                        rs.getInt("tipus"),
                        rs.getString("email"),
                        rs.getString("telefon"),
                        rs.getDate("datanaixement"),
                        rs.getDate("databaixa"),
                        rs.getString("idioma")));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }

    public static Usuaris getBuscarEmailUsuari(int idUsuari) {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        Usuaris u = new Usuaris();

        String sql = "select email from usuaris where idusuari=? ";

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idUsuari);
            pstm.executeQuery();
            rs = pstm.getResultSet();
            if (rs.first()) {
                u.setEmail(rs.getString("email"));
            }
        } catch (SQLException sqle) {
            LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - getBuscarEmailUsuari - Error de SQL: " + sqle.getMessage());
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return u;
    }

    public static void enviarInvitacioRecuperarContrasenya(int id) {
        String to = "abcd@gmail.com";
        String from = "web@gmail.com";
        String host = "localhost";
        Properties properties = System.getProperties();

        properties.setProperty("mail.smtp.host", host);
        Session session = Session.getDefaultInstance(properties);

        try {
            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(from));

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            //   message.addRecipient(Message.RecipientType.CC, new InternetAddress("administracio@carnandbeef.com"));

            message.setSubject("This is the Subject Line!");

            message.setContent("<h1>This is actual message</h1>", "text/html");

            Transport.send(message);
        } catch (MessagingException mex) {
            LoggerOut.sendLogger(null, mex, UsuariService.class, "Usuari - enviarInvitacioRecuperarContrasenya - Error de MessagingException: " + mex.getMessage());
            mex.printStackTrace();
        }
    }

    public static boolean resetPassword(String token, String password) {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        boolean r = false;
        int idUsuari = 0;

        String sql = "select comprovar_token_recu_contra(?) as id_usuari";

        try {
            con = Bdades.getDataSource().getConnection();

            pstm = con.prepareStatement(sql);
            pstm.setString(1, token);
            pstm.execute();
            rs = pstm.getResultSet();
            rs.next();
            idUsuari = rs.getInt("id_usuari");

            if (idUsuari != 0) {
                UsuariService.setPassword(idUsuari, password);
                r = true;
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        if (idUsuari > 0 && password != "") {
            setPassword(idUsuari, password);
            r = true;
        }
        return r;
    }

    public static ArrayList<Usuaris> getBuscarNomUsuaris(String idioma) {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        idioma = Idioma.CAT;
        ArrayList<Usuaris> u = new ArrayList<Usuaris>();

        String sql = "select idusuari,nom,login "
                + "from usuaris order by nom asc";

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            while (rs.next()) {
                Usuaris us = new Usuaris();
                us.setIdUsuari(rs.getInt("idusuari"));
                us.setNom(rs.getString("nom"));
                us.setLogin(rs.getString("login"));
                u.add(us);
            }
        } catch (SQLException sqle) {
            LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - getBuscarNomUsuaris - Error de SQL: " + sqle.getMessage());
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return u;
    }

    public String getBuscarNom(int idusuari) {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        String nom = "";
        String sql = "select nom from usuaris where idusuari=? ";

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idusuari);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            if (rs.first()) {
                nom = rs.getString("nom");
            }
        } catch (SQLException sqle) {
            LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - getBuscarNom - Error de SQL: " + sqle.getMessage());
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return nom;
    }

    public String getBuscarEmail(int idusuari) {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        String email = "";
        String sql = "select email from usuaris where idusuari=? ";

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idusuari);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            if (rs.first()) {
                email = rs.getString("email");
            }
        } catch (SQLException sqle) {
            LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - getBuscarEmail - Error de SQL: " + sqle.getMessage());
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return email;
    }

    /**
     * El método genera una cadena única que se utiliza para encriptar la
     * contraseña
     *
     * @return Devuelve un String con el salt
     */
    private static Object generarSalt() {
        RandomNumberGenerator rng = new SecureRandomNumberGenerator();
        Object salt = rng.nextBytes();
        return salt;
    }

    /**
     * Encripta la contraseña en SHA256 junto al salt con 1024 vueltas
     *
     * @param contrasenya contraseña en texto plano
     * @param salt salt generado con el método generarSalt()
     * @return Devuelve la contraseña encriptada
     */
    private static String encriptarContrasenya(String contrasenya, Object salt) {
        return new Sha256Hash(contrasenya, salt, 1024).toBase64();
    }
    
    /**
     * Comprova si l'usuari no s'ha donat de baixa de la web
     *
     * @param login Login de l'usuari a comprovar.
     * @return true si existeix i false si no existeix a la base de dades.
     */
    public static boolean esUsuari(String login) {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        Date baixa = null;
        boolean r = false;

        String sql = "select databaixa from usuaris where login=?";

        try {
            
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, login);
            rs = pstm.executeQuery();
            if (rs.first()) {
                baixa = rs.getDate("databaixa");
            }
            
            if ( baixa == null ){
                r = true;
            } else {
                r = false;
            }
            
        } catch (SQLException sqle) {
            LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - existeix - Error de SQL: " + sqle.getMessage());
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    public static Usuaris getDadesUsuari(int idusuari) {
        Usuaris user = new Usuaris();
        
        String sql = "select login,nom,email,telefon,datanaixement from usuaris where idusuari=?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idusuari);
            rs = pstm.executeQuery();
            if (rs.next()) {
                user.setLogin(rs.getString("login"));
                user.setNom(rs.getString("nom"));
                user.setEmail(rs.getString("email"));
                user.setTelefon(rs.getString("telefon"));
                user.setDatanaixement(rs.getDate("datanaixement"));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return user;
    }
    
    public static Usuaris getDadesNouUsuari(int idusuari) {
        Usuaris user = new Usuaris();
        
        String sql = "select idusuari,login,nom,tipus,email,telefon,datanaixement,databaixa,idioma from usuaris where idusuari=?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idusuari);
            rs = pstm.executeQuery();
            if (rs.next()) {
                user.setIdUsuari(rs.getInt("idusuari"));
                user.setLogin(rs.getString("login"));
                user.setNom(rs.getString("nom"));
                user.setTipus(rs.getInt("tipus"));
                user.setEmail(rs.getString("email"));
                user.setTelefon(rs.getString("telefon"));
                user.setDatanaixement(rs.getDate("datanaixement"));
                user.setIdioma(rs.getString("idioma"));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return user;
    }
    

    public static boolean existeixUsuari(String email) {

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        String emailsql = null;
        boolean r = false;

        String sql = "select email from usuaris where email=?";

        try {
            
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, email);
            rs = pstm.executeQuery();
            if (rs.first()) {
                emailsql = rs.getString("email");
            }
            
            if ( emailsql == null ){
                r = false;
            } else {
                r = true;
            }
            
        } catch (SQLException sqle) {
            LoggerOut.sendLogger(null, sqle, UsuariService.class, "Usuari - existeix - Error de SQL: " + sqle.getMessage());
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }
    
    public static ArrayList<Usuaris> getUsuarisRecordatori() {
        ArrayList<Usuaris> r = new ArrayList<Usuaris>();

        String q = "select * from usuaris where idusuari IN (SELECT idusuari FROM recordatori where date(dhreserva) = date(date_add(NOW(), INTERVAL -6 WEEK)))";
        // String q = "select * from usuaris";
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                r.add(UsuariService.obtenir(
                        rs.getInt("idusuari"),
                        rs.getString("login"),
                        rs.getString("nom"),
                        rs.getInt("tipus"),
                        rs.getString("email"),
                        rs.getString("telefon"),
                        rs.getDate("datanaixement"),
                        rs.getDate("databaixa"),
                        rs.getString("idioma")));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }


}
