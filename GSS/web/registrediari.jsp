<%-- 
    Document   : registrediari
    Created on : 26-ago-2020, 12:49:53
    Author     : Abraham M
--%>

<%@page import="com.cescit.security.Rols"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="util.commons.Utils"%>
<%@page import="controladors.base.Idioma"%>
<%@page import="java.util.ArrayList" %>
<%@page import="models.*" %>
<%@page import="models.services.*" %>
<% Idioma idioma = (Idioma) request.getAttribute("idioma");%>
<!--API VIDEOLOGIC-->
<% String urlAPI = Utils.getProperty("URL");%>
<!--BARRERES-->
<% String urlBarreraEntrada = Utils.getProperty("URL_BARRERA_ENTRADA");%>
<% String urlBarreraSortida = Utils.getProperty("URL_BARRERA_SORTIDA");%>
<!--CAMERES-->
<% String urlCamaraCMR = Utils.getProperty("URL_CMR");%>
<% String urlCamaraDNI = Utils.getProperty("URL_DNI");%>
<% String urlCamaraConductor = Utils.getProperty("URL_CONDUCTOR");%>
<% String urlCargaEntrada = Utils.getProperty("URL_CARGA_ENTRADA");%>
<% String urlCargaSortida = Utils.getProperty("URL_CARGA_SORTIDA");%>
<!--BASCULA-->
<% String urlBascula = Utils.getProperty("URL_BASCULA");%>
<!--CARPETA IMAGENES-->
<% String urlCarpetaImatges = Utils.getProperty("​CARPETA_IMATGES_REGISTRES");%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script>
    // =======================================
    // VARIABLES GLOBALS
    // =======================================
    
    var principalLayout, gridTransit, gridSortida;
    var formRegistre, formEsperaVehicles;
    var formAltaEmpresa, formAltaVehicle;
    var gridEmpreses, gridVehicles;
    var windowVehicleEspera;
    
    var espera = true;
    var linea = null;
    var id_espera = null;
    var id_registre = null;
    var id_empresa = null;
    var matricula = null;
    var hora_entrada = null;
    var pes_entrada = null;
    var hora_sortida = null;
    var pes_sortida = null;
    
    var foto_matricula = null;
    var foto_cmr = "";
    var foto_dni = "";
    var foto_conductor = "";
    var foto_cargaent = "";
    var foto_cargasort = "";
    
    // CONTROLADORS
    var URL_DESA_ENTRADA = "registre-insert-entrada";
    var URL_DESA_SORTIDA = "registre-insert-sortida";
    
    var URL_BUSCAR_ENTRADES = "registre-get";    
    var URL_BUSCAR_SORTIDES = "sortides-get";   
    
    var URL_BUSCAR_EMPRESES = "empreses-get";
    var URL_DESA_EMPRESA = "empresa-insert";
    var URL_ESBORRA_EMPRESA = "empresa-delete";
    
    var URL_BUSCAR_VEHICLES = "vehicles-get";
    var URL_DESA_VEHICLE = "vehicle-insert";
    var URL_ESBORRA_VEHICLE = "vehicle-delete";
    
    var URL_ESBORRA_ESPERA = "esborra-espera";
        
    var URL_COMPROVAR_ESPERES = "comprovar-esperes";
    var URL_PROTOCOL_CAMERA = "camera-getprotocol";
    
    
    // =======================================
    // DEFINICIÓN FORMULARIOS
    // =======================================
    var contentFormRegistre = [
        {type: "settings", position: "label-left", labelWidth: WIDTH_LABEL_FORM, inputWidth: "auto", offsetLeft: 0},
        {type: "hidden", name: "idRegistre", label: "<%= idioma.get("ID_REGISTRE")%>", inputWidth: "auto"},
        {type: "block", width: 470, list:[
            {type: "hidden", name: "idEmpresa", label: "<%=idioma.get("ID_EMPRESA")%>", inputWidth: "auto"},
            {type: "input", name: "nomempresa", label: "<%= idioma.get("NOM_EMPRESA")%>", inputWidth: "auto"},
            {type: "newcolumn"},
            {type: "button", name: "cercaEmpresa", value: "<%= idioma.get("CERCA_EMPRESA")%>", offsetTop: "auto"}
        ]},
        {type: "block", width: 470, list:[
            {type: "hidden", name: "idVehicle", label: "<%=idioma.get("ID_VEHICLE")%>", inputWidth: "auto"},
            {type: "input", name: "matricula", label: "<%=idioma.get("MATRICULA")%>", inputWidth: "auto"},
            {type: "newcolumn"},
            {type: "button", name: "cercaMatricula", value: "<%= idioma.get("CERCA_VEHICLE")%>", offsetTop: "auto"}
        ]},
        {type: "block", width: 470, list:[
            {type: "calendar", name: "hentrada", label: "<%= idioma.get("HORA_ENTRADA")%>", dateFormat: "%Y-%m-%d %H:%i:%s"}
        ]},
        {type: "block", width: 470, list:[
            {type: "input", name: "pesentrada", label: "<%= idioma.get("PES_ENTRADA")%>", inputWidth: "auto"},
            {type: "newcolumn"},
            {type: "button", name: "recollirPesEntrada", value: "<%= idioma.get("PES_ENTRADA")%>", offsetTop: "auto"}
        ]},
        {type: "block", width: 470, list:[
            {type: "calendar", name: "hsortida", label: "<%= idioma.get("HORA_SORTIDA")%>", dateFormat: "%Y-%m-%d %H:%i:%s"}
        ]},
        {type: "block", width: 470, list:[
            {type: "input", name: "pessortida", label: "<%= idioma.get("PES_SORTIDA")%>", inputWidth: "auto"},
            {type: "newcolumn"},
            {type: "button", name: "recollirPesSortida", value: "<%= idioma.get("PES_SORTIDA")%>", offsetTop: "auto"}
        ]},
        {type: "block", width: 470, list:[
            {type: "hidden", id: "idEspera", width: '*', sort: "int", align: "right"},
            {type: "hidden", id: "linea", width: '*', sort: "int", align: "right"},
            {type: "button", name: "recollirMatricula", value: "<%= idioma.get("FOTO_MATRICULA")%>", offsetTop: "auto"},
            {type: "hidden", id: "foto_mtr", width: '0', sort: "str", align: "center"},
            {type: "button", name: "recollirCMR", value: "<%= idioma.get("FOTO_CMR")%>", offsetTop: "auto"},
            {type: "button", name: "recollirDni", value: "<%= idioma.get("FOTO_DNI")%>", offsetTop: "auto"},
            {type: "button", name: "recollirFotoConductor", value: "<%= idioma.get("FOTO_CONDUCTOR")%>", offsetTop: "auto"},
            {type: "button", name: "recollirFotoCargaEntrada", value: "<%= idioma.get("FOTO_CARGA_ENTRADA")%>", offsetTop: "auto"},
            {type: "button", name: "recollirFotoCargaSortida", value: "<%= idioma.get("FOTO_CARGA_SORTIDA")%>", offsetTop: "auto"},
            {type: "button", name: "desaRegistre", value: "<%= idioma.get("DESA")%>", offsetTop: "auto"},
            {type: "newcolumn", offset: 25},
            {type:"checkbox", name:"ch_matricula", label:"<%= idioma.get("FOTO_DESADA")%>", disabled: true},
            {type:"checkbox", name:"ch_cmr", label:"<%= idioma.get("FOTO_DESADA")%>", disabled: true},
            {type:"checkbox", name:"ch_dni", label:"<%= idioma.get("FOTO_DESADA")%>", disabled: true},
            {type:"checkbox", name:"ch_conductor", label:"<%= idioma.get("FOTO_DESADA")%>", disabled: true},
            {type:"checkbox", name:"ch_cargaEntrada", label:"<%= idioma.get("FOTO_DESADA")%>", disabled: true},
            {type:"checkbox", name:"ch_cargaSortida", label:"<%= idioma.get("FOTO_DESADA")%>", disabled: true}
        ]} 
        
    ];
    
    var contentFormRegistreFotos = [
        {type: "settings", position: "label-left", labelWidth: "200", inputWidth: "auto", offsetLeft: 20},
        {type: "label", name: "fotoEntrada", label: "<%= idioma.get("FOTO_MATRICULA")%>", inputWidth: "auto"},
        {type: "container", name:"contenidorMatricula", inputWidth: 320, inputHeight: 240},
        {type: "label", name: "fotoSortida", label: "<%= idioma.get("FOTO_CMR")%>", inputWidth: "auto"},
        {type: "container", name:"contenidorCMR", inputWidth: 320, inputHeight: 240},
        {type: "newcolumn"},
        {type: "label", name: "fotoSortida", label: "<%= idioma.get("FOTO_DNI")%>", inputWidth: "auto"},
        {type: "container", name:"contenidorDNI", inputWidth: 320, inputHeight: 240},
        {type: "label", name: "fotoSortida", label: "<%= idioma.get("FOTO_CONDUCTOR")%>", inputWidth: "auto"},
        {type: "container", name:"contenidorConductor", inputWidth: 320, inputHeight: 240},
        {type: "newcolumn"},
        {type: "label", name: "fotoSortida", label: "<%= idioma.get("FOTO_CARGA_ENTRADA")%>", inputWidth: "auto"},
        {type: "container", name:"contenidorCargaEnt", inputWidth: 320, inputHeight: 240},
        {type: "label", name: "fotoSortida", label: "<%= idioma.get("FOTO_CARGA_SORTIDA")%>", inputWidth: "auto"},
        {type: "container", name:"contenidorCargaSort", inputWidth: 320, inputHeight: 240}
    ];
    
    var contentFormAltaEmpresa = [
        {type: "settings", position: "label-left", labelWidth: WIDTH_LABEL_FORM, inputWidth: "auto", offsetLeft: OFFSET_LEFT_FORM},
        {type: "hidden", name: "idEmpresa", label: "<%=idioma.get("ID_EMPRESA")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "nom", label: "<%=idioma.get("NOM")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "idFiscal", label: "<%=idioma.get("ID_FISCAL")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "adreca", label: "<%=idioma.get("ADRECA")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "cpostal", label: "<%=idioma.get("CPOSTAL")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "poblacio", label: "<%=idioma.get("POBLACIO")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "pais", label: "<%=idioma.get("PAIS")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "telefon", label: "<%=idioma.get("TELEFON")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "contacte", label: "<%=idioma.get("CONTACTE")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "email", label: "<%=idioma.get("EMAIL")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "web", label: "<%=idioma.get("WEB")%>", inputWidth: INPUT_MEDIUM},
        {type: "button", name: "desa", value: "<%=idioma.get("DESA")%>", offsetTop: OFFSET_LEFT_FORM}
    ];
    
    var contentFormAltaVehicle = [
        {type: "settings", position: "label-left", labelWidth: WIDTH_LABEL_FORM, inputWidth: "auto", offsetLeft: OFFSET_LEFT_FORM},
        {type: "hidden", name: "idVehicle", label: "<%=idioma.get("ID_VEHICLE")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "matricula", label: "<%=idioma.get("MATRICULA")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "tipus", label: "<%=idioma.get("TIPUS")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "tara", label: "<%=idioma.get("TARA")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "button", name: "desa", value: "<%=idioma.get("DESA")%>", offsetTop: OFFSET_LEFT_FORM}
    ];
    
    var contentFormCamera = [
        {type: "settings", position: "label-left", labelWidth: "auto", inputWidth: "auto", offsetLeft: 20},
        {type: "container", name:"contenidorVideo", inputWidth: 640, inputHeight: 480},
        {type: "button", name: "desa", value: "<%=idioma.get("DESA")%>", offsetTop: OFFSET_LEFT_FORM}
    ];
    
    var contentFormEsperaVehicles = [
        {type: "settings", position: "label-left", labelWidth: WIDTH_LABEL_FORM, inputWidth: "auto", offsetLeft: OFFSET_LEFT_FORM},
        {type: "input", name: "linea", label: "<%=idioma.get("LINEA")%>"},
        {type: "input", name: "matricula", label: "<%=idioma.get("MATRICULA")%>"},
        {type: "input", name: "hentrada", label: "<%=idioma.get("HORA_ENTRADA")%>"},
        {type: "image", name: "fotoEspera", label: "<%= idioma.get("FOTO_CARGA")%>", inputWidth: "auto"},
        {type: "input", name: "registrat", label: "<%=idioma.get("REGISTRAT")%>"},
        {type: "newcolumn"},
        {type: "button", name: "registrar", value: "<%=idioma.get("REGISTRAR_VEHICLE")%>", offsetTop: OFFSET_LEFT_FORM}
    ];
    
    // =======================================
    // DEFINICI�N GRID
    // =======================================
    var contentGridTransit = {
        image_path: "codebase-pro/imgs/",
        columns: [
            {label: "<%= idioma.get("ID_REGISTRE")%>", id: "idRegistre", width: '100', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("HORA_ENTRADA")%>", id: "hentrada", width: '100', type: "ro", sort: "int", align: "center"},
            {label: "<%= idioma.get("HORA_ENTRADA")%>", id: "hores", width: '100', type: "ro", sort: "int", align: "center"},
            {label: "<%= idioma.get("ID_VEHICLE")%>", id: "idVehicle", width: '100', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("MATRICULA")%>", id: "matricula", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("ID_EMPRESA")%>", id: "idEmpresa", width: '100', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("NOM_EMPRESA")%>", id: "nomempresa", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("ID_FISCAL")%>", id: "idFiscal", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("TELEFON")%>", id: "telefon", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("PES_ENTRADA")%>", id: "pesentrada", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_mtr", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_cmr", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_dni", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_conductor", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_cargaent", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_cargasort", width: '*', type: "ro", sort: "str", align: "right"}
            ],
        multiselect: false
    };
    
    var contentGridSortida = {
        image_path: "codebase-pro/imgs/",
        columns: [
            {label: "<%= idioma.get("ID_REGISTRE")%>", id: "idRegistre", width: '100', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("HORA_ENTRADA")%>", id: "horaEntrada", width: '70', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("HORA_ENTRADA")%>", id: "horesEntrada", width: '100', type: "ro", sort: "int", align: "center"},
            {label: "<%= idioma.get("ID_VEHICLE")%>", id: "idVehicle", width: '100', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("MATRICULA")%>", id: "matricula", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("ID_EMPRESA")%>", id: "idEmpresa", width: '100', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("NOM_EMPRESA")%>", id: "nomempresa", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("ID_FISCAL")%>", id: "idFiscal", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("PES_ENTRADA")%>", id: "pesEntrada", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("HORA_SORTIDA")%>", id: "horaSortida", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("HORA_SORTIDA")%>", id: "horesSortida", width: '*', type: "ro", sort: "str", align: "center"},
            {label: "<%= idioma.get("PES_SORTIDA")%>", id: "pessortida", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("DIF_TEMPS")%>", id: "difTemps", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("DIF_PES")%>", id: "difPes", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_mtr", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_cmr", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_dni", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_conductor", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_cargaent", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_cargasort", width: '*', type: "ro", sort: "str", align: "right"}
            ],
        multiselect: false
    };
    
    var contentGridEmpreses = {
        image_path: "codebase-pro/imgs/",
        columns: [
            {label: "<%= idioma.get("ID_EMPRESA")%>", id: "idEmpresa", width: '50', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("NOM")%>", id: "nom", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("ID_FISCAL")%>", id: "idFiscal", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("ADRECA")%>", id: "adreca", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("CPOSTAL")%>", id: "cpostal", width: INPUT_SMALL, type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("POBLACIO")%>", id: "poblacio", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("PAIS")%>", id: "pais", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("TELEFON")%>", id: "telefon", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("CONTACTE")%>", id: "contacte", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("EMAIL")%>", id: "email", width: '*', type: "ro", sort: "int", align: "left"},
            {label: "<%= idioma.get("WEB")%>", id: "web", width: '*', type: "ro", sort: "str", align: "left"}
        ],
        multiselect: false
    };
    
    var contentGridVehicles = {
        image_path: "codebase-pro/imgs/",
        columns: [
            {label: "<%= idioma.get("ID_VEHICLE")%>", id: "idVehicle", width: '80', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("MATRICULA")%>", id: "matricula", width: '200', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("TIPUS")%>", id: "tipus", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("TARA")%>", id: "tara", width: '*', type: "ro", sort: "str", align: "right"}
        ],
        multiselect: false
    };
    
    var contentGridEsperes = {
        image_path: "codebase-pro/imgs/",
        columns: [
            {label: "<%= idioma.get("ID_ESPERA")%>", id: "idEspera", width: '*', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("LINEA")%>", id: "linea", width: '45', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("MATRICULA")%>", id: "matricula", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("HORA_ENTRADA")%>", id: "hentrada", width: '150', type: "ro", sort: "int", align: "center"},
            {label: "<%= idioma.get("REGISTRAT")%>", id: "registrat", width: '65', type: "ro", sort: "str", align: "center"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_mtr", width: '0', type: "ro", sort: "str", align: "center"}
        ],
        multiselect: false
    };

    
    function doOnLoad() {
        principalLayout = new dhtmlXLayoutObject({
            parent: "layoutRegistreDiari",
            pattern: "3J",
            cells: [
                {id: "a", text: "<%= idioma.get("TRANSIT")%>"},
                {id: "b", text: "<%= idioma.get("REGISTRE")%>"},
                {id: "c", text: "<%= idioma.get("SORTIDA")%>"}
            ]
        });
        
        principalLayout.cells("b").setWidth(480);
        principalLayout.cells("a").setHeight(250);
        
        // =======================================
        // TOOLBAR PRINCIPAL
        // =======================================
        toolBarEntrada = principalLayout.attachToolbar();
        toolBarEntrada.addButton("barrera", 0, "<%= idioma.get("BARRERA_NO_VEHICLES")%>");
        toolBarEntrada.addSeparator("sep1", 1);
        toolBarEntrada.setIconset("awesome");
        toolBarEntrada.addButton("obrirBarreraEntrada", 2, "<%= idioma.get("OBRIR_BARRERA_ENTRADA")%>", "fa fa-car", null);
        toolBarEntrada.setItemToolTip("obrirBarreraEntrada", "<%= idioma.get("OBRIR_BARRERA_TOOLTIP")%>");
        toolBarEntrada.addButton("obrirBarreraSortida", 3, "<%= idioma.get("OBRIR_BARRERA_SORTIDA")%>", "fa fa-car", null);
        toolBarEntrada.setItemToolTip("obrirBarreraSortida", "<%= idioma.get("OBRIR_BARRERA_TOOLTIP")%>");
//        toolBarEntrada.setAlign("center");
        toolBarEntrada.disableItem("barrera");
        toolBarEntrada.attachEvent("onClick", function (id) {
            if (id === "obrirBarreraEntrada") {
                obrirBarrera("entrada");
            }
            if (id === "obrirBarreraSortida") {
                obrirBarrera("sortida");
            }
            if (id === "barrera") {
                mostraVehiclesEspera();
            }
        });
        
        // =======================================
        // GRID TRANSIT
        // =======================================
        gridTransit = principalLayout.cells("a").attachGrid(contentGridTransit);
        gridTransit.load(URL_BUSCAR_ENTRADES);
        gridTransit.setColumnHidden(0, true);
        gridTransit.setColumnHidden(1, true);
        gridTransit.setColumnHidden(3, true);
        gridTransit.setColumnHidden(5, true);
        gridTransit.setColumnHidden(10, true);
        gridTransit.setColumnHidden(11, true);
        gridTransit.setColumnHidden(12, true);
        gridTransit.setColumnHidden(13, true);
        gridTransit.setColumnHidden(14, true);
        gridTransit.setColumnHidden(15, true);
        gridTransit.attachFooter(",,#stat_count,,,,,,,#stat_total",["", "", "text-align:right;font-size:16px;font-weight: bold;", "", "", "", "", "", "", "text-align:right;font-size:16px;font-weight: bold;"]);
        inicialitzarGrid(gridTransit);
        
        // =======================================
        // GRID SORTIDA
        // =======================================
        gridSortida = principalLayout.cells("c").attachGrid(contentGridSortida);
        gridSortida.attachHeader(",,#text_filter,,#combo_filter,,#combo_filter,#text_filter,#text_filter,,#text_filter,#text_filter,#text_filter,#text_filter");
        gridSortida.load(URL_BUSCAR_SORTIDES);
        gridSortida.setColumnHidden(0, true);
        gridSortida.setColumnHidden(1, true);
        gridSortida.setColumnHidden(3, true);
        gridSortida.setColumnHidden(5, true);
        gridSortida.setColumnHidden(9, true);
        gridSortida.setColumnHidden(14, true);
        gridSortida.setColumnHidden(15, true);
        gridSortida.setColumnHidden(16, true);
        gridSortida.setColumnHidden(17, true);
        gridSortida.setColumnHidden(18, true);
        gridSortida.setColumnHidden(19, true);
        gridSortida.attachFooter(",,#stat_count,,,,,,,,,,,#stat_total",["", "", "text-align:right;font-size:16px;font-weight: bold;", "", "", "", "", "", "", "", "", "", "", "text-align:center;font-size:16px;font-weight: bold;"]);
        inicialitzarGrid(gridSortida);

        // =======================================
        // FORMULARI REGISTRES
        // =======================================
        formRegistre = principalLayout.cells("b").attachForm();
        formRegistre.loadStruct(contentFormRegistre);
        formRegistre.bind(gridTransit);
        formRegistre.hideItem("recollirFotoCargaSortida");
        formRegistre.hideItem("ch_cargaSortida");
        
        gridTransit.attachEvent("onRowSelect", function (id, ind) {
            comprobarChecksFotos(gridTransit, id);
            formRegistre.showItem("recollirFotoCargaSortida");
            formRegistre.showItem("ch_cargaSortida");
        });
        
        gridSortida.attachEvent("onRowSelect", function (id, ind) {
//            comprobarChecksFotos(gridSortida, id);
            formRegistre.clear();
            formRegistre.hideItem("recollirFotoCargaSortida");
            formRegistre.hideItem("ch_cargaSortida");
        });
                
        formRegistre.attachEvent("onButtonClick", function (nom) {
            if (nom === "cercaEmpresa") {
                mostraCercaEmpresa();
            }  else if (nom === "recollirPesEntrada") {
                recollirPes("entrada");
            } else if (nom === "recollirPesSortida") {
                recollirPes("sortida");
            } else if (nom === "cercaMatricula") {
                mostraCercaVehicle();
            } else if (nom === "recollirMatricula") {
                getProtocol("matricula");
//                getImatgeCamera("<%=urlCamaraCMR%>", "matricula");
            } else if (nom === "recollirCMR") {
                getProtocol("cmr");
            } else if (nom === "recollirDni") {
                getProtocol("dni");
//                getImatgeCamera("<%=urlCamaraDNI%>", "dni");
            } else if (nom === "recollirFotoConductor") {
                getProtocol("conductor");
//                getImatgeCamera("<%=urlCamaraConductor%>", "conductor");
            } else if (nom === "recollirFotoCargaEntrada") {
                getProtocol("cargaent");
//                getImatgeCamera("<%=urlCargaEntrada%>", "cargaent");
            } else if (nom === "recollirFotoCargaSortida") {
                getProtocol("cargasort");
//                getImatgeCamera("<%=urlCargaSortida%>", "cargasort");
            } else if (nom === "desaRegistre") {
                if(formRegistre.getItemValue("hsortida") === null){
                    insertarEntrada();
                } else {
                    insertarSortida();
                }
            }
        });
        
        var calEntrada = formRegistre.getCalendar("hentrada");
        calEntrada.showTime();
        var calSortida = formRegistre.getCalendar("hsortida");
        calSortida.showTime();
        
        // =======================================
        // TOOLBAR REGISTRES
        // =======================================
        
        toolBar = principalLayout.cells("b").attachToolbar();
        toolBar.setIconset("awesome");
        toolBar.addButton("nou", 1, "", "fas fa-plus", null);
        toolBar.setItemToolTip("nou", "<%= idioma.get("CREAR_REGISTRE_TOOLTIP")%>");
        toolBar.addButton("fotos", 2, "", "fas fa-camera", null);
        toolBar.setItemToolTip("fotos", "<%= idioma.get("FOTOS_REGISTRE_TOOLTIP")%>");
        
        toolBar.setIconSize(18);
        toolBar.setAlign("left");

        toolBar.attachEvent("onClick", function (id) {
            if (id === "nou") {
                formRegistre.clear();
            } else if (id === "fotos") {
                var idRegistre = formRegistre.getItemValue("idRegistre");
                if(idRegistre === ""){
                    dhtmlx.message({
                        type: "error",
                        text: "<%= idioma.get("ERROR_REGISTRE_BUIT")%>",
                        expire: "10000"
                    });
                } else {
                    obrirWindowFotosRegistre(idRegistre);
                }
            }
        });

        // =======================================
        // CERCA I ALTA D'EMPRESA
        // =======================================
        
        function mostraCercaEmpresa() {
            windowCercaEmpresa = new dhtmlXWindows().createWindow("windowCercaEmpresa", 0, 0, WINDOWS_WIDTH_FULL, WINDOWS_HEIGHT_FULL);
            windowCercaEmpresa.setText("<%= idioma.get("TITOL_CERCA_EMPRESA")%>");
            inicializarWindowWithClose(windowCercaEmpresa);

            var cercaEmpresaLayout = windowCercaEmpresa.attachLayout({
                pattern: "2U",
                cells: [
                    {id: "a", text: "<%= idioma.get("LLISTAT_D_EMPRESES")%>"},
                    {id: "b", text: "<%= idioma.get("EDICIO_D_EMPRESA")%>"}
                ]
            });

            cercaEmpresaLayout.cells("b").setWidth(350);

            gridEmpreses = cercaEmpresaLayout.cells("a").attachGrid(contentGridEmpreses);
            gridEmpreses.attachHeader("#text_filter,#text_filter,#text_filter,#combo_filter,#text_filter,#text_filter,#text_filter,#combo_filter,#text_filter");
            gridEmpreses.load(URL_BUSCAR_EMPRESES);
            inicialitzarGrid(gridEmpreses);

            gridEmpreses.attachEvent("onRowSelect", function (id, ind) {
                var idEmpresa = gridEmpreses.cells(id, gridEmpreses.getColIndexById("idEmpresa")).getValue();
                var nomEmpresa = gridEmpreses.cells(id, gridEmpreses.getColIndexById("nom")).getValue();
                formRegistre.setItemValue("idEmpresa", idEmpresa);
                formRegistre.setItemValue("nomempresa", nomEmpresa);
            });

            gridEmpreses.attachEvent("onRowDblClicked", function (id, cInd) {
                var idEmpresa = gridEmpreses.cells(id, gridEmpreses.getColIndexById("idEmpresa")).getValue();
                var nomEmpresa = gridEmpreses.cells(id, gridEmpreses.getColIndexById("nom")).getValue();
                formRegistre.setItemValue("idEmpresa", idEmpresa);
                formRegistre.setItemValue("nomempresa", nomEmpresa);
                windowCercaEmpresa.close();
            });

            formAltaEmpresa = cercaEmpresaLayout.cells("b").attachForm();
            formAltaEmpresa.loadStruct(contentFormAltaEmpresa);

            formAltaEmpresa.bind(gridEmpreses);
            formAltaEmpresa.attachEvent("onButtonClick", function (nom) {
                if (nom === "desa") {
                    if (formAltaEmpresa.validate()) {
                        cercaEmpresaLayout.progressOn();
                        formAltaEmpresa.send(URL_DESA_EMPRESA, "POST", function (r) {
                            dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: getAjax(r, TEMPS)});
                            gridEmpreses.clearAndLoad(URL_BUSCAR_EMPRESES, function () {
                                cercaEmpresaLayout.progressOff();
                                formAltaEmpresa.clear();
                            });
                        });
                    }
                }
            });

            toolBar = cercaEmpresaLayout.cells("b").attachToolbar();
            toolBar.setIconset("awesome");
            toolBar.addButton("nou", 1, "", "fas fa-plus", null);
            toolBar.setItemToolTip("nou", "<%= idioma.get("CREAR_EMPRESA_TOOLTIP")%>");
            toolBar.addButton("esborrar", 2, "", "fas fa-trash", null);
            toolBar.setItemToolTip("esborrar", "<%= idioma.get("ESBORRAR_EMPRESA_TOOLTIP")%>");
            toolBar.setIconSize(18);
            toolBar.setAlign("left");

            toolBar.attachEvent("onClick", function (id) {
                if (id === "nou") {
                    formAltaEmpresa.clear();
                }
                if (id === "esborrar") {
                    var id = gridEmpreses.getSelectedRowId();
                    if (id !== null) {
                        dhtmlx.confirm({
                            title: "<%= idioma.get("CONFIRMAR")%>",
                            type: "confirm-warning",
                            text: "<%= idioma.get("CONFIRMACIO_ESBORRAT_EMPRESA")%>",
                            ok: "<%= idioma.get("ACCEPTAR")%>",
                            cancel: "<%= idioma.get("CANCELAR")%>",
                            callback: function (r) {
                                if (r) {
                                    window.dhx.ajax.post(URL_ESBORRA_EMPRESA, "id=" + id, function (r) {
                                        var resultat = getAjax(r, RESULTAT);
                                        if (resultat === ESBORRAT_EXIT) {
                                            gridEmpreses.clearAndLoad(URL_BUSCAR_EMPRESES, function () {
                                                gridEmpreses.filterByAll();
                                            });
                                            formAltaEmpresa.clear();
                                            dhtmlx.message({text: '<%= idioma.get("ESBORRAT_EXIT")%>', expire: "6000 "});
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        dhtmlx.message({
                            text: "<%= idioma.get("ELIMINAR_SELECCIO")%>",
                            expire: "10000"
                        });
                    }
                }
            });
        }
        
        // =======================================
        // CERCA I ALTA DE VEHICLE
        // =======================================
        
        function mostraCercaVehicle(p_matricula) {
            windowCercaVehicle = new dhtmlXWindows().createWindow("windowCercaVehicle", 0, 0, WINDOWS_WIDTH_FULL, WINDOWS_HEIGHT_FULL);
            windowCercaVehicle.setText("<%= idioma.get("TITOL_CERCA_VEHICLE")%>");
            inicializarWindowWithClose(windowCercaVehicle);

            var cercaVehicleLayout = windowCercaVehicle.attachLayout({
                pattern: "2U",
                cells: [
                    {id: "a", text: "<%= idioma.get("LLISTAT_DE_VEHICLES")%>"},
                    {id: "b", text: "<%= idioma.get("EDICIO_DE_VEHICLE")%>"}
                ]
            });

            cercaVehicleLayout.cells("b").setWidth(350);

            gridVehicles = cercaVehicleLayout.cells("a").attachGrid(contentGridVehicles);
            gridVehicles.attachHeader("#text_filter,#text_filter,#text_filter,#combo_filter,#text_filter,#text_filter,#text_filter,#combo_filter,#text_filter");
            gridVehicles.load(URL_BUSCAR_VEHICLES);
            inicialitzarGrid(gridVehicles);

            gridVehicles.attachEvent("onRowSelect", function (id, ind) {
                var idVehicle = gridVehicles.cells(id, gridVehicles.getColIndexById("idVehicle")).getValue();
                var matricula = gridVehicles.cells(id, gridVehicles.getColIndexById("matricula")).getValue();
                formRegistre.setItemValue("idVehicle", idVehicle);
                formRegistre.setItemValue("matricula", matricula);
            });

            gridVehicles.attachEvent("onRowDblClicked", function (id, cInd) {
                var idVehicle = gridVehicles.cells(id, gridVehicles.getColIndexById("idVehicle")).getValue();
                var matricula = gridVehicles.cells(id, gridVehicles.getColIndexById("matricula")).getValue();
                formRegistre.setItemValue("idVehicle", idVehicle);
                formRegistre.setItemValue("matricula", matricula);
                windowCercaVehicle.close();
            });

            formAltaVehicle = cercaVehicleLayout.cells("b").attachForm();
            formAltaVehicle.loadStruct(contentFormAltaVehicle);
            
            formAltaVehicle.bind(gridVehicles);
            formAltaVehicle.attachEvent("onButtonClick", function (nom) {
                if (nom === "desa") {
                    if (formAltaVehicle.validate()) {
                        cercaVehicleLayout.progressOn();
                        formAltaVehicle.send(URL_DESA_VEHICLE, "POST", function (r) {
                            dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: getAjax(r, TEMPS)});
                            gridVehicles.clearAndLoad(URL_BUSCAR_VEHICLES, function () {
                                formAltaVehicle.clear();
                            });
                            cercaVehicleLayout.progressOff();
                        });
                    }
                }
            });
            
            if(p_matricula !== null){
                formAltaVehicle.setItemValue("matricula", p_matricula);
            }
            
            toolBar = cercaVehicleLayout.cells("b").attachToolbar();
            toolBar.setIconset("awesome");
            toolBar.addButton("nou", 1, "", "fas fa-plus", null);
            toolBar.setItemToolTip("nou", "<%= idioma.get("CREAR_VEHICLE_TOOLTIP")%>");
            toolBar.addButton("esborrar", 2, "", "fas fa-trash", null);
            toolBar.setItemToolTip("esborrar", "<%= idioma.get("ESBORRAR_VEHICLE_TOOLTIP")%>");
            toolBar.setIconSize(18);
            toolBar.setAlign("left");

            toolBar.attachEvent("onClick", function (id) {
                if (id === "nou") {
                    formAltaVehicle.clear();
                }
                if (id === "esborrar") {
                    var id = gridVehicles.getSelectedRowId();
                    if (id !== null) {
                        dhtmlx.confirm({
                            title: "<%= idioma.get("CONFIRMAR")%>",
                            type: "confirm-warning",
                            text: "<%= idioma.get("CONFIRMACIO_ESBORRAT_VEHICLE")%>",
                            ok: "<%= idioma.get("ACCEPTAR")%>",
                            cancel: "<%= idioma.get("CANCELAR")%>",
                            callback: function (r) {
                                if (r) {
                                    window.dhx.ajax.post(URL_ESBORRA_VEHICLE, "id=" + id, function (r) {
                                        var resultat = getAjax(r, RESULTAT);
                                        if (resultat === ESBORRAT_EXIT) {
                                            gridVehicles.clearAndLoad(URL_BUSCAR_VEHICLES, function () {
                                                gridVehicles.filterByAll();
                                            });
                                            formAltaVehicle.clear();
                                            dhtmlx.message({text: '<%= idioma.get("ESBORRAT_EXIT")%>', expire: "6000 "});
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        dhtmlx.message({
                            text: "<%= idioma.get("ELIMINAR_SELECCIO")%>",
                            expire: "10000"
                        });
                    }
                }
            });
        }        
        
        // =======================================
        // FUNCIONS 
        // =======================================
        
        function comprobarChecksFotos(grid, id){
            foto_matricula = grid.cells(id, grid.getColIndexById("foto_mtr")).getValue();
            foto_cmr = grid.cells(id, grid.getColIndexById("foto_cmr")).getValue();
            foto_dni = grid.cells(id, grid.getColIndexById("foto_dni")).getValue();
            foto_conductor = grid.cells(id, grid.getColIndexById("foto_conductor")).getValue();
            foto_cargaent = grid.cells(id, grid.getColIndexById("foto_cargaent")).getValue();
            foto_cargasort = grid.cells(id, grid.getColIndexById("foto_cargasort")).getValue();
            
            if(foto_matricula !== null && foto_matricula !== "null" && foto_matricula !== ""){
                formRegistre.setItemValue("ch_matricula", "true");
            }
            if(foto_cmr !== ""){
                formRegistre.setItemValue("ch_cmr", "true");
            }
            if(foto_dni !== ""){
                formRegistre.setItemValue("ch_dni", "true");
            }
            if(foto_conductor !== ""){
                formRegistre.setItemValue("ch_conductor", "true");
            }
            if(foto_cargaent !== ""){
                formRegistre.setItemValue("ch_cargaEntrada", "true");
            }
            if(foto_cargasort !== ""){
                formRegistre.setItemValue("ch_cargaSortida", "true");
            }
        }
        
        function obrirBarrera(posicio) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var opt = this.response;
                    
                }
            };
            var titol = null;
            if (posicio == 'entrada'){
                titol = "Barrera d'" + posicio;
            } else {
                titol = "Barrera de " + posicio;
            }
            
            dhtmlx.confirm({
                title: titol,
                type:"dialogoGSS",
                text: "<%= idioma.get("CONFIRMACIO_OBRIR_BARRERA")%>",
                callback: function(result){
                    if(result){
                        if (posicio == 'entrada'){
                            xhttp.open("POST", "<%=urlBarreraEntrada%>", true);
                            dhtmlx.message({
                                title: "<%= idioma.get("TITOL_BARRERA_OBERTA")%>",
                                type: "alert-warning", 
                                text: "<%= idioma.get("BARRERA_ENTRADA_OBERTA")%>", 
                                expire: 6000});
                        } else {
                            titol = "Barrera de" + posicio;
                            xhttp.open("POST", "<%=urlBarreraSortida%>", true);
                            dhtmlx.message({
                                title: "<%= idioma.get("TITOL_BARRERA_OBERTA")%>",
                                type: "alert-warning", 
                                text: "<%= idioma.get("BARRERA_SORTIDA_OBERTA")%>", 
                                expire: 6000});
                        }
                        xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                        xhttp.send();
                    }
                    
                }
            });
            
            
            
            
        }
        
        function recollirPes(situacio) {
            window.dhx.ajax.get("<%=urlBascula%>", function (r) {
                var result = JSON.parse(r.xmlDoc.responseText);
                if ( result.codigo === 200){
                    var peso = result.mensaje.split("'");
                    if(situacio == 'entrada'){
                        formRegistre.setItemValue("pesentrada", peso[1]);
                    } else {
                        formRegistre.setItemValue("pessortida", peso[1]);
                    }
                } else {
                    dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: getAjax(r, TEMPS)});
                }
            }); 
        }
        
        function insertarEntrada() {
            if (formRegistre.validate()) {
                principalLayout.progressOn();
                var ck_matricula = formRegistre.getItemValue("ch_matricula");
                var ck_cmr = formRegistre.getItemValue("ch_cmr");
                var ck_dni = formRegistre.getItemValue("ch_dni");
                var ck_conductor = formRegistre.getItemValue("ch_conductor");
                var ck_carga = formRegistre.getItemValue("ch_cargaEntrada");
                
                id_empresa = formRegistre.getItemValue("idEmpresa");
                
                if (id_empresa !== "" && ck_matricula === 1 && ck_cmr === 1 && ck_dni === 1 && ck_conductor === 1 && ck_carga === 1 ){
                    var jsonParametres = crearJSONEntrada();
                    window.dhx.ajax.post(URL_DESA_ENTRADA, "dades=" +jsonParametres, function (r) {
                        var result = getAjax(r, RESULTAT);
                        if ( result === "ok"){
                            gridTransit.clearAndLoad(URL_BUSCAR_ENTRADES, function () {
                                dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: getAjax(r, TEMPS)});
                                formRegistre.clear();
                                gridSortida.load(URL_BUSCAR_SORTIDES);
                                inicialitzarGrid(gridSortida);
                                 obrirBarrera("sortida");
                            });
                        } else {
                            dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: getAjax(r, TEMPS)});
                        }
                        principalLayout.progressOff();
                    });    
                } else {
                    dhtmlx.message({type: "error", text: "<%= idioma.get("ERROR_FALTEN_DADES")%>", expire: 10000});
                    principalLayout.progressOff();
                }
            }
        }
        
        function insertarSortida() {
            if (formRegistre.validate()) {
                principalLayout.progressOn();
                
                var ck_carga = formRegistre.getItemValue("ch_cargaSortida");
                
                id_empresa = formRegistre.getItemValue("idEmpresa");
                
                if (ck_carga === 1 ){
                    var jsonParametres = crearJSONSortida();
                    window.dhx.ajax.post(URL_DESA_SORTIDA, "dades=" +jsonParametres, function (r) {
                        var result = getAjax(r, RESULTAT);
                        if ( result === "ok"){
                            gridTransit.clearAndLoad(URL_BUSCAR_ENTRADES, function () {
                                dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: getAjax(r, TEMPS)});
                                formRegistre.clear();
                                gridSortida.load(URL_BUSCAR_SORTIDES);
                                inicialitzarGrid(gridSortida);
                                 obrirBarrera("entrada");
                            });
                        } else {
                            dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: getAjax(r, TEMPS)});
                        }
                        principalLayout.progressOff();
                    });    
                } else {
                    dhtmlx.message({type: "error", text: "<%= idioma.get("ERROR_FALTEN_DADES")%>", expire: 10000});
                    principalLayout.progressOff();
                }
            }
        }
        
        function getImatgeCamera(camera, tipus) {
//            window.dhx.ajax.get(camera, function (r) {
//                var result = JSON.parse(r.xmlDoc.responseText);
//                alert(result);
//            }); 
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var opt = this.response;
                    if (opt == "ERROR") {
                        dhtmlx.message({type: "error", text: "<%= idioma.get("ERROR_VISUALITZAR_CAMERA")%>", expire: 10000});
                    } else {
                        var url = opt["url"];
                        obrirWindowVideo(url, tipus);
                    }
                }
            };

            xhttp.open("GET", camera , true);
            xhttp.responseType = "json";
            xhttp.send();
        }
        
        function obrirWindowFotosRegistre(){
            var imatge_matricula = null;
            var imatge_cmr = null;
            var imatge_dni = null;
            var imatge_conductor = null;
            var imatge_carga_entrada = null;
            var imatge_carga_sortida = null;
            if(foto_matricula !== ""){
                imatge_matricula = "imatgesregistres/imatges-get?a=" + foto_matricula;
            } else {
                imatge_matricula = "imatges/imatges-get?a=logo_07.png";
            }
            if(foto_cmr !== ""){
                imatge_cmr = "imatgesregistres/imatges-get?a=" + foto_cmr;
            } else {
                imatge_cmr = "imatges/imatges-get?a=logo_07.png";
            }
            if(foto_dni !== ""){
                imatge_dni = "imatgesregistres/imatges-get?a=" + foto_dni;
            } else {
                imatge_dni = "imatges/imatges-get?a=logo_07.png";
            }
            if(foto_conductor !== ""){
                imatge_conductor = "imatgesregistres/imatges-get?a=" + foto_conductor;
            } else {
                imatge_conductor = "imatges/imatges-get?a=logo_07.png";
            }
            if(foto_cargaent !== ""){
                imatge_carga_entrada = "imatgesregistres/imatges-get?a=" + foto_cargaent;
            } else {
                imatge_carga_entrada = "imatges/imatges-get?a=logo_07.png";
            }
            if(foto_cargasort !== ""){
                imatge_carga_sortida = "imatgesregistres/imatges-get?a=" + foto_cargasort;
            } else {
                imatge_carga_sortida = "imatges/imatges-get?a=logo_07.png";
            }
            
            winFotos = new dhtmlXWindows().createWindow("winFotos", 0, 0, 1060, 640);
            winFotos.setText("<%= idioma.get("TITOL_FOTOS_REGISTRE")%>");
            inicializarWindowWithClose(winFotos);
            formFotos = winFotos.attachForm();
            formFotos.loadStruct(contentFormRegistreFotos);
            var containerMatricula = formFotos.getContainer("contenidorMatricula");
            containerMatricula.innerHTML = '<img id="camera" width=" '+ 320 + '" height="'+ 240 + '" src="' + imatge_matricula + '" frameborder="0" allowfullscreen></img>';
            var containerCMR = formFotos.getContainer("contenidorCMR");
            containerCMR.innerHTML = '<img id="camera" width=" '+ 320 + '" height="'+ 240 + '" src="' + imatge_cmr + '" frameborder="0" allowfullscreen></img>';
            var containerDNI = formFotos.getContainer("contenidorDNI"); 
            containerDNI.innerHTML = '<img id="camera" width=" '+ 320 + '" height="'+ 240 + '" src="' + imatge_dni + '" frameborder="0" allowfullscreen></img>';
            var containerConductor = formFotos.getContainer("contenidorConductor");
            containerConductor.innerHTML = '<img id="camera" width=" '+ 320 + '" height="'+ 240 + '" src="' + imatge_conductor + '" frameborder="0" allowfullscreen></img>';
            var containerCargaEnt = formFotos.getContainer("contenidorCargaEnt");
            containerCargaEnt.innerHTML = '<img id="camera" width=" '+ 320 + '" height="'+ 240 + '" src="' + imatge_carga_entrada + '" frameborder="0" allowfullscreen></img>';
            var containerCargaSort = formFotos.getContainer("contenidorCargaSort");
            containerCargaSort.innerHTML = '<img id="camera" width=" '+ 320 + '" height="'+ 240 + '" src="' + imatge_carga_sortida + '" frameborder="0" allowfullscreen></img>';
            
        }
        
        function obrirWindowVideo(url, tipus){
            winVideo = new dhtmlXWindows().createWindow("winVideo", 0, 0, 690, 610);
            winVideo.setText("<%= idioma.get("TITOL_VIDEO")%>");
            var idEmpresa = formRegistre.getItemValue("idEmpresa");
            var hora_entrada = formRegistre.getItemValue("hentrada");
            
            inicializarWindowWithClose(winVideo);
            formCamera = winVideo.attachForm();
            formCamera.loadStruct(contentFormCamera);
            var container = formCamera.getContainer("contenidorVideo");
            container.innerHTML = '<img id="camera" width=" '+ 640 + '" height="'+ 480 + '" src="' + "<%=urlAPI%>" + url + '" frameborder="0" allowfullscreen></img>';
            if (idEmpresa === "" && hora_entrada === null){
                formCamera.getItem("desa").disable();
            }
                
            formCamera.attachEvent("onButtonClick", function(name){
                if ( name === "desa"){
                    if (tipus === "matricula") {
                        foto_matricula = url;
                        formRegistre.setItemValue("ch_matricula", "true");
                    } else if (tipus === "cmr") {
                        foto_cmr = url;
                        formRegistre.setItemValue("ch_cmr", "true");
                    } else if (tipus === "dni") {
                        foto_dni = url;
                        formRegistre.setItemValue("ch_dni", "true");
                    } else if (tipus === "conductor") {
                        foto_conductor = url;
                        formRegistre.setItemValue("ch_conductor", "true");
                    } else if (tipus === "cargaent") {
                        foto_cargaent = url;
                        formRegistre.setItemValue("ch_cargaEntrada", "true");
                    } else if (tipus === "cargasort") {
                        foto_cargasort = url;
                        formRegistre.setItemValue("ch_cargaSortida", "true");
                    }
                    winVideo.close();
                }
            });
        }
        
        // Apartat de comunicacio amb el servidor per trobar esperes
        var temps;
        if (espera) {
            espera = false;
            temps = setInterval(comprovacioEsperes, 5000);
        }
        
        function comprovacioEsperes() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var opt = this.response;
                    if(opt !== "no"){
                        toolBarEntrada.setItemText("barrera", "<%= idioma.get("BARRERA_SI_VEHICLES")%>");
                        toolBarEntrada.enableItem("barrera");
//                        paraComprovacio();
//                        mostraVehiclesEspera(opt);
                    } else {
                         toolBarEntrada.setItemText("barrera", "<%= idioma.get("BARRERA_NO_VEHICLES")%>");
                         toolBarEntrada.disableItem("barrera");
                    }
                }
            };
            xhttp.open("GET", URL_COMPROVAR_ESPERES, true);
            xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhttp.send();
        }
        
        function paraComprovacio() {
            clearInterval(temps);
        }
        
        function mostraVehiclesEspera() {
            windowVehicleEspera = new dhtmlXWindows().createWindow("windowVehicleEspera", 0, 0, WINDOWS_WIDTH_SMALL, WINDOWS_HEIGHT_MEDIUM);
            windowVehicleEspera.setText("<%= idioma.get("TITOL_VEHICLES_ESPERA")%>");
            inicializarWindowWithClose(windowVehicleEspera);
            
            gridEsperes = windowVehicleEspera.attachGrid(contentGridEsperes);
            gridEsperes.attachHeader(",,#combo_filter,#text_filter,#combo_filter");
            gridEsperes.load(URL_COMPROVAR_ESPERES);
            gridEsperes.setColumnHidden(0, true);
            gridEsperes.setColumnHidden(5, true);
            inicialitzarGrid(gridEsperes);
            
            formRegistre.bind(gridEsperes);
            gridEsperes.attachEvent("onRowSelect", filaSeleccionada);
        }
        
        function filaSeleccionada(id){
            id_espera = gridEsperes.cells(id, gridEsperes.getColIndexById("idEspera")).getValue();
            var registrat = gridEsperes.cells(id, gridEsperes.getColIndexById("registrat")).getValue();
            foto_matricula = gridEsperes.cells(id, gridEsperes.getColIndexById("foto_mtr")).getValue();
            var mtr = gridEsperes.cells(id, gridEsperes.getColIndexById("matricula")).getValue();
            var toolBar = windowVehicleEspera.attachToolbar();
            
            toolBar.setIconset("awesome");
            toolBar.addButton("esborraEntrada", 0, "<%= idioma.get("ESBORRAR_ENTRADA")%>", "fas fa-trash", null);
            toolBar.setIconSize(18);
            toolBar.setAlign("right");
            toolBar.attachEvent("onClick", function (id) {
                if (id === "esborraEntrada") {
                    // Esborrar registre d'entrada de vehicle
                    dhtmlx.confirm({
                        title: "<%= idioma.get("CONFIRMAR")%>",
                        type: "confirm-warning",
                        text: "<%= idioma.get("CONFIRMACIO_ESBORRAT_ENTRADA")%>",
                        ok: "<%= idioma.get("ACCEPTAR")%>",
                        cancel: "<%= idioma.get("CANCELAR")%>",
                        callback: function (r) {
                            if (r) {
                                window.dhx.ajax.post(URL_ESBORRA_ESPERA, "id=" + id_espera, function (r) {
                                    var resultat = getAjax(r, RESULTAT);
                                    if (resultat == ESBORRAT_EXIT) {
                                        gridEsperes.clearAndLoad(URL_COMPROVAR_ESPERES, function () {
                                            gridEsperes.filterByAll();
                                        });
                                        
                                        dhtmlx.message({text: '<%= idioma.get("ENTRADA_ESBORRAT_EXIT")%>', expire: "6000 "});
                                    }
                                });
                            }
                        }
                    });
                }
            });
            
            if( registrat === "SI"){
                toolBar.addButton("registraEntrada", 1, "<%= idioma.get("REGISTRAR_ENTRADA")%>", "fas fa-paper-plane", null);
                toolBar.setIconSize(18);
                toolBar.setAlign("right");
                toolBar.attachEvent("onClick", function (id) {
                    if (id === "registraEntrada") {
                        espera = true;
                        formRegistre.setItemValue("ch_matricula", "true");
                        obrirBarrera('entrada');
                        windowVehicleEspera.close();
                    }
                });
            } else {
                toolBar.addButton("registraVehicle", 1, "<%= idioma.get("REGISTRAR_VEHICLE")%>", "fas fa-paper-plane", null);
                toolBar.setIconSize(18);
                toolBar.setAlign("right");
                toolBar.attachEvent("onClick", function (id) {
                    if (id === "registraVehicle") {
                        espera = true;
                        formRegistre.setItemValue("ch_matricula", "true");
                        windowVehicleEspera.close();
                        mostraCercaVehicle(mtr);
                    }
                });
            }
        }
        
        function crearJSONEntrada(){
            id_registre = formRegistre.getItemValue("idRegistre");
            matricula = formRegistre.getItemValue("matricula");
            hora_entrada = formRegistre.getCalendar("hentrada");
            pes_entrada = formRegistre.getItemValue("pesentrada");
                            
            var arrayParametres = [id_espera, id_registre, id_empresa, matricula, 
                hora_entrada.getDate(true), pes_entrada, foto_matricula, foto_cmr, 
                foto_dni, foto_conductor, foto_cargaent ];    
            
            return JSON.stringify(arrayParametres);
        }
        
        function crearJSONSortida(){
            id_registre = formRegistre.getItemValue("idRegistre");
            hora_sortida = formRegistre.getCalendar("hsortida");
            pes_sortida = formRegistre.getItemValue("pessortida");
                
            var arrayParametres = [id_registre, hora_sortida.getDate(true), 
                pes_sortida, foto_cargasort ];    
            
            return JSON.stringify(arrayParametres);
        }
        
        function getProtocol(camera){
            window.dhx.ajax.post(URL_PROTOCOL_CAMERA, "nom=" + camera, function (r) {
                var resultat = JSON.parse(r.xmlDoc.responseText);
                if(resultat != 'ERROR'){
                    getImatgeCamera(resultat, camera);
                } else {
                    dhtmlx.message({text: '<%= idioma.get("ERROR_VISUALITZAR_CAMERA")%>', expire: "6000 "});
                }
            });
        }
        
    }
    
    document.addEventListener("DOMContentLoaded", doOnLoad);
</script>

<div id="layoutRegistreDiari" style="width:99.8%;height:90%;overflow: hidden;" ></div>

