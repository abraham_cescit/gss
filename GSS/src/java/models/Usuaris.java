package models;

import java.sql.Date;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import util.commons.Utils;

public class Usuaris {

    public Integer idUsuari;
    public String login;
    public String nom;
    public Integer tipus;
    public String email;
    public String telefon;
    public Date datanaixement;
    public Date databaixa;
    public String idioma;
    public String contrasenya;

    public Usuaris(Integer idUsuari, String login, String nom, Integer tipus, String email, String telefon, Date datanaixement, Date databaixa, String idioma) {
        this.idUsuari = idUsuari;
        this.login = login;
        this.nom = nom;
        this.tipus = tipus;
        this.email = email;
        this.telefon = telefon;
        this.datanaixement = datanaixement;
        this.databaixa = databaixa;
        this.idioma = idioma;
    }
    
    public Usuaris(HttpServletRequest request){
        this.idUsuari = Utils.getParameterInteger(request, "idUsuari");
        this.login = Utils.getParameterString(request, "login");
        this.nom = Utils.getParameterString(request, "nom");
        this.tipus = Utils.getParameterInteger(request, "tipus"); 
        this.email = Utils.getParameterString(request, "email");
        this.telefon = Utils.getParameterString(request, "telefon");        
        this.datanaixement = Utils.getParameterDateSQL(request, "datanaixement");
        this.databaixa = Utils.getParameterDateSQL(request,"databaixa");
        this.idioma = Utils.getParameterString(request, "idioma");
        this.contrasenya = Utils.getParameterString(request, "contrasenya");
    }

    public Usuaris() {

    }

    public Integer getIdUsuari() {
        return idUsuari;
    }

    public void setIdUsuari(Integer idUsuari) {
        this.idUsuari = idUsuari;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getTipus() {
        return tipus;
    }

    public void setTipus(Integer tipus) {
        this.tipus = tipus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public Date getDatanaixement() {
        return datanaixement;
    }

    public void setDatanaixement(Date datanaixement) {
        this.datanaixement = datanaixement;
    }

        public Date getDataBaixa() {
        return databaixa;
    }

    public void setDataBaixa(Date databaixa) {
        this.databaixa = databaixa;
    }
    
    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getContrasenya() {
        return contrasenya;
    }

    public void setContrasenya(String contrasenya) {
        this.contrasenya = contrasenya;
    }    

    public boolean equals(Usuaris u) {
        return u != null
                && Objects.equals(idUsuari, u.idUsuari)
                && login.equals(u.login)
                && nom.equals(u.nom)
                && tipus.equals(u.tipus)
                && email.equals(u.email)
                && telefon.equals(u.telefon)
                && datanaixement.equals(u.datanaixement)
                && databaixa.equals(u.databaixa)
                && idioma.equals(u.idioma)
                && contrasenya.equals(u.contrasenya);
    }

    @Override
    public String toString() {
        String s = "Id:  " + idUsuari
                + "\nLogin: " + login
                + "\nNom: " + nom
                + "\nTipus: " + tipus
                + "\nEmail: " + email
                + "\nTelèfon: " + telefon
                + "\nData naixement: " + datanaixement
                + "\nData baixa: " + databaixa
                + "\nIdioma: " + idioma
                + "\nContrasenya: " + contrasenya;
        return s;
    }

}
