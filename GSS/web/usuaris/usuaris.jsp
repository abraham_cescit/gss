<%@page import="com.cescit.security.Rols"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="util.commons.Utils"%>
<%@page import="controladors.base.Idioma"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="models.*" %>
<%@ page import="models.services.*" %>
<% Idioma idioma = (Idioma) request.getAttribute("idioma");%>

<script>

    // =======================================
    // VARIABLES GLOBALS
    // =======================================
    var usuarisLayout, gridUsuaris, formUsuari, formNouUsuari, win, gridRecordatoriUsuaris, formNouUsuariExpress;
    var winds = new dhtmlXWindows();
    var URL_BUSCAR_USUARIS = "usuaris-get";

    // CONTROLADORS
    var URL_DESA_USUARIS = "usuari-insert";
    var URL_RECORDATORI_USUARIS = "usuari-recordatori"; 
    var URL_ENVIAR_RECORDATORI_USUARIS = "usuari-enviar-recordatori";
    var URL_DESA_USUARIS_EXPRESS = "usuari-insert-express";
    
    // =======================================
    // DEFINICIÓN FORMULARIOS
    // =======================================
    var contentFormEdicio = [
        {type: "settings", position: "label-left", labelWidth: WIDTH_LABEL_FORM, inputWidth: "auto", offsetLeft: OFFSET_LEFT_FORM},
        {type: "hidden", name: "idUsuari", label: "<%=idioma.get("IDUSUARI")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "login", label: "<%=idioma.get("LOGIN1")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "nom", label: "<%=idioma.get("NOM")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "combo", name: "tipus", label: "<%=idioma.get("TIPUS")%>", connector: "tipususuari-getcombo", inputWidth: INPUT_SMALL, required: true, readonly: true},
        {type: "input", name: "email", label: "<%=idioma.get("EMAIL")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "password", name: "contrasenya", label: "<%=idioma.get("CONTRASENYA")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "telefon", label: "<%=idioma.get("TELEFON")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "calendar", name: "datanaixement", label: "<%=idioma.get("DATA_NAIXEMENT")%>", dateFormat: "%Y-%m-%d", serverDateFormat: "%Y-%m-%d"},
        {type: "calendar", name: "databaixa", label: "<%=idioma.get("DATA_BAIXA")%>", dateFormat: "%Y-%m-%d", serverDateFormat: "%Y-%m-%d"},
        {type: "combo", name: "idioma", label: "<%=idioma.get("IDIOMA")%>", connector: "idiomes-getcombo", inputWidth: INPUT_SMALL, required: true, readonly: true},
        {type: "button", name: "desa", value: "<%=idioma.get("DESA")%>", offsetTop: OFFSET_LEFT_FORM}
    ];
    
    var contentFormAltaUsuariExpress = [
        {type: "settings", position: "label-left", labelWidth: WIDTH_LABEL_FORM, inputWidth: "auto", offsetLeft: OFFSET_LEFT_FORM},
        {type: "input", name: "nom", label: "<%=idioma.get("NOM")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "email", label: "<%=idioma.get("EMAIL")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "telefon", label: "<%=idioma.get("TELEFON")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "calendar", name: "datanaixement", label: "<%=idioma.get("DATA_NAIXEMENT")%>", dateFormat: "%Y-%m-%d", serverDateFormat: "%Y-%m-%d"},
        {type: "button", name: "desaAltaExpress", value: "<%=idioma.get("DESA")%>", offsetTop: OFFSET_LEFT_FORM}
    ];

    // =======================================
    // DEFINICIÓN GRID
    // =======================================
    var contentGridUsuaris = {
        image_path: "codebase-pro/imgs/",
        columns: [
            {label: "<%= idioma.get("ID_USUARI")%>", id: "idUsuari", width: '70', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("LOGIN1")%>", id: "login", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("NOM")%>", id: "nom", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("TIPUS")%>", id: "tipus", width: '1', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("TIPUS")%>", id: "tipusNom", width: '*', type: "ro", sort: "int", align: "left"},
            {label: "<%= idioma.get("EMAIL")%>", id: "email", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("TELEFON")%>", id: "telefon", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("DATA_NAIXEMENT")%>", id: "datanaixement", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("IDIOMA")%>", id: "idioma", width: '60', type: "ro", sort: "int", align: "left"},
            {label: "<%= idioma.get("DATA_BAIXA")%>", id: "databaixa", width: '*', type: "ro", sort: "str", align: "left"}
        ],
        multiselect: false
    };
    
    var contentGridRecordatoriUsuaris = {
        image_path: "codebase-pro/imgs/",
        columns: [
            {label: "<%= idioma.get("ID_USUARI")%>", id: "idUsuari", width: '70', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("NOM")%>", id: "nom", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("EMAIL")%>", id: "email", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("TELEFON")%>", id: "telefon", width: '*', type: "ro", sort: "str", align: "left"}
        ],
        multiselect: false
    };

    function doOnLoad() {

        usuarisLayout = new dhtmlXLayoutObject({
            parent: "layoutUsuaris",
            pattern: "2U",
            cells: [
                {id: "a", text: "<%= idioma.get("LLISTAT_DE_USUARIS")%>"},
                {id: "b", text: "<%= idioma.get("EDICIO_DE_USUARI")%>"}
            ]
        });

        usuarisLayout.cells("b").setWidth(350);

        gridUsuaris = usuarisLayout.cells("a").attachGrid(contentGridUsuaris);
        gridUsuaris.attachHeader("#text_filter,#text_filter,#text_filter,,#combo_filter,#text_filter,#text_filter,#text_filter,#combo_filter,#text_filter");
        gridUsuaris.load(URL_BUSCAR_USUARIS);
        inicialitzarGrid(gridUsuaris);

        formUsuari = usuarisLayout.cells("b").attachForm();
        formUsuari.loadStruct(contentFormEdicio);

        formUsuari.bind(gridUsuaris);
        formUsuari.attachEvent("onButtonClick", function (nom) {
            if (nom == "desa") {
                enviar();
            }
        });
        formUsuari.attachEvent("onEnter", enviar);

        function enviar() {
            if (formUsuari.validate()) {
                usuarisLayout.progressOn();
                formUsuari.send(URL_DESA_USUARIS, "POST", function (r) {
                    dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: getAjax(r, TEMPS)});
                    gridUsuaris.clearAndLoad(URL_BUSCAR_USUARIS, function () {
                        usuarisLayout.progressOff();
                    });
                });
            }
        }

        toolBar = usuarisLayout.cells("b").attachToolbar();
        toolBar.setIconset("awesome");
        toolBar.addButton("nou", 1, "", "fas fa-plus", null);
        toolBar.setItemToolTip("nou", "<%= idioma.get("CREAR_USUARI_TOOLTIP")%>");
        toolBar.addButton("esborrar", 2, "", "fas fa-trash", null);
        toolBar.setItemToolTip("esborrar", "<%= idioma.get("ESBORRAR_USUARI_TOOLTIP")%>");
        toolBar.addButton("nou-express", 2, "Alta Rapida", "fa fa-user-plus", null);
        toolBar.setItemToolTip("nou-express", "<%= idioma.get("ALTA_USUARI_EXPRESS")%>");
        toolBar.setIconSize(18);
        toolBar.setAlign("left");


        toolBar.attachEvent("onClick", function (id) {
            if (id === "nou") {
                formUsuari.clear();
            }
            if (id === "esborrar") {
                var id = gridUsuaris.getSelectedRowId();
                if (id !== null) {
                    dhtmlx.confirm({
                        title: "<%= idioma.get("CONFIRMAR")%>",
                        type: "confirm-warning",
                        text: "<%= idioma.get("CONFIRMACIO_ESBORRAT_USUARI")%>",
                        ok: "<%= idioma.get("ACCEPTAR")%>",
                        cancel: "<%= idioma.get("CANCELAR")%>",
                        callback: function (r) {
                            if (r) {
                                window.dhx.ajax.post("usuari-delete", "id=" + id, function (r) {
                                    var resultat = getAjax(r, RESULTAT);
                                    if (resultat == ESBORRAT_EXIT) {
                                        gridUsuaris.clearAndLoad(URL_BUSCAR_USUARIS, function () {
                                            gridUsuaris.filterByAll();
                                        });
                                        formUsuari.clear();
                                        dhtmlx.message({text: '<%= idioma.get("ESBORRAT_EXIT")%>', expire: "6000 "});
                                    }
                                });
                            }
                        }
                    });
                } else {
                    dhtmlx.message({
                        text: "<%= idioma.get("ELIMINAR_SELECCIO")%>",
                        expire: "10000"
                    });
                }
            }
            if (id == "nou-express") {
                mostraFormAltaExpress();
            }
        });

    <%
        String rutaexcel = Utils.getProperty("RUTA_EXCEL");
        String iconoxls = Utils.getProperty("iconoxls");
    %>


        var toolBar = usuarisLayout.cells("a").attachToolbar();
        toolBar.setIconset("awesome");
        toolBar.addButton("exportar", 1, "Exportar", "fas fa-file-excel", null);
        toolBar.setIconSize(18);
        toolBar.setAlign("right");
        
        toolBar.attachEvent("onClick", function (id) {
            if (id === "exportar") {
//                gridUsuaris.toExcel("<%=rutaexcel%>");
            }
        });
        
        function mostraFormAltaExpress() {
            windowAltaExpressUsuaris = new dhtmlXWindows().createWindow("windowAltaExpressUsuaris", 0, 0, WINDOWS_WIDTH_SMALL, WINDOWS_HEIGHT_MEDIUM);
            windowAltaExpressUsuaris.setText("Alta d'Usuaris");
            inicializarWindowWithClose(windowAltaExpressUsuaris);
            
            formNouUsuariExpress = windowAltaExpressUsuaris.attachForm();
            formNouUsuariExpress.loadStruct(contentFormAltaUsuariExpress);
            
            formNouUsuariExpress.attachEvent("onButtonClick", function (nom) {
                if (nom == "desaAltaExpress") {
                    enviarAltaExpress();
                }
            });
        }

        function enviarAltaExpress() {
            if (formNouUsuariExpress.validate()) {
                windowAltaExpressUsuaris.progressOn();
                formNouUsuariExpress.send(URL_DESA_USUARIS_EXPRESS, "POST", function (r) {
                    dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: getAjax(r, TEMPS)});
                    gridUsuaris.clearAndLoad(URL_BUSCAR_USUARIS, function () {
                        windowAltaExpressUsuaris.progressOff();
                    });
                    windowAltaExpressUsuaris.close();
                });
            }
        }
    }

    document.addEventListener("DOMContentLoaded", doOnLoad);
</script>
<div id="layoutUsuaris" style="width:99.8%;height:90%;overflow:hidden" ></div>
<!--<div id="layoutUsuaris" style="width:100%;height:100%;overflow:hidden" ><div id="car"></div></div>
<div id="cnt_nou_usuari">
    <div id="cnt_dlg_nou_usuari"></div>
</div>-->



