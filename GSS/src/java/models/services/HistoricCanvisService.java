/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.services;

import com.cescit.security.ControlAcces;
import models.dto.HistoricDTO;
import models.dto.HistoricUsuariDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.HistoricCanvis;
import persistencia.Bdades;
import util.commons.Utils;

/**
 *
 * @author fernando
 */
public class HistoricCanvisService {
    
    public static String TIPUS_INSERT = "insert";
    public static String TIPUS_DELETE = "delete";
    public static String TIPUS_UPDATE = "update";   
    
    public static boolean insertCanvi(HistoricCanvis hc){
        String sql = "insert into historiccanvis "
                + "(idtaula, nomtaula, idusuari, tipus, valors) "
                + "values (?,?,?,?,?)";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        boolean r = false;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, hc.getIdtaula());
            pstm.setString(2, hc.getNomtaula());
            pstm.setInt(3, hc.getIdusuari());
            pstm.setString(4, hc.getTipus());
            pstm.setString(5, hc.getValors());
            pstm.executeUpdate();
            r = true;
        } catch (SQLException sqle) {
            Logger.getLogger(HistoricCanvisService.class.getName()).log(Level.SEVERE, null, sqle);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }
    
        /**
     * Inserta un registre a la taula historiccanvis
     * @param idtaula - ID de la taula del registre modificat
     * @param tipus - insert, update, delete
     * @param nomtaula - nom de la taula
     * @param valors - Valors de l'objecte en format JSON
     * @return booleà indicant si ha anat be o malament
     */
    public static boolean insertHistoric(Integer idtaula, String tipus, String nomtaula, String valors){
        boolean r = false;
        
        try{
            HistoricCanvis hc = new HistoricCanvis();
            int idusuari = Integer.parseInt(ControlAcces.getAtributoSession("iduser"));
            hc.setIdtaula(idtaula);
            hc.setIdusuari(idusuari);
            hc.setNomtaula(nomtaula);
            hc.setTipus(tipus);
            hc.setValors(valors);
            HistoricCanvisService.insertCanvi(hc);
            r = true;
            
        }catch(Exception e){
            r = false;
        }
        
        return r;
    }
    
    /**
     * Retorna un ArrayList amb els valors de la taula historic canvis amb els filtres que li passem
     * @param desde data inici
     * @param fins data fi
     * @param taula nom de la taula
     * @param usuari usuari
     * @param tipus tipus
     * @return ArrayList
     */
    public static ArrayList<HistoricDTO> getHistoricCanvis(String desde, String fins, String taula, String usuari, String tipus) {
        
        ArrayList<HistoricDTO> listaHistoric = new ArrayList<HistoricDTO>();
        
        String q =  "SELECT " +
                    "h.id AS id, " +
                    "h.data AS data, " +
                    "h.idtaula AS idtaula, " +
                    "h.nomtaula AS nomtaula, " +
                    "h.idusuari AS idusuari, " +
                    "u.login AS nomusuari, " +
                    "h.tipus AS tipus, " +
                    "h.valors AS valors " +
                    "FROM historiccanvis h " +
                    "JOIN usuaris u ON h.idusuari = u.idusuari " +
                    "WHERE 1 = 1 ";
        
        if (!desde.equals("") && !fins.equals(""))
            q += "  AND h.data BETWEEN \""+desde+"\" AND \""+fins+"\"";
        if (!taula.equals(""))
            q += " and h.nomtaula like '%"+taula+"%'";
        if (!usuari.equals(""))
            q += " and h.idusuari like '%"+usuari+"%'";
        if (!tipus.equals(""))
            q += " and h.tipus like '%"+tipus+"%'";
        
        q += " ORDER BY h.data DESC ";
        q += "LIMIT " + Utils.getProperty("NUM_LIMIT_SELECTS");
        
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;
        
        try {
            con = Bdades.getDataSource().getConnection();
            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {                
                HistoricDTO historic = new HistoricDTO();
                historic.setId(rs.getInt("id"));
                historic.setData(rs.getDate("data"));
                historic.setIdtaula(rs.getInt("idtaula"));
                historic.setNomtaula(rs.getString("nomtaula"));
                historic.setIdusuari(rs.getInt("idusuari"));
                historic.setNomusuari(rs.getString("nomusuari"));
                historic.setTipus(rs.getString("tipus"));
                historic.setValors(rs.getString("valors"));
                listaHistoric.add(historic);
            }
        } catch (SQLException sqle) {
            Logger.getLogger(HistoricDTO.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);        
        }
        return listaHistoric;
    }
    
    /**
     * Retorna un ArrayList amb totes les files de la taula historiccanvis
     * @return ArrayList amb tots els valors de la taula
     */
    public static ArrayList<HistoricDTO> getHistoricCanvis() {
        
        ArrayList<HistoricDTO> listaHistoric = new ArrayList<HistoricDTO>();
        
        String q = "select h.id as id, h.data as data, h.idtaula as idtaula, h.nomtaula as nomtaula, h.idusuari as idusuari, u.login as nomusuari, h.tipus as tipus, h.valors as valors " +
                   "from historiccanvis h " +
                   "join usuaris u " +
                   "on h.idusuari = u.idusuari " + 
                   "ORDER BY h.data DESC " +
                   "LIMIT "+Utils.getProperty("NUM_LIMIT_SELECTS");
        
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;
        
        try {
            con = Bdades.getDataSource().getConnection();
            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {                
                HistoricDTO historic = new HistoricDTO();
                historic.setId(rs.getInt("id"));
                historic.setData(rs.getDate("data"));
                historic.setIdtaula(rs.getInt("idtaula"));
                historic.setNomtaula(rs.getString("nomtaula"));
                historic.setIdusuari(rs.getInt("idusuari"));
                historic.setNomusuari(rs.getString("nomusuari"));
                historic.setTipus(rs.getString("tipus"));
                historic.setValors(rs.getString("valors"));
                listaHistoric.add(historic);
            }
        } catch (SQLException sqle) {
            Logger.getLogger(HistoricDTO.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);        
        }
        return listaHistoric;
    }
    
    /**
     * Retorna un ArrayList amb tots els id + nom de les taules que estan en la taula historiccanvis
     * @return ArrayList amb els valors
     */
    public static ArrayList getTaula() {
        
        ArrayList<String> list = new ArrayList<String>();
        
        String q = "select distinct nomtaula from historiccanvis ";
        q += "LIMIT "+Utils.getProperty("NUM_LIMIT_SELECTS");
        
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        
        try {
            con = Bdades.getDataSource().getConnection();
            Statement stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                list.add(rs.getString("nomtaula"));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(HistoricDTO.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);        
        }
        
        return list;
        
    }
    
    /**
     * Retorna un ArrayList amb tot els tipus d'accions ( insert, update, delete, etc )  que estan en la taula historiccanvis
     * @return ArrayList amb els valors
     */
    public static ArrayList<String> getTipus() {
        
        ArrayList<String> list = new ArrayList<String>();
        
        String q = "SELECT DISTINCT tipus FROM historiccanvis;";
        
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;
        
        try {
            con = Bdades.getDataSource().getConnection();
            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                list.add(rs.getString("tipus"));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(HistoricDTO.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);        
        }
        
        return list;

    }
    
    public static ArrayList<HistoricUsuariDTO> getUsuari() {
     
        ArrayList<HistoricUsuariDTO> list = new ArrayList<HistoricUsuariDTO>();
        
        String q = "SELECT DISTINCT u.idusuari as idUsuari, u.login as nomUsuari FROM usuaris u JOIN historiccanvis h on h.idusuari = u.idusuari;";
        
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;
        
        try {
            con = Bdades.getDataSource().getConnection();
            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                HistoricUsuariDTO taula = new HistoricUsuariDTO();
                taula.setIdUsuari(rs.getInt("idUsuari"));
                taula.setNomUsuari(rs.getString("nomUsuari"));
                list.add(taula);
            }
        } catch (SQLException sqle) {
            Logger.getLogger(HistoricUsuariDTO.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);        
        }
        
        return list;
        
    }
    
}
