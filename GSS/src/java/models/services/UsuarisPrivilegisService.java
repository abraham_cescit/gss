package models.services;

import models.dto.HistoricDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.UsuarisPrivilegis;
import persistencia.Bdades;

public class UsuarisPrivilegisService {

    private static final String NOM_TAULA = "usuarisprivilegis";

    /**
     * Retorna un ArrayList amb un x nombre de files ( x = numero ) de la taula
     * usuarisprivilegis
     *
     * @return ArrayList llista dels privilegis
     */
    public static ArrayList<UsuarisPrivilegis> getUsuarisPrivilegis() {

        ArrayList<UsuarisPrivilegis> listPrivilegis = new ArrayList<>();

        String q = "select id, tipus, classe from usuarisprivilegis ORDER BY tipus, classe ASC";

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                UsuarisPrivilegis privilegis = new UsuarisPrivilegis();
                privilegis.setId(rs.getInt("id"));
                privilegis.setTipus(rs.getInt("tipus"));
                privilegis.setClasse(rs.getString("classe"));
                listPrivilegis.add(privilegis);
            }
        } catch (SQLException sqle) {
            Logger.getLogger(HistoricDTO.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }

        return listPrivilegis;

    }

    /**
     * Retorna un ArrayList amb totes les files que tinguin el tipus i el
     * controlador que entrem
     *
     * @param tipus tipus de privilegi
     * @param controlador nom del controlador
     * @return ArrayList llista de privilegis
     */
    public static ArrayList<UsuarisPrivilegis> getUsuarisPrivilegis(String tipus, String controlador) {

        ArrayList<UsuarisPrivilegis> listPrivilegis = new ArrayList<>();

        String q = "select id, tipus, classe from usuarisprivilegis where tipus LIKE \"%" + tipus + "%\" and classe like \"%" + controlador + "%\" ORDER BY tipus, classe ASC";

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                UsuarisPrivilegis privilegis = new UsuarisPrivilegis();
                privilegis.setId(rs.getInt("id"));
                privilegis.setTipus(rs.getInt("tipus"));
                privilegis.setClasse(rs.getString("classe"));
                listPrivilegis.add(privilegis);
            }
        } catch (SQLException sqle) {
            Logger.getLogger(HistoricDTO.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }

        return listPrivilegis;

    }

    /**
     * Retorna un ArrayList amb tots els tipus que hi ha en la base de dades
     *
     * @return ArrayList llistat amb els tipus de privilegis
     */
    public static ArrayList<UsuarisPrivilegis> getTipus() {

        ArrayList<UsuarisPrivilegis> listTypes = new ArrayList<>();

        String q = "select distinct tipus from usuaris ORDER BY tipus ASC";

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                UsuarisPrivilegis privilegis = new UsuarisPrivilegis();
                privilegis.setTipus(rs.getInt("tipus"));
                listTypes.add(privilegis);
            }
        } catch (SQLException sqle) {
            Logger.getLogger(HistoricDTO.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }

        return listTypes;

    }

    public static boolean insertUsuariPrivilegi(UsuarisPrivilegis usuariprivilegi) {

        String sql = "INSERT INTO usuarisprivilegis (tipus, classe) VALUES (?, ?)";

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean r = false;
        Integer id = 0;

        try {
            con = Bdades.getDataSource().getConnection();
            pstmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, usuariprivilegi.getTipus());
            pstmt.setString(2, usuariprivilegi.getClasse());
            pstmt.executeUpdate();
            rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                r = true;
                id = rs.getInt(1);
                usuariprivilegi.setId(id);
            }

            HistoricCanvisService.insertHistoric(id, HistoricCanvisService.TIPUS_INSERT, NOM_TAULA, usuariprivilegi.toString());

        } catch (SQLException sqle) {
            Logger.getLogger(UsuarisPrivilegis.class.getName()).log(Level.SEVERE, null, sqle);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstmt, rs);
        }

        return r;
    }

    public static boolean updateUsuariPrivilegi(UsuarisPrivilegis usuariprivilegi) {

        String q = "update usuarisprivilegis set tipus = ?, classe = ? where id = ?";

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean r = true;
        Integer id = usuariprivilegi.getId();

        HistoricCanvisService.insertHistoric(id, HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, usuariprivilegi.toString());

        try {
            con = Bdades.getDataSource().getConnection();
            pstmt = con.prepareStatement(q);
            pstmt.setInt(1, usuariprivilegi.getTipus());
            pstmt.setString(2, usuariprivilegi.getClasse());
            pstmt.setInt(3, usuariprivilegi.getId());
            pstmt.executeUpdate();

        } catch (SQLException sqle) {
            Logger.getLogger(UsuarisPrivilegis.class.getName()).log(Level.SEVERE, null, sqle);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstmt, rs);
        }

        return r;
    }

    public static boolean deleteUsuariPrivilegi(UsuarisPrivilegis usuariprivilegi) {

        String q = "delete from usuarisprivilegis where id = ?";

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean r = true;
        Integer id = usuariprivilegi.getId();

        try {
            con = Bdades.getDataSource().getConnection();
            pstmt = con.prepareStatement(q);
            pstmt.setInt(1, usuariprivilegi.getId());
            pstmt.executeUpdate();
            HistoricCanvisService.insertHistoric(id, HistoricCanvisService.TIPUS_DELETE, NOM_TAULA, usuariprivilegi.toString());

        } catch (SQLException sqle) {
            Logger.getLogger(UsuarisPrivilegis.class.getName()).log(Level.SEVERE, null, sqle);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstmt, rs);
        }

        return r;
    }

}
