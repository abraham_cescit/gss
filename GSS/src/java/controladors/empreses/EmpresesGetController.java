/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.empreses;

import controladors.base.CITServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Empreses;
import models.services.EmpresesService;
import util.commons.UtilsGrid;

/**
 *
 * @author Abraham M
 */
public class EmpresesGetController extends CITServlet {

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {
                response.setContentType("text/xml;charset=UTF-8");
                response.setContentType("application/xml");
                addDiccionari("empreses");
                setNua();

                ArrayList<Empreses> llista = EmpresesService.getEmpreses();

                PrintWriter out = response.getWriter();
                out.print("<?xml version='1.0' encoding='UTF-8'?>");
                out.print("<rows>");
                for (Empreses e : llista) {
                    out.print(UtilsGrid.toXMLPK(e.getIdEmpresa() + ""));
                    out.print(UtilsGrid.toXML(e.getIdEmpresa() + ""));
                    out.print(UtilsGrid.toXML(e.getNom() + ""));
                    out.print(UtilsGrid.toXML(e.getIdFiscal() + ""));
                    out.print(UtilsGrid.toXML(e.getAdreca() + ""));
                    out.print(UtilsGrid.toXML(e.getCpostal() + ""));
                    out.print(UtilsGrid.toXML(e.getPoblacio() + ""));
                    out.print(UtilsGrid.toXML(e.getPais() + ""));
                    out.print(UtilsGrid.toXML(e.getTelefon() + ""));
                    out.print(UtilsGrid.toXML(e.getContacte() + ""));
                    out.print(UtilsGrid.toXML(e.getEmail() + ""));
                    out.print(UtilsGrid.toXML(e.getWeb() + ""));
                    out.print(UtilsGrid.toXMLend());
                }
                out.print("</rows>");

                super.processRequest(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmpresesGetController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}