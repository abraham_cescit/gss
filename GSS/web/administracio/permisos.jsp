<%@page import="util.commons.Utils"%>
<%@ page import="controladors.base.Idioma" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<% Idioma idioma = (Idioma) request.getAttribute("idioma");%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script>
    
    //=========
    // VARIABLES
    //=========
    var layoutBase, formCercar, gridUsuarisPrivilegis, formEdicio;
    var params = ""; //Variable para guardar la búsqueda
    
    //============
    // FORMULARIS
    //============
    
    var contentFormCercar = [
        {type: "settings", position: "label-left", labelWidth: WIDTH_LABEL_FORM, inputWidth: "auto", offsetLeft: OFFSET_LEFT_FORM},
        {type: "combo", readonly: true, name: "tipus", id: "tipus", label: "<%=idioma.get("TIPUS")%> : ",  connector: "permisos-get-tipus", inputWidth: "225"},
        {type:"input", name:"controlador",label: "<%=idioma.get("CONTROLADOR")%> : ", inputWidth: "275"},
        {type: "button", name: "cercar", value: "<i class='fa fa-search iconboton'></i><%=idioma.get("CERCAR")%>", offsetTop: OFFSET_LEFT_FORM},
    ];
    
    var contentFormEdicio = [
        {type: "settings", position: "label-left", labelWidth: WIDTH_LABEL_FORM, inputWidth: "auto", offsetLeft: OFFSET_LEFT_FORM},
        {type:"input", name:"id"},        
        {type: "combo", readonly: true, name: "tipus", id: "tipus", label: "<%=idioma.get("TIPUS")%> : ",  connector: "permisos-get-tipus", inputWidth: "225"},
        {type:"input", name:"controlador",label: "<%=idioma.get("CONTROLADOR")%> : ", inputWidth: "275"},
        {type: "button", name: "desa", value: "<i class='fas fa-save iconboton'></i><%=idioma.get("DESA")%>", offsetTop: OFFSET_LEFT_FORM}
    ];
    
    //===============
    // CONTENT GRIDS
    //===============
    
    var contentGridUsuarisPrivilegis = {
        image_path: "../codebase-pro/imgs/",
        columns: [
            {label: "id", id: "id"},
            {label: "<%=idioma.get("TIPUS")%>", id: "tipus", width: 50, type: "ro", sort: "int", align: "right"},
            {label: "<%=idioma.get("CONTROLADOR")%>", id: "controlador", width: '*', type: "ro", sort: "str", align: "left"}
        ],
        multiselect: false
    };
    
    //CUERPO
    function doOnload() {
    
        layoutBase = new dhtmlXLayoutObject({
           parent: "cuerpo",
           pattern : "3W",
           cells: [
               {id: "a", text:"<%=idioma.get("FORM_CERCA") %>"},
               {id: "b", text:"<%=idioma.get("GRID_PRIVILEGIS") %>"},
               {id: "c", text:"<%=idioma.get("FORM_EDICIO") %>"}
           ]
        });
        
        layoutBase.cells("a").setWidth(WIDTH_LAYOUT_FORM_25);
        layoutBase.cells("c").setWidth(WIDTH_LAYOUT_FORM_25);
        
        //FORM CERCA
        
        formCercar = layoutBase.cells("a").attachForm();
        formCercar.loadStruct(contentFormCercar);
        
        // EVENTS FORM CERCA
        
        formCercar.attachEvent("onButtonClick", function(name){
            if (name==="cercar"){
                var tipus = formCercar.getItemValue("tipus");
                var controlador = formCercar.getItemValue("controlador");
                
                params = "?tipus="+tipus;
                params += "&controlador="+controlador;
                
                progressOn(layoutBase);
                
                gridUsuarisPrivilegis.clearAndLoad("permisos-get"+params, function () {
                    progressOff(layoutBase);                
                    if(gridUsuarisPrivilegis.getRowsNum() > 0)
                        gridUsuarisPrivilegis.filterByAll();
                        dhtmlx.message({type: "informacio", text: gridUsuarisPrivilegis.getRowsNum()+" <%=idioma.get("RESULTATS_CERCA")%>", expire: "6000 "});
                });
            }
        });
        
        // GRID
        
        gridUsuarisPrivilegis = layoutBase.cells("b").attachGrid(contentGridUsuarisPrivilegis);
        gridUsuarisPrivilegis.enableRowsHover(true, "hover");
        gridUsuarisPrivilegis.enableMultiline(true);
        gridUsuarisPrivilegis.setColumnHidden(0,true);
        gridUsuarisPrivilegis.attachHeader(",#numeric_filter,#text_filter");
        
        // TOOLBAR GRID
        
        var toolBarGrid = layoutBase.cells("b").attachToolbar();
        toolBarGrid.setIconset("awesome");        
        toolBarGrid.addButton("mostra", 0, "", "fas fa-database");
        toolBarGrid.setIconSize(18);
        toolBarGrid.setAlign("right");
        toolBarGrid.attachEvent("onClick", function (id) {
            if (id == "mostra") {
                progressOn(layoutBase);
                gridUsuarisPrivilegis.clearAndLoad("permisos-get", function () {
                    progressOff(layoutBase);
                });
            }
        });
        
        // FORM EDICIO
        
        formEdicio = layoutBase.cells("c").attachForm();
        formEdicio.loadStruct(contentFormEdicio);
        
        formEdicio.bind(gridUsuarisPrivilegis);
        formEdicio.hideItem("id");
        
        //TOOLBAR FORMULARIO EDICION
        var toolbarFormEdicio = layoutBase.cells("c").attachToolbar();
        toolbarFormEdicio.setIconset("awesome");
        toolbarFormEdicio.addButton("afegir", 1, "", "fas fa-plus");
        toolbarFormEdicio.addButton("eliminar", 2, "", "fas fa-trash-alt");
        toolbarFormEdicio.setAlign("left");
        
        
        toolbarFormEdicio.attachEvent("onClick", function (id) {
                var tipus = formEdicio.getItemValue("tipus");
                var controlador = formEdicio.getItemValue("controlador");
                var idPrivilegi = formEdicio.getItemValue("id");
        
            if (id == "afegir") {
                formEdicio.clear();
                
            }else if (id == "eliminar"){
                if(tipus!=="" && controlador!=="" && idPrivilegi!==""){
                    esborraPermis(tipus,controlador,idPrivilegi);
                } else {
                    dhtmlx.message({type: "error", text: "<%=idioma.get("HAS_DE_SELECCIONAR_UNA_FILA")%>", expire: "6000 "});
                }
                    
            }
        });
        
        // EVENTS FORM EDICIO
        
        formEdicio.attachEvent("onButtonClick", function(name){
            if (name == "desa"){
                url = "";
                
                if (formEdicio.getItemValue("id") == ""){
                    url += "permisos-insert";
                } else {
                    url += "permisos-update";
                }
                
                formEdicio.send(url, "POST", function(r){
                    dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: "6000 "});
                    gridUsuarisPrivilegis.clearAndLoad("permisos-get"+params, function(){
                        gridUsuarisPrivilegis.filterByAll();
                        if(getAjax(r, RESULTAT) != "error")
                            formEdicio.clear();
                    });
                });             
            }
        });
    }
    
    //=========
    // FUNCIONS
    //=========
    
    function esborraPermis(tipus, controlador, id){
        dhtmlx.confirm({
            title: "<%= idioma.get("CONFIRMAR_ACCIO")%>",
            type: "confirm-warning",
            text: "<%= idioma.get("CONFIRMACIO_ELIMINAR_PERMIS")%>",
            ok: "<%= idioma.get("ACCEPTAR")%>",
            cancel: "<%= idioma.get("CANCELAR")%>",
            callback: function (r) {
                if (r) {
                    window.dhx.ajax.post("permisos-delete", "tipus=" + tipus + "&controlador=" + controlador + "&id=" + id, function (r) {
                        console.log(id);
                        dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: "6000 "});       
                        gridUsuarisPrivilegis.clearAndLoad("permisos-get"+params, function(){
                            gridUsuarisPrivilegis.filterByAll();
                            formEdicio.clear();
                        });
                    });
                }
            }
        });
    }
    
    document.addEventListener("DOMContentLoaded", doOnload);
</script>

<div id="cuerpo" style="width:100%;height:96%;overflow:hidden">
</div>