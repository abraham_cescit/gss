package persistencia;


import javax.sql.DataSource;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import util.commons.LoggerOut;

/**
 * Encapsula la conexio a la base de dades
 *
 * @author Jose
 */
public abstract class Bdades {

    public Connection conn;
    public Statement stmt;
    public PreparedStatement pstmt;
    public ResultSet rs;
    private static int count = 0;
    public static DataSource dataSource;

    public static boolean newPool() {

        Context ctx;
        try {
            ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/gssDS");
        } catch (NamingException ex) {
            Logger.getLogger(Bdades.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    public static DataSource getDataSource() throws SQLException {
        if (dataSource == null) {
            newPool();
        }
        count++;
        return dataSource;
    }



    public static void closeConnections(Connection conn, PreparedStatement pstmt, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                LoggerOut.sendLogger(null, ex, Bdades.class, "Error (" + ex.getErrorCode() + "): " + ex.getMessage());
            }
        }

        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                LoggerOut.sendLogger(null, ex, Bdades.class, "Error (" + ex.getErrorCode() + "): " + ex.getMessage());
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                LoggerOut.sendLogger(null, ex, Bdades.class, "Error (" + ex.getErrorCode() + "): " + ex.getMessage());
            }
        }
    }
    
    public static void closeConnections(Connection conn, Statement stmt, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                LoggerOut.sendLogger(null, ex, Bdades.class, "Error (" + ex.getErrorCode() + "): " + ex.getMessage());
            }
        }

        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                LoggerOut.sendLogger(null, ex, Bdades.class, "Error (" + ex.getErrorCode() + "): " + ex.getMessage());
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                LoggerOut.sendLogger(null, ex, Bdades.class, "Error (" + ex.getErrorCode() + "): " + ex.getMessage());
            }
        }
    }
    
    public static void closeConnections(PreparedStatement pstmt, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                LoggerOut.sendLogger(null, ex, Bdades.class, "Error (" + ex.getErrorCode() + "): " + ex.getMessage());
            }
        }

        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException ex) {
                LoggerOut.sendLogger(null, ex, Bdades.class, "Error (" + ex.getErrorCode() + "): " + ex.getMessage());
            }
        }

    }
    
    public static void closeConnections(Statement stmt, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                LoggerOut.sendLogger(null, ex, Bdades.class, "Error (" + ex.getErrorCode() + "): " + ex.getMessage());
            }
        }

        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                LoggerOut.sendLogger(null, ex, Bdades.class, "Error (" + ex.getErrorCode() + "): " + ex.getMessage());
            }
        }

    }
    
    
    public static int getLastInsertId(Connection con) {
        int lid = 0;
        String sql = "select LAST_INSERT_ID()";

        try {
            Statement stm = con.createStatement();
            stm.execute(sql);
            ResultSet rs = stm.getResultSet();
            rs.next();
            lid = rs.getInt(1);
            rs.close();
            stm.close();
        } catch (SQLException sqle) {
            LoggerOut.sendLogger(null, sqle, Bdades.class, "Error (" + sqle.getErrorCode() + "): " + sqle.getMessage());
        }
        return lid;
    }
    
    public static Connection createConnectionTransaction() throws SQLException {
        Connection con = getDataSource().getConnection();
        con.setAutoCommit(false);
        return con;
    }
}
