/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.conductors;

import controladors.base.CITServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Abraham M
 */
public class ConductorGetController extends CITServlet {

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {
                response.setContentType("text/xml;charset=UTF-8");
                response.setContentType("application/xml");
//                addDiccionari("conductors");
                setNua();

//                ArrayList<Usuaris> llista = UsuarisService.getUsuaris();

                PrintWriter out = response.getWriter();
                out.print("<?xml version='1.0' encoding='UTF-8'?>");
                out.print("<rows>");
//                for (Usuaris u : llista) {
//                    Date date = new Date();
//                    if(u.getDataBaixa() == null || u.getDataBaixa().compareTo(date)>0){
//                    out.print(UtilsGrid.toXMLPK(u.getIdUsuari() + ""));
//                    out.print(UtilsGrid.toXML(u.getIdUsuari() + ""));
//                    out.print(UtilsGrid.toXML(u.getLogin() + ""));
//                    out.print(UtilsGrid.toXML(u.getNom() + ""));
//                    out.print(UtilsGrid.toXML(u.getTipus() + ""));
//                    out.print(UtilsGrid.toXML(Rols.desdeInt(u.getTipus()).getNom() + ""));
//                    out.print(UtilsGrid.toXML(u.getEmail() + ""));
//                    out.print(UtilsGrid.toXML(u.getTelefon() + ""));
//                    out.print(UtilsGrid.toXMLDateSQL(u.getDatanaixement()));
//                    out.print(UtilsGrid.toXML(u.getIdioma() + ""));
//                    out.print(UtilsGrid.toXMLDateSQL(u.getDataBaixa()));
//                    out.print(UtilsGrid.toXMLend());
//                    }
//                }
                out.print("</rows>");

                super.processRequest(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConductorGetController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}