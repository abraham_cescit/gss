package com.cescit.security;


import java.sql.SQLException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

/**
 * Maneja el control de acceso.
 *
 * @author Jose Francisco Belda Santonja
 */
public class ControlAcces {

    public static final String ATR_USUARI = "usuari";

    /**
     * FA logout de l'usuari que hi ha amb sesio iniciada.
     *
     */
    public static void logout() {
        Subject currentUser = SecurityUtils.getSubject();
        currentUser.logout();
    }


    /**
     * Mira si l'usuari havia iniciat sesio autenticant-se
     *
     *
     * @return true si l'usuari havia iniciat sesio false si no
     */
    public static boolean estaAutenticat() {
        Subject currentUser = SecurityUtils.getSubject();        
        return currentUser.isAuthenticated();
    }

    /**
     * Examina si el usuari en sesio te cert privilegi
     *
     * @param p Privilegi sobre el que es pregunta
     * @return true si te el privilegi y false si no el te
     * @throws SQLException excepcio
     */
    public static boolean tePrivilegi(Class p) throws SQLException {
        boolean r = false;
        Subject currentUser = SecurityUtils.getSubject();
        try {
            currentUser.checkPermission(p.getName());
            r = true;
        } catch (UnauthorizedException ua) {
            r = false;
        }
        return r;
    }
        
    public static String getNombreSession() {
        Subject currentUser = SecurityUtils.getSubject();
        return currentUser.getPrincipal().toString();
    }
    
    public static String getAtributoSession(String atributo){
        Subject currentUser = SecurityUtils.getSubject();
        Session session = currentUser.getSession();
        return session.getAttribute(atributo).toString();
    }
}
