/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.historic;

import controladors.base.CITServlet;
import models.dto.HistoricUsuariDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.services.HistoricCanvisService;

/**
 *
 * @author Sònia
 */
public class GetUsuarisComboController extends CITServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {
                
                setXML();
                
                ArrayList<HistoricUsuariDTO> llistaUsuari = HistoricCanvisService.getUsuari();
                
                PrintWriter out = response.getWriter();
                out.print("<?xml version='1.0' encoding='UTF-8'?>");
                out.print("<complete>");
                out.print("<option value=\"\"></option>");
                for (HistoricUsuariDTO taula : llistaUsuari){
                    out.print("<option value=\""+taula.getIdUsuari()+"\">"+taula.getNomUsuari()+"</option>");
                }
                out.print("</complete>");
                
                super.processRequest(request, response);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(GetUsuarisComboController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}