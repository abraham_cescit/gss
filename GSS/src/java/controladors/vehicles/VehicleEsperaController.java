/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.vehicles;

import controladors.base.CITServlet;
import static controladors.base.CITServlet.MISSATGE;
import static controladors.base.CITServlet.RESULTAT;
import static controladors.base.CITServlet.TIPUS_BREU_XML;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Espera;
import models.Vehicles;
import models.services.APIService;
import models.services.VehiclesService;
import util.commons.Utils;

/**
 *
 * @author Abraham M
 */
public class VehicleEsperaController extends CITServlet {

    public static final String DESAT_EXIT = "DESAT_EXIT";
    public static final String TOKEN = "token";
    public static final String MATRICULA = "matr";
    public static final String LINEA = "lane";
    public static final String FOTO = "picture";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this, false)) {
                request.setCharacterEncoding("UTF-8");
                setTipus(TIPUS_BREU_XML);
                addDiccionari("vehicles");
                String token = Utils.getParameterString(request, TOKEN);
                String matricula = Utils.getParameterString(request, MATRICULA);
                Integer linea = Utils.getParameterInteger(request, LINEA);
                String foto = Utils.getParameterString(request, FOTO);
                Espera espera = new Espera(matricula, linea, foto);
                if (token.equals(Utils.getProperty(TOKEN.toUpperCase()))){
                    if(matricula.length() == 7){
                        String numeros = matricula.substring(0, 4);
                        String lletres = matricula.substring(4, 7);

                        if (Utils.isNumeric(numeros) && !Utils.isNumeric(lletres)){
                            Vehicles vehicle = VehiclesService.getVehiclePerMatricula(matricula);

                            if (APIService.insertarEntrada(espera)){
                                afegirRespostaBreu(RESULTAT, "ok");
                                afegirRespostaBreu(MISSATGE, getTexte("VEHICLE_ENTRADA_INSERTAR"));
                                try {
                                    if (vehicle.getIdVehicle() == null) {
            //                            afegirRespostaBreu(RESULTAT, "no");
                                        afegirRespostaBreu(RESULTAT, "ok");
                                        afegirRespostaBreu(MISSATGE, getTexte("VEHICLE_TOKEN"));
            //                            afegirRespostaBreu(MISSATGE, getTexte("VEHICLE_NO_TROBAT"));
            //                            afegirRespostaBreu(TEMPS, "6000");
                                    } else {
                                        afegirRespostaBreu(RESULTAT, "ok");
                                        afegirRespostaBreu(MISSATGE, getTexte("VEHICLE_TOKEN"));
            //                            afegirRespostaBreu(MISSATGE, getTexte("VEHICLE_TROBAT"));
            //                            afegirRespostaBreu(TEMPS, "6000");
                                    }
                                } catch (Exception expa) {
            //                        afegirRespostaBreu(RESULTAT, "error");
            //                        afegirRespostaBreu(MISSATGE, getTexte("VEHICLE_NO_TROBAT"));
            //                        afegirRespostaBreu(TEMPS, "0");
                                    Logger.getLogger(VehicleEsperaController.class.getName()).log(Level.SEVERE, null, expa);
                                }
                            } else {
                                afegirRespostaBreu(RESULTAT, "error");
                                afegirRespostaBreu(MISSATGE, getTexte("VEHICLE_ENTRADA_INSERTAR_ERROR"));
            //                    afegirRespostaBreu(TEMPS, "0");
                            }
                        } else {
                            afegirRespostaBreu(RESULTAT, "error");
                            afegirRespostaBreu(MISSATGE, getTexte("VEHICLE_ERROR_MATRICULA"));
                        }
                    } else {
                        afegirRespostaBreu(RESULTAT, "error");
                        afegirRespostaBreu(MISSATGE, getTexte("VEHICLE_ERROR_MATRICULA"));
                    }
                } else {
                    afegirRespostaBreu(RESULTAT, "error");
                    afegirRespostaBreu(MISSATGE, getTexte("VEHICLE_ERROR_TOKEN"));
//                    afegirRespostaBreu(TEMPS, "0");
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(VehicleEsperaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(VehicleEsperaController.class.getName()).log(Level.SEVERE, null, ex);
        }


        super.processRequest(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
