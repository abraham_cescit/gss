/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Cameres;
import persistencia.Bdades;

/**
 *
 * @author Abraham M
 */
public class CameresService {
    private static String NOM_TAULA = "camera";

    public static ArrayList<Cameres> getCameres() {
        ArrayList<Cameres> r = new ArrayList<Cameres>();

        String q = "SELECT idcamera, nom, posicio, descripcio, marca, model, protocol FROM camera";

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                r.add(CameresService.obtenir(
                        rs.getInt("idcamera"),
                        rs.getString("nom"),
                        rs.getInt("posicio"),
                        rs.getString("descripcio"),
                        rs.getString("marca"),
                        rs.getString("model"),
                        rs.getString("protocol")));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(CameresService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }

    public static boolean insertarCamera(Cameres c) {
        String sql = "INSERT INTO camera (nom, posicio, descripcio, marca, model, protocol) VALUES (?,?,?,?,?,?)";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        boolean r = false;
        int id = 0;

        try {
            
            int i = 1;
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstm.setString(i++, c.getNom());
            pstm.setInt(i++, c.getPosicio());
            pstm.setString(i++, c.getDescripcio());
            pstm.setString(i++, c.getMarca());
            pstm.setString(i++, c.getModel());
            pstm.setString(i++, c.getProtocol());
            pstm.executeUpdate();
            rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }

            if (id != 0) {
                r = true;
                HistoricCanvisService.insertHistoric(id, HistoricCanvisService.TIPUS_INSERT, NOM_TAULA, c.toString());
            }

        } catch (SQLException sqle) {
            Logger.getLogger(CameresService.class.getName()).log(Level.SEVERE, null, sqle);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }

        if (r) {

        }

        return r;
    }

    public static boolean updateCamera(Cameres c) {
        boolean r = false;
        String sql = "UPDATE camera SET " 
                + "nom = ?," 
                + "posicio = ?," 
                + "descripcio = ?," 
                + "marca = ?," 
                + "model = ?," 
                + "protocol = ? "
                + "WHERE idcamera = ?";
        
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            Integer i = 1;
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(i++, c.getNom());
            pstm.setInt(i++, c.getPosicio());
            pstm.setString(i++, c.getDescripcio());
            pstm.setString(i++, c.getMarca());
            pstm.setString(i++, c.getModel());
            pstm.setString(i++, c.getProtocol());
            pstm.setInt(i++, c.getIdCamera());
            pstm.executeUpdate();

            r = true;
            if (true) {
                HistoricCanvisService.insertHistoric(c.getIdCamera(), HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, c.toString());
            }

        } catch (SQLException ex) {
            Logger.getLogger(CameresService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    public static boolean deleteCamera(int idCamera) {
        boolean r = false;

        String sql = "DELETE FROM camera WHERE idcamera=?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idCamera);
            pstm.executeUpdate();
            r = true;
            HistoricCanvisService.insertHistoric(idCamera, HistoricCanvisService.TIPUS_DELETE, NOM_TAULA, "Empresa donada de baixa");
        } catch (SQLException ex) {
            Logger.getLogger(CameresService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }
    
    public static Cameres getProtocol(String posicioCamera) {
        Cameres camera = new Cameres();

        String sql = "SELECT idcamera, nom, posicio, descripcio, marca, model, protocol FROM camera WHERE nom = ?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, posicioCamera);
            pstm.executeQuery();
            rs = pstm.getResultSet();
            if (rs.first()) {
                camera.setIdCamera(rs.getInt("idcamera"));
                camera.setNom(rs.getString("nom"));
                camera.setPosicio(rs.getInt("posicio"));
                camera.setDescripcio(rs.getString("descripcio"));
                camera.setMarca(rs.getString("marca"));
                camera.setModel(rs.getString("model"));
                camera.setProtocol(rs.getString("protocol"));
                
            }
        } catch (SQLException sqle) {
            Logger.getLogger(CameresService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return camera;
    }

    public static Cameres obtenir(Integer idCamera, String nom, Integer posicio, 
            String descripcio, String marca, String model, String protocol) {
        return new Cameres(idCamera, nom, posicio, descripcio, marca, model, protocol);
    }
    
}
