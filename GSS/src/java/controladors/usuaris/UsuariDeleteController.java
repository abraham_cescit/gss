package controladors.usuaris;

import controladors.base.CITServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.services.UsuarisService;

public class UsuariDeleteController extends CITServlet {

    public static final String ESBORRAT_EXIT="ESBORRAT_EXIT";

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String resposta=INDEFINIDA;
        try {
            if (inicialitzar(request, response, this)) {
                setTipus(TIPUS_BREU_XML);
                addDiccionari("usuaris");
                String stIdUsuari=request.getParameter("id");
                
                try{
                    if(stIdUsuari!=null && !stIdUsuari.equals("") & Integer.parseInt(stIdUsuari)>0){
                        int idUsuari=Integer.parseInt(stIdUsuari);
                        if(UsuarisService.deleteUsuari(idUsuari)){
                            resposta=ESBORRAT_EXIT;
                        }
                        else{
                            resposta=ERROR;
                        }
                    }
                }
                catch(NumberFormatException nfe){
                    resposta=ERROR;
                }
                finally{
                    afegirRespostaBreu(RESULTAT,resposta);
                    afegirRespostaBreu(MISSATGE,getTexte(resposta));
                    super.processRequest(request, response);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuariDeleteController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
