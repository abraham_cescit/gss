<%@page import="util.commons.UtilsBase"%>
<%@page import="util.commons.Utils"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>${titol}</title>
        <%@include file="custom-css.jsp" %>
        <%@ page import="java.util.ArrayList"  %>
        <%
            ArrayList<String> libreriasCSS;
            if (request.getAttribute("csss") != null) {
                libreriasCSS = (ArrayList<String>) request.getAttribute("csss");
                for (String css : libreriasCSS) {
        %><link rel="STYLESHEET" type="text/css" href="<%= css%>"><%
                }
            }
            ArrayList<String> libreriasJS;
            if (request.getAttribute("jss") != null) {
                libreriasJS = (ArrayList<String>) request.getAttribute("jss");
                for (String jss : libreriasJS) {
        %><script type="text/javascript" src="<%= jss%>"></script><%
                }
            }
        %>
        <style>
            /*these styles allow dhtmlxLayout to work in fullscreen mode in different browsers correctly*/
            html, body {
                width: 100% !important;
                height: 100% !important;
                margin: 0px !important;
                overflow: hidden !important;
                background-color: white ;
                font-family: "Gill Sans Extrabold", Helvetica, sans-serif
            }
        </style>


        <!--<link rel="STYLESHEET" type="text/css" href="codebase/dhtmlx.css">-->
        <link rel="stylesheet" type="text/css" href="codebase-pro/dhtmlx.css">
        <link href="codebase-pro/fonts/font-awesome/css/fontawesome.css" rel="stylesheet">
        <link href="codebase-pro/fonts/font-awesome/css/brands.css" rel="stylesheet">
        <link href="codebase-pro/fonts/font-awesome/css/solid.css" rel="stylesheet">

        <!--<script src="codebase/dhtmlx.js" type="text/javascript"></script> -->
        <script src="codebase-pro/dhtmlx.js" type="text/javascript"></script> 
        <script src="codebase-pro/jquery-3.1.1.min.js" type="text/javascript"></script> 
        <script src="js/custom.js" type="text/javascript"></script> 
        <script src="js/validate.js" type="text/javascript"></script>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/md5.js"></script>


        <script>
            if (window.screen.height / window.screen.width > 1) {
                var link = document.createElement('meta');
                link.setAttribute('name', 'viewport');
                link.content = "width=device-width, initial-scale=1.0";
                document.getElementsByTagName('head')[0].appendChild(link);
            }

            //******* VARIABLES JAVASCRIPT GLOBALES ******//
            var RESULTAT = "RESULTAT"; // a mode de constant per recuperar resultats Ajax
            var MISSATGE = "MISSATGE"; // a mode de constant per recuperar resultats Ajax
            var TEMPS = "TEMPS"; // a mode de constant per recuperar resultats Ajax
            var OK = "OK";// a mode de constant per recuperar resultats Ajax
            var ERROR = "ERROR";// a mode de constant per recuperar resultats Ajax
            var PARAMETRE = "PARAMETRE";// a mode de constant per recuperar resultats Ajax
            var INDEFINIDA = "INDEFINIDA";// a mode de constant per recuperar resultats Ajax
            var DESAT_EXIT = "DESAT_EXIT";
            var ESBORRAT_EXIT = "ESBORRAT_EXIT";




            //******* ICONOS ******//

            var ICON_NAME_TARIFA = "<%=UtilsBase.ICON_NAME_TARIFA%>";
            var ICON_TARIFA = "<%=UtilsBase.ICON_TARIFA%>";
            var ICON_TARIFA_WHITE = "<%=UtilsBase.ICON_TARIFA_WHITE%>";

            var ICON_NAME_PROVEIDOR = "<%=UtilsBase.ICON_NAME_PROVEIDOR%>";
            var ICON_PROVEIDOR = "<%=UtilsBase.ICON_PROVEIDOR%>";
            var ICON_PROVEIDOR_WHITE = "<%=UtilsBase.ICON_PROVEIDOR_WHITE%>";

            //******* TAMAÑOS ******//
            var WIDTH_LAYOUT_FORM_25 = getWidthWindow(25);
            var WIDTH_LAYOUT_FORM_35 = getWidthWindow(35);
            var WIDTH_LAYOUT_FORM_45 = getWidthWindow(45);
            var WIDTH_LAYOUT_BUTTON_25_FULL = getWidthWindow(25) - 40;
            var WIDTH_LABEL_FORM = 100;
            var WIDTH_LABEL_FORM_MEDIUM = 100;
            var WIDTH_LABEL_FORM_BIG = 200;
            var WIDTH_LABEL_FORM_FULL_15 = getWidthWindow(15) - 40;
            var WIDTH_LABEL_FORM_FULL_20 = getWidthWindow(20) - 40;
            var WIDTH_LABEL_FORM_35_FULL = 200;
            var OFFSET_LEFT_FORM = 20;
            var MAX_WIDTH_INPUT = WIDTH_LAYOUT_FORM_25 - WIDTH_LABEL_FORM - OFFSET_LEFT_FORM;

            var INPUT_BIG = MAX_WIDTH_INPUT * 0.9;
            if (INPUT_BIG < 300)
                INPUT_BIG = 300;

            var INPUT_MEDIUM = MAX_WIDTH_INPUT * 0.6;
            if (INPUT_MEDIUM < 200)
                INPUT_MEDIUM = 200;

            var INPUT_SMALL = MAX_WIDTH_INPUT * 0.4;
            if (INPUT_SMALL < 100)
                INPUT_SMALL = 100;

            var INPUT_XSMALL = MAX_WIDTH_INPUT * 0.1;
            if (INPUT_XSMALL < 100)
                INPUT_XSMALL = 100;

            var WIDTH_LAYOUT_FORM_15 = getWidthWindow(15);
            var WIDTH_LAYOUT_FORM_20 = getWidthWindow(20);
            var WIDTH_LAYOUT_FORM_50 = getWidthWindow(50);
            var WIDTH_LAYOUT_FORM_60 = getWidthWindow(60);
            var WIDTH_LAYOUT_FORM_70 = getWidthWindow(70);
            var WIDTH_LAYOUT_FORM_80 = getWidthWindow(80);
            var HEIGHT_LAYOUT_60 = getHeightWindow(60);
            var HEIGHT_LAYOUT_40 = getHeightWindow(40);
            var HEIGHT_LAYOUT_30 = getHeightWindow(30);
            var HEIGHT_LAYOUT_20 = getHeightWindow(20);
            var HEIGHT_LAYOUT_10 = getHeightWindow(10);

            var WINDOWS_WIDTH_SMALL = getWidthWindow(30);
            var WINDOWS_WIDTH_MEDIUM = getWidthWindow(45);
            var WINDOWS_WIDTH_BIG = getWidthWindow(65);
            var WINDOWS_WIDTH_FULL = getWidthWindow(85);

            var WINDOWS_HEIGHT_SMALL = getHeightWindow(30);
            var WINDOWS_HEIGHT_MEDIUM = getHeightWindow(45);
            var WINDOWS_HEIGHT_BIG = getHeightWindow(65);
            var WINDOWS_HEIGHT_FULL = getHeightWindow(85);

            var HEIGHT_20 = getHeightWindow(20);

            var WIDTH_LABEL_FORM_20_2COL = WIDTH_LAYOUT_FORM_20 / 4;
            var WIDTH_LABEL_FORM_25_2COL = WIDTH_LAYOUT_FORM_25 / 4;
            var WIDTH_LABEL_FORM_45_2COL = WIDTH_LAYOUT_FORM_45 / 4;
            var WIDTH_LABEL_FORM_50_2COL = WIDTH_LAYOUT_FORM_50 / 4;
            var WIDTH_LABEL_FORM_70_2COL = WIDTH_LAYOUT_FORM_70 / 4;
            var WIDTH_LABEL_FORM_SUPER_BIG = WIDTH_LAYOUT_FORM_70 / 3 - 100;

            var INPUT_20_2COL = WIDTH_LAYOUT_FORM_20 / 4 - 40;
            var INPUT_25_2COL = WIDTH_LAYOUT_FORM_25 / 4 - 40;
            var INPUT_35_2COL = WIDTH_LAYOUT_FORM_35 / 4 - 40;
            var INPUT_45_2COL = WIDTH_LAYOUT_FORM_45 / 4 - 40;
            var INPUT_50_2COL = WIDTH_LAYOUT_FORM_50 / 4 - 40;
            var INPUT_70_2COL = WIDTH_LAYOUT_FORM_70 / 4 - 40;

            var INPUT_20 = WIDTH_LAYOUT_FORM_20 / 2 - 40;
            var INPUT_25 = WIDTH_LAYOUT_FORM_25 / 2 - 40;
            var INPUT_45 = WIDTH_LAYOUT_FORM_45 / 2 - 40;
            var INPUT_50 = WIDTH_LAYOUT_FORM_50 / 2 - 40;


            var INPUT_SMALL_20_2COL = WIDTH_LAYOUT_FORM_25 / 4 - 40;

        </script>

    </head>
    <body>
        <div id="divMissatges"></div>
        <div style="height: 90%;">
            <% if (request.getAttribute("cap") != null) { %>
            <jsp:include page="${cap}" />
            <% } %>
            <jsp:include page="${vista}" />
            <% if (request.getAttribute("peu") != null) { %>
            <jsp:include page="${peu}" />
            <% }%>
        </div>
    </body>
</html>
