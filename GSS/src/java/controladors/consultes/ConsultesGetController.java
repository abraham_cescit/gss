/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.consultes;

import controladors.base.CITServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.dto.RegistreDiariDTO;
import models.services.RegistreDiariService;
import util.commons.UtilsGrid;

/**
 *
 * @author Abraham M
 */
public class ConsultesGetController extends CITServlet {

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {
                response.setContentType("text/xml;charset=UTF-8");
                response.setContentType("application/xml");
                addDiccionari("cameres");
                addDiccionari("registres");
                setNua();

                ArrayList<RegistreDiariDTO> llista = RegistreDiariService.getHistoric();
                if (llista != null){
                    for(int i = 0; i < llista.size(); i++){
                        llista.get(i).setFotosArray(RegistreDiariService.getImatgesbyRegistre(llista.get(i).getIdRegistre()));
                    }
                }
                
                PrintWriter out = response.getWriter();
                out.print("<?xml version='1.0' encoding='UTF-8'?>");
                out.print("<rows>");
                for (RegistreDiariDTO rd : llista) {
                    out.print(UtilsGrid.toXMLPK(rd.getIdRegistre()+ ""));
                    out.print(UtilsGrid.toXML(rd.getIdRegistre() + ""));
                    out.print(UtilsGrid.toXML(rd.getEntrada() + ""));
                    out.print(UtilsGrid.toXML(rd.getIdVehicle() + ""));
                    out.print(UtilsGrid.toXML(rd.getMatricula() + ""));
                    out.print(UtilsGrid.toXML(rd.getIdEmpresa() + ""));
                    out.print(UtilsGrid.toXML(rd.getNomEmpresa() + ""));
                    out.print(UtilsGrid.toXML(rd.getIdFiscal() + ""));
                    // out.print(UtilsGrid.toXML(rd.getTelefon() + ""));
                    out.print(UtilsGrid.toXML(rd.getPesEntrada() + ""));
                    out.print(UtilsGrid.toXML(rd.getSortida() + ""));
                    out.print(UtilsGrid.toXML(rd.getPesSortida() + ""));
                    out.print(UtilsGrid.toXML(rd.getTemps() + ""));
                    out.print(UtilsGrid.toXML(rd.getPesDiferencia() + ""));
                    for(int j = 0; j < rd.getFotosArray().size(); j++){
                        out.print(UtilsGrid.toXML(rd.getFotosArray().get(j).getUrl() + ""));
                    }
                    out.print(UtilsGrid.toXMLend());
                }
                out.print("</rows>");

                super.processRequest(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsultesGetController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ConsultesGetController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}