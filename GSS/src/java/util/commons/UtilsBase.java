/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.commons;

import com.cescit.security.ControlAcces;
import controladors.base.Idioma;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author fernando
 */
public class UtilsBase {
    
    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd";
    public static final String DATE_FORMAT_NOW_HORA = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_ESPANYOL = "dd-MM-yyyy";
    public static final String HOUR_FORMAT = "HH:mm:ss";
    
    
    
    // ================================================= ICONS =================================================
    public static final String ICON_NAME_TARIFA = "fas fa-store";
    public static final String ICON_TARIFA = "<i class='" + ICON_NAME_TARIFA + "'></i>";
    public static final String ICON_TARIFA_WHITE = "<i class='" + ICON_NAME_TARIFA + " white'></i>";
    
    public static final String ICON_NAME_PROVEIDOR = "fas fa-shopping-cart";
    public static final String ICON_PROVEIDOR = "<i class='" + ICON_NAME_PROVEIDOR + "'></i>";
    public static final String ICON_PROVEIDOR_WHITE = "<i class='" + ICON_NAME_PROVEIDOR + " white'></i>";
    
    
    
    
    
    
    public static final Map<String, String> ESTATS_ALBARANS  = new HashMap<String, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put("A", idioma.get("ALBARANAT"));
        put("AD", idioma.get("ALBARANAT_DEVOLUCIO"));
        put("AF", idioma.get("ALBARANAT_FACTURAT"));
        put("F", idioma.get("ALBARANAT_FACTURAT"));
        put("AC", idioma.get("ALBARANAT_SEGELLAT"));
        put("ACF", idioma.get("ALBARANAT_SEGELLAT_FACTURAT"));
        put("ACD", idioma.get("ALBARANAT_SEGELLAT_DEVOLUCIO"));
        put("AFD", idioma.get("ALBARANAT_FACTURAT_DEVOLUCIO"));
    }};
    
    
    public static final Map<String, String> ESTATS_SEGONS_ARXIUS  = new HashMap<String, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put("A", idioma.get("ALBARANAT"));
        put("AD", idioma.get("ALBARANAT_DEVOLUCIO"));
        put("AF", idioma.get("ALBARANAT_FACTURAT"));
        put("AC", idioma.get("ALBARANAT_SEGELLAT"));
        put("ACF", idioma.get("ALBARANAT_SEGELLAT_FACTURAT"));
        put("ACD", idioma.get("ALBARANAT_SEGELLAT_DEVOLUCIO"));
        put("F", idioma.get("FACTURA"));
        put("FP", idioma.get("FACTURA_PAGADA"));
        put("DF", idioma.get("DEVOLUCIO_FACTURA"));
    }};
    
    
    public static final Map<String, String> ESTATS_COMANDES  = new HashMap<String, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put("P", idioma.get("PENDENT"));
        put("PA", idioma.get("PENDENT_ALBARANAR"));
        put("A", idioma.get("ALBARANAT"));
        put("AC", idioma.get("ALBARANAT_CONFIRMAT"));
        put("A/A", idioma.get("APLAN"));
    }};
    
    public static final Map<String, String> ESTATS_LINIES_COMANDES  = new HashMap<String, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put("PEND", idioma.get("PENDENT"));
        put("FIN", idioma.get("FINALITZAT"));
        put("ATU", idioma.get("ATURAT"));
        put("ANU", idioma.get("ANULAT"));
    }};
    
    public static final Map<String, String> TIPUS_ALBARANS  = new HashMap<String, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put("A", idioma.get("ABONAMENT"));
        put("C", idioma.get("CARREC"));
    }};
    
    public static final Map<String, String> extensioIcono  = new HashMap<String, String>() {{
        put("pdf", "fas fa-file-pdf");
        put("doc", "fas fa-file-word");
        put("docx", "fas fa-file-word");
        put("odt", "fas fa-file-word");
        put("mp3", "fas fa-file-video");
        put("png", "fas fa-file-image");
        put("xls", "fas fa-file-excel");
        put("zip", "fas fa-file-archive");
    }};
    
    public static final Map<String, String> relacioFitxerTipusCarpeta  = new HashMap<String, String>() {{
        put("A", Utils.getProperty("RUTA_DESAR_ALBARA"));
        put("AD", Utils.getProperty("RUTA_DESAR_ALBARA"));
        put("AC", Utils.getProperty("RUTA_DESAR_ALBARA"));
        put("F", Utils.getProperty("RUTA_GUARDAR_FACTURA"));
        put("ACF", Utils.getProperty("RUTA_GUARDAR_FACTURA"));
    }};
    
    
    
    
    
    public static final Map<String, String> METODES_PAGAMENT  = new HashMap<String, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put("TR", idioma.get("TRANSFERENCIA"));
        put("DB", idioma.get("DOMICILIACIO"));
        put("E", idioma.get("EFECTIU"));
        put("CP", idioma.get("CHECK"));
    }};
    
    
    public static final Map<String, String> PERIODICITAT_PAGAMENT  = new HashMap<String, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put("S", idioma.get("SETMANAL"));
        put("Q", idioma.get("QUINZENAL"));
        put("M", idioma.get("MENSUAL"));
        put("T", idioma.get("TRIMESTRAL"));
        put("AC", idioma.get("A_COMANDA"));
    }};
    
    
    public static final Map<String, String> MITJA_COMUNICACIO  = new HashMap<String, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put("1", idioma.get("WHATSAPP"));
        put("2", idioma.get("MAIL"));
        put("3", idioma.get("TELEFON"));
    }};
    
    public static final Map<String, String> MITJA_COMUNICACIO_REVERSE  = new HashMap<String, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put(idioma.get("WHATSAPP"), "1");
        put(idioma.get("MAIL"), "2");
        put(idioma.get("TELEFON"), "3");
    }};
    
    public static final Map<String, String> ICONO_ESTAT_LINIA_COMANDA  = new HashMap<String, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put("P",idioma.get("WHATSAPP"));
        put("F",idioma.get("MAIL"));
        put("A",idioma.get("TELEFO"));
    }};
    
    
    public static final Map<String, String> ICONOS_ESTATS_LINIES_COMANDA  = new HashMap<String, String>() {{
        put("FIN", "fas fa-check-double");
        put("ABO", "fas fa-hand-holding-usd");
        put("APL", "fas fa-pause-circle");
        put("PROC", "fas fa-spinner");
        put("ANU", "fas fa-minus-circle");
        put("P", "fas fa-clock");
        put("C", "fas fa-exclamation-triangle");
        put("PEND", "fas fa-clock");
    }};
    
    
    public static final Map<String, String> TIPUS_MODIFICACIO_STOCK  = new HashMap<String, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put("C", idioma.get("COMPRA"));
        put("V", idioma.get("VENTA"));
        put("D", idioma.get("DESPECEJAMENT"));
        put("P", idioma.get("PRODUCCIO"));
        put("M", idioma.get("MERMA"));
        put("R", idioma.get("DEVOLUCIO"));
        put("A", idioma.get("ACTUALITZACIO"));
        put("CO", idioma.get("CONGELAT"));
        put("DC", idioma.get("DESCONGELAT"));
        put("E", idioma.get("ELIMINAR"));
    }};
    
    
    public static final Map<String, String> ICONOS_ESTATS_FACTURES  = new HashMap<String, String>() {{
        put("FNC", "fas fa-clipboard");
        put("FC", "fas fa-file-invoice-dollar");
        put("FD", "fas fa-undo");
    }};
    
    
    public static final Map<Integer, String> PALETA_COLORS_GRAFICS  = new HashMap<Integer, String>() {{
        put(1, "#555e7b");
        put(2, "#b7d968");
        put(3, "#b576ad");
        put(4, "#fde47f");
        put(5, "#e04644");
        put(6, "#b2cdf4");
        put(7, "#7ccce5");
        put(8, "#0b4e89");
        put(9, "#FE4365");
        put(10, "#83AF9B");
        put(11, "#53777A");
        put(12, "#C02942");
    }};
    
    public static final Map<Integer, String> PALETA_COLORS_GRAFICS2  = new HashMap<Integer, String>() {{
        put(1, "#4580FF");
        put(2, "#3397E8");
        put(3, "#6E79EB");
        put(4, "#5738FF");
        put(5, "#224080");
        put(6, "#3766CC");
        put(7, "#112040");
        put(8, "#4E33E8");
        put(9, "#B438FF");
        put(10, "#296EFF");
        put(11, "#472CFF");
        put(12, "#5DBCFF");
    }};
    
    
    public static final Map<Integer, String> NOM_MESOS  = new HashMap<Integer, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put(1, idioma.get("GENER"));
        put(2, idioma.get("FEBRER"));
        put(3, idioma.get("MARC"));
        put(4, idioma.get("ABRIL"));
        put(5, idioma.get("MAIG"));
        put(6, idioma.get("JUNY"));
        put(7, idioma.get("JULIOL"));
        put(8, idioma.get("AGOST"));
        put(9, idioma.get("SETEMBRE"));
        put(10, idioma.get("OCTUBRE"));
        put(11, idioma.get("NOVEMBRE"));
        put(12, idioma.get("DESEMBRE"));
    }};
    
public static final Map<Integer, String> ABR_DIES  = new HashMap<Integer, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put(1, idioma.get("DIUMENGE"));
        put(2, idioma.get("DILLUNS"));
        put(3, idioma.get("DIMARTS"));
        put(4, idioma.get("DIMECRES"));
        put(5, idioma.get("DIJOUS"));
        put(6, idioma.get("DIVENDRES"));
        put(7, idioma.get("DISSABTE"));
    }};
        
    public static final Map<Integer, String> NOM_MESOS_DIM  = new HashMap<Integer, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put(1, idioma.get("GENER_DIM"));
        put(2, idioma.get("FEBRER_DIM"));
        put(3, idioma.get("MARC_DIM"));
        put(4, idioma.get("ABRIL_DIM"));
        put(5, idioma.get("MAIG_DIM"));
        put(6, idioma.get("JUNY_DIM"));
        put(7, idioma.get("JULIOL_DIM"));
        put(8, idioma.get("AGOST_DIM"));
        put(9, idioma.get("SETEMBRE_DIM"));
        put(10, idioma.get("OCTUBRE_DIM"));
        put(11, idioma.get("NOVEMBRE_DIM"));
        put(12, idioma.get("DESEMBRE_DIM"));
    }};
    
    
    public static final Map<Integer, String> TIPUS_USUARIS  = new HashMap<Integer, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put(1, idioma.get("USUARI"));
        put(4, idioma.get("ADMINISTRADOR"));
    }};
    
    
    public static final Map<String, String> TIPUS_ETIQUETA  = new HashMap<String, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put("P_VED", idioma.get("P_VEDELLA"));
        put("PP_VED", idioma.get("PP_VEDELLA"));
        put("P_IN", idioma.get("PES_INF_NUT"));
        put("PP_IN", idioma.get("PES_PREU_INF_NUT"));
        put("PP", idioma.get("PES_PREU"));
        put("P", idioma.get("PES"));
        
    }};
    
    
    public static final Map<String, String> EXTRA_TEMPERATURA  = new HashMap<String, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put("CONGELAT", idioma.get("TEXT_CONGELAT"));
        put("FRESC", idioma.get("TEXT_FRESC"));
        
    }};
    
    
    public static final Map<Integer, String> IMATGE_ETIQUETA_COMBO  = new HashMap<Integer, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put(1, idioma.get("10.08485/L"));
        put(2, idioma.get("40.18833/L"));
    }};
    
    
    public static final Map<Integer, String> IMATGE_ETIQUETA_NOM  = new HashMap<Integer, String>() {{
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        put(1, idioma.get("ETIQUETA-10"));
        put(2, idioma.get("ETIQUETA-40"));
    }};
    
    
    public static final Map<Integer, Double> RECARREC_EQUIVALENCIA_IVA  = new HashMap<Integer, Double>() {{
        put(21, 5.2);
        put(10, 1.4);
        put(4, 0.5);
    }};
    
    
    public static final Map<Integer, String> RUTES_CLIENTS  = new HashMap<Integer, String>() {{
        put(-1, "");
        put(1, "RUTA 1");
        put(2, "RUTA 2");
        put(3, "RUTA 3");
        put(4, "RUTA 4");
        put(5, "RUTA 5");
        put(6, "RUTA 6");
    }};
    
}