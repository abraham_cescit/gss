/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.permisos;

import controladors.base.CITServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.UsuarisPrivilegis;
import models.services.UsuarisPrivilegisService;

/**
 *
 * @author pc103
 */
public class PermisosUpdateController extends CITServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        boolean res = false;
        try {
            setBreuXML();
            if (inicialitzar(request, response, this)) {
                
                UsuarisPrivilegis usuariprivilegi = new UsuarisPrivilegis(request);
                res = UsuarisPrivilegisService.updateUsuariPrivilegi(usuariprivilegi);
                
                if (res) {
                    afegirRespostaBreu(RESULTAT,"ok");
                    afegirRespostaBreu(MISSATGE,getTexte("DESAT_EXIT"));
                } else {
                    afegirRespostaBreu(RESULTAT,"error");
                    afegirRespostaBreu(MISSATGE,getTexte("DESAT_ERROR"));
                }                
            } else {
                afegirRespostaBreu(RESULTAT,"error");
                afegirRespostaBreu(MISSATGE,getTexte("NO_TENS_PERMISOS"));
            }
        } catch (Exception ex) {
            Logger.getLogger(PermisosInsertController.class.getName()).log(Level.SEVERE, null, ex);   
            afegirRespostaBreu(RESULTAT,"error");
            afegirRespostaBreu(MISSATGE,getTexte("DESAT_ERROR")+": "+ex.getLocalizedMessage());
        } finally {
            super.processRequest(request, response);
        }
    }
    
}