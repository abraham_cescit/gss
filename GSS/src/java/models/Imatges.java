/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.InputStream;

/**
 *
 * @author Abraham M
 */
public class Imatges {
    
    public Integer idFoto;
    public Integer idRegistre;
    public String tipus;
    public String url;
    public InputStream streamFoto;

    public Imatges() {
    }

    public Imatges(Integer idFoto, Integer idRegistre, String tipus, String url) {
        this.idFoto = idFoto;
        this.idRegistre = idRegistre;
        this.tipus = tipus;
        this.url = url;
    }

    public Integer getIdFoto() {
        return idFoto;
    }

    public void setIdFoto(Integer idFoto) {
        this.idFoto = idFoto;
    }

    public Integer getIdRegistre() {
        return idRegistre;
    }

    public void setIdRegistre(Integer idRegistre) {
        this.idRegistre = idRegistre;
    }

    public String getTipus() {
        return tipus;
    }

    public void setTipus(String tipus) {
        this.tipus = tipus;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public InputStream getStreamFoto() {
        return streamFoto;
    }

    public void setStreamFoto(InputStream streamFoto) {
        this.streamFoto = streamFoto;
    }
    
}
