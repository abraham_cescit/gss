/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.usuaris;

import com.cescit.security.Rols;
import controladors.base.CITServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author pc103
 */
public class GetTipusUsuarisComboController extends CITServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {

                response.setContentType("text/xml;charset=UTF-8");
                response.setContentType("application/xml");

                PrintWriter out = response.getWriter();
                out.print("<?xml version='1.0' encoding='UTF-8'?>");
                out.print("<complete>");
                out.print("<option value=''></option>");
                for (Rols t : Rols.listaRoles()) {
                    out.print("<option value='" + t.getId() + "'>" + t.getNom() +"</option>");
                }
                out.print("</complete>");
            }
        } catch (SQLException ex) {
            Logger.getLogger(GetTipusUsuarisComboController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
