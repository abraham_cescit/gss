/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.registrediari;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controladors.base.CITServlet;
import static controladors.base.CITServlet.MISSATGE;
import static controladors.base.CITServlet.RESULTAT;
import static controladors.base.CITServlet.TEMPS;
import static controladors.base.CITServlet.TIPUS_BREU_XML;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Imatges;
import models.Vehicles;
import models.dto.RegistreDiariDTO;
import models.services.RegistreDiariService;
import models.services.VehiclesService;
import util.commons.Utils;

/**
 *
 * @author Abraham M
 */
public class RegistreDiariInsertEntradaController extends CITServlet {

    public static final String DESAT_EXIT = "DESAT_EXIT";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {
                Gson gson = new Gson();
                request.setCharacterEncoding("UTF-8");
                setTipus(TIPUS_BREU_XML);
                addDiccionari("registres");
                String respostaMetode = null;
                
                String json = Utils.getParameterString(request, "dades");
                java.lang.reflect.Type listType = new TypeToken<ArrayList<String>>(){}.getType();
                ArrayList<String> arrayJson = gson.fromJson(json, listType);
                // 1. id_espera // 2. id_registre // 3. id_empresa // 4. matricula
                // 5. hora_entrada // 6. pes_entrada // 7. foto_matricula // 8. foto_cmr 
                // 9. foto_dni // 10. foto_conductor // 11. foto_carga_entrada 
                String aux = arrayJson.get(0);
                Integer idEspera = null;
                if(aux != null){
                    idEspera = Integer.parseInt(arrayJson.get(0));
                }
                
                if( idEspera != null){
                    respostaMetode = entradaRegistres(arrayJson);
                    
                } else {
//                    respostaMetode = sortidaRegistres(arrayJson);
                    respostaMetode = "error";
                }                

                if(respostaMetode.equals("ok")){
                    afegirRespostaBreu(RESULTAT, "ok");
                    afegirRespostaBreu(MISSATGE, getTexte("REGISTRE_CREAT_EXIT"));
                    afegirRespostaBreu(TEMPS, "6000");
                } else {
                    afegirRespostaBreu(RESULTAT, "error");
                    afegirRespostaBreu(MISSATGE, getTexte("REGISTRE_CREAT_ERROR"));
                    afegirRespostaBreu(TEMPS, "0");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(RegistreDiariInsertEntradaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(RegistreDiariInsertEntradaController.class.getName()).log(Level.SEVERE, null, ex);
        }

        super.processRequest(request, response);
    }
    
    public String entradaRegistres(ArrayList<String> json) {
        String url = Utils.getProperty("URL");
        String result = null;
        // 1. id_espera // 2. id_registre // 3. id_empresa // 4. matricula
        // 5. hora_entrada // 6. pes_entrada // 7. foto_matricula // 8. foto_cmr 
        // 9. foto_dni // 10. foto_conductor // 11. foto_carga_entrada 
        String aux = json.get(0);
        Integer idEspera = null;
        if(aux != null){
            idEspera = Integer.parseInt(json.get(0));
        }

        Integer idRegistre = null;
        if(!json.get(1).isEmpty()){
            idRegistre = Integer.parseInt(json.get(1));
        }
        Integer idEmpresa = null;
        if(!json.get(2).isEmpty()){
            idEmpresa = Integer.parseInt(json.get(2));
        }
        String matricula = json.get(3);
        Vehicles vehicle = VehiclesService.getVehiclePerMatricula(matricula);
        Timestamp hora_entrada = new Timestamp(
                Utils.convertirStringToSqlTimestamp(json.get(4)).getTime());
        Double pes_entrada = null;
        if(!json.get(5).isEmpty()){
            pes_entrada = Double.parseDouble(json.get(5));
        }
        
        String foto_matricula = json.get(6);
        String foto_cmr = json.get(7);
        String foto_dni = json.get(8);
        String foto_conductor = json.get(9);
        String foto_cargaent = json.get(10);
        
        try {
            RegistreDiariDTO registre = new RegistreDiariDTO();
            registre.setIdEspera(idEspera);
            registre.setIdRegistre(idRegistre);
            registre.setIdEmpresa(idEmpresa);
            registre.setIdVehicle(vehicle.getIdVehicle());
            registre.setEntrada(hora_entrada);
            registre.setPesEntrada(pes_entrada);

            if (idRegistre == null) {
                Integer numRegistre = RegistreDiariService.ultimRegistre() + 1;
                ArrayList<Imatges> arrayImatges = new ArrayList<>();
                Imatges imgMtr = new Imatges();
                InputStream f_matricula = new java.net.URL(url + foto_matricula).openStream();
                imgMtr.setTipus("matricula");
                imgMtr.setUrl(foto_matricula);
                imgMtr.setStreamFoto(f_matricula);

                Imatges imgCMR = new Imatges();
                InputStream f_cmr = new java.net.URL(url + foto_matricula).openStream();
                imgCMR.setTipus("cmr");
                imgCMR.setUrl(foto_cmr);
                imgCMR.setStreamFoto(f_cmr);

                Imatges imgDNI = new Imatges();
                InputStream f_dni = new java.net.URL(url + foto_matricula).openStream();
                imgDNI.setTipus("dni");
                imgDNI.setUrl(foto_dni);
                imgDNI.setStreamFoto(f_dni);

                Imatges imgConductor = new Imatges();
                InputStream f_conductor = new java.net.URL(url + foto_matricula).openStream();
                imgConductor.setTipus("conductor");
                imgConductor.setUrl(foto_conductor);
                imgConductor.setStreamFoto(f_conductor);

                Imatges imgCarga = new Imatges();
                InputStream f_cargaent = new java.net.URL(url + foto_matricula).openStream();
                imgCarga.setTipus("cargaent");
                imgCarga.setUrl(foto_cargaent);
                imgCarga.setStreamFoto(f_cargaent);

                arrayImatges.add(imgMtr);
                arrayImatges.add(imgCMR);
                arrayImatges.add(imgDNI);
                arrayImatges.add(imgConductor);
                arrayImatges.add(imgCarga);
                registre.setFotosArray(arrayImatges);
                
                String fileName = Utils.getProperty("​CARPETA_IMATGES_REGISTRES");
                boolean resultat = guardarImagenEnSistemaDeFicheros(registre, fileName, numRegistre);
                if(resultat){
                    RegistreDiariService.insertEntradaRegistre(registre);
                } else {
                    result = "error";
                }
            } else {
                ArrayList<Imatges> arrayImatges = new ArrayList<>();
                Imatges imgMtr = new Imatges();
                imgMtr.setTipus("matricula");
                imgMtr.setUrl(foto_matricula);

                Imatges imgCMR = new Imatges();
                imgCMR.setTipus("cmr");
                imgCMR.setUrl(foto_cmr);

                Imatges imgDNI = new Imatges();
                imgDNI.setTipus("dni");
                imgDNI.setUrl(foto_dni);

                Imatges imgConductor = new Imatges();
                imgConductor.setTipus("conductor");
                imgConductor.setUrl(foto_conductor);

                Imatges imgCarga = new Imatges();
                imgCarga.setTipus("cargaent");
                imgCarga.setUrl(foto_cargaent);

                arrayImatges.add(imgMtr);
                arrayImatges.add(imgCMR);
                arrayImatges.add(imgDNI);
                arrayImatges.add(imgConductor);
                arrayImatges.add(imgCarga);
                registre.setFotosArray(arrayImatges);
                
                RegistreDiariService.updateRegistre(registre);
                ArrayList<Imatges> llistaFotos = RegistreDiariService.getImatgesbyRegistre(idRegistre);
                if(llistaFotos.size() > 0){
                    for( int i = 0; i < llistaFotos.size(); i++){
                        RegistreDiariService.updateImatgesByRegistre(llistaFotos.get(i).getIdFoto(), registre.getFotosArray().get(i).getUrl());
                    }
                }                        
            }
            result = "ok";

        } catch (IOException expa) {
            result = "error";
            Logger.getLogger(RegistreDiariInsertEntradaController.class.getName()).log(Level.SEVERE, null, expa);
        } catch (Exception expa) {
            result = "error";
            Logger.getLogger(RegistreDiariInsertEntradaController.class.getName()).log(Level.SEVERE, null, expa);
        }
        
        return result;
    }
    
    public String sortidaRegistres(ArrayList<String> json) {
        String url = Utils.getProperty("URL");
        String result = null;
        // 1. id_espera // 2. id_registre // 3. id_empresa // 4. matricula
        // 5. hora_entrada // 6. pes_entrada // 7. hora_sortida // 8. pes_sortida
        // 9. foto_matricula // 10. foto_cmr // 11. foto_dni // 12. foto_conductor
        // 13. foto_carga_entrada
        String aux = json.get(0);
        Integer idEspera = null;
        if(aux != null){
            idEspera = Integer.parseInt(json.get(0));
        }

        Integer idRegistre = null;
        if(!json.get(1).isEmpty()){
            idRegistre = Integer.parseInt(json.get(1));
        }
        Integer idEmpresa = null;
        if(!json.get(2).isEmpty()){
            idEmpresa = Integer.parseInt(json.get(2));
        }
        String matricula = json.get(3);
        Vehicles vehicle = VehiclesService.getVehiclePerMatricula(matricula);
        Timestamp hora_entrada = new Timestamp(
                Utils.toTimestampDHTMLX(json.get(4)).getTime());
        Double pes_entrada = null;
        if(!json.get(5).isEmpty()){
            pes_entrada = Double.parseDouble(json.get(5));
        }
        Timestamp hora_sortida = null;
        if(json.get(6) != null){
            hora_sortida = new Timestamp(
                Utils.toTimestampDHTMLX(json.get(6)).getTime());
        }
        Double pes_sortida = null;
        if(!json.get(7).isEmpty()){
            pes_sortida = Double.parseDouble(json.get(7));
        }
        String foto_cargasort = json.get(13);
        
        try {
            ArrayList<Imatges> arrayImatges = new ArrayList<>();
            Imatges imgCargaSort = new Imatges();
            InputStream f_cargasort = new java.net.URL(url + foto_cargasort).openStream();
            imgCargaSort.setTipus("cargasort");
            imgCargaSort.setUrl(foto_cargasort);
            imgCargaSort.setStreamFoto(f_cargasort);
            
                
            RegistreDiariDTO registre = new RegistreDiariDTO();
            registre.setIdEspera(idEspera);
            registre.setIdRegistre(idRegistre);
            registre.setIdEmpresa(idEmpresa);
            registre.setIdVehicle(vehicle.getIdVehicle());
            registre.setEntrada(hora_entrada);
            registre.setPesEntrada(pes_entrada);
            registre.setSortida(hora_sortida);
            registre.setPesSortida(pes_sortida);
            
            arrayImatges.add(imgCargaSort);
            registre.setFotosArray(arrayImatges);
            
            String fileName = Utils.getProperty("​CARPETA_IMATGES_REGISTRES");
            boolean resultat = guardarImagenEnSistemaDeFicheros(registre, fileName, idRegistre);
            if(resultat){
                if( RegistreDiariService.insertSortidaRegistre(registre) ){
                    result = "ok";
                } else {
                    result = "error";
                }
            } else {
                result = "error";
            }
            
        } catch (Exception expa) {
            result = "error";
            Logger.getLogger(RegistreDiariInsertEntradaController.class.getName()).log(Level.SEVERE, null, expa);
        }
        return result;
    }
    
    public static boolean guardarImagenEnSistemaDeFicheros(RegistreDiariDTO p_registre, String pFile, Integer registre)
        throws ServletException {
        boolean ok = false;
        File fDia = null;
        File fRegistre = null;
        Integer num_Fotos = p_registre.getFotosArray().size();
        
        String carpetaDia = Utils.avuiString();
        // Creacio de la carpeta del dia actual
        fDia = new File(pFile + carpetaDia);
        if(!fDia.exists()){
            // Si no existeix carpeta, es crea
            fDia.mkdirs();
        }
        // Creacio de la carpeta del registre
        fRegistre = new File(fDia + "/" + String.valueOf(registre));
        if(!fRegistre.exists()){
            // Si no existeix carpeta, es crea
            fRegistre.mkdirs();
        }
        String urlBaseDades = carpetaDia + "\\" + String.valueOf(registre);
        try {
            Integer pos = 0;
            do {
                // This is a sample inputStream, use your own.
                InputStream inputStream = p_registre.getFotosArray().get(pos).getStreamFoto();
                OutputStream outputStream = null;
                String ruta = null;
                ruta = fRegistre + "/" + p_registre.getFotosArray().get(pos).getTipus() + ".jpg";
                // Write the inputStream to a FileItem
                outputStream = new FileOutputStream(ruta);  
                p_registre.getFotosArray().get(pos).setUrl(urlBaseDades + "\\" + p_registre.getFotosArray().get(pos).getTipus() + ".jpg");

                int read = 0;
                byte[] bytes = new byte[1024];
                while ((read = inputStream.read(bytes)) != -1) {
                    outputStream.write(bytes, 0, read);
                }

                // Don't forget to release all the resources when you're done with them, or you may encounter memory/resource leaks.
                inputStream.close();
                outputStream.flush(); // This actually causes the bytes to be written.
                outputStream.close();
                num_Fotos--;
                pos++;
            } while(num_Fotos > 0);
            ok = true;
        } catch (IOException ex) {
            Logger.getLogger(RegistreDiariInsertEntradaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(RegistreDiariInsertEntradaController.class.getName()).log(Level.SEVERE, null, ex);
        }        

        // NOTE: You may also want to delete your outFile if you are done with it and dont want to take space on disk.
        
        return ok;
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}