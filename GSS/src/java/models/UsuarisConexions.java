/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Date;

/**
 *
 * @author fernando
 */
public class UsuarisConexions {
    
    Integer id;
    String login;
    String ip;
    String pais;
    Date data;
    
    public UsuarisConexions(){
        
    }

    public UsuarisConexions(Integer id, String login, String ip, String pais, Date data) {
        this.id = id;
        this.login = login;
        this.ip = ip;
        this.pais = pais;
        this.data = data;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
    
    
}
