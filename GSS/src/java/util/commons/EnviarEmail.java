
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.commons;

import java.io.File;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author sònia
 */
public class EnviarEmail {

    /**
     * Funció per l'enviament d'emails
     * @param arxius ArrayList amb els arxius a adjuntar al correu. NULL no adjuntarà cap
     * @param adrecaDesti Adreça destí del correu
     * @param assumpte Assumpte del correu
     * @param textCorreu Cos del correu
     * @param rutaAxiu ruta al arxiu adjunt. NULL no adjuntarà res
     * @return 
     */
    public static Boolean enviaEmailFitxer(String adrecaDesti, String assumpte, String textCorreu, String rutaAxiu) {
        try {

            String REMITENTE_CORREO = Utils.getProperty("REMITENTE_CORREO");
            String PASS_CORREO = Utils.getProperty("PASS_CORREO");
            String SERVIDOR_CORREO = Utils.getProperty("SERVIDOR_CORREO");
            String PUERTO_CORREO = Utils.getProperty("PUERTO_CORREO");
            String AUTH_CORREO = Utils.getProperty("AUTH_CORREO");
            String VERSION_DESARROLLO = Utils.getProperty("VERSION_DESARROLLO");
            String CORREO_DESARROLLO = Utils.getProperty("CORREO_DESARROLLO");
            String REMITENTE_CORREO_DESARROLLO = Utils.getProperty("REMITENTE_CORREO_DESARROLLO");
            String PASS_CORREO_DESARROLLO = Utils.getProperty("PASS_CORREO_DESARROLLO");
            
            Properties props = new Properties();   
            Session session = null;
                    
          
            if (VERSION_DESARROLLO.equals("true")) {
                adrecaDesti = CORREO_DESARROLLO;
                props.put("mail.smtp.host", "smtp.gmail.com");    
                props.put("mail.smtp.socketFactory.port", "465");   
                props.put("mail.smtp.socketFactory.class",    
                          "javax.net.ssl.SSLSocketFactory");    
                props.put("mail.smtp.auth", "true");    
                props.put("mail.smtp.port", "465"); 
                session = Session.getDefaultInstance(props,
                    new javax.mail.Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(REMITENTE_CORREO_DESARROLLO, PASS_CORREO_DESARROLLO);
                        }
                    });
                
            } else {
                props.put("mail.smtp.starttls.enable", "true");
                props.put("mail.smtp.auth", AUTH_CORREO);
                props.put("mail.smtp.host", SERVIDOR_CORREO);
                props.put("mail.smtp.port", PUERTO_CORREO);
                session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(REMITENTE_CORREO, PASS_CORREO);
                    }
                });

            }

//            BodyPart texto = new MimeBodyPart();
//            texto.setText(textCorreu);
//            texto.setContent(texto, "text/html; charset=utf-8");
            
            MimeBodyPart texto = new MimeBodyPart();
            texto.setContent(textCorreu, "text/html; charset=utf-8");

            BodyPart adjunto = new MimeBodyPart();
            MimeMultipart multiParte = new MimeMultipart();
            multiParte.addBodyPart(texto);
            
            
            if (rutaAxiu != null) {
                File fitxerEnviar = new File(rutaAxiu);
                if (fitxerEnviar.exists()) {
                    adjunto = new MimeBodyPart();
                    adjunto.setDataHandler(new DataHandler(new FileDataSource(fitxerEnviar)));
                    adjunto.setFileName(fitxerEnviar.getName());
                    multiParte.addBodyPart(adjunto);
                }
            }

            Message message = new MimeMessage(session);
            
            message.setFrom(new InternetAddress(REMITENTE_CORREO));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(adrecaDesti));
            message.addRecipient(Message.RecipientType.BCC, new InternetAddress(REMITENTE_CORREO));

            // message.setHeader("Content-Type", "text/html");
            message.setSubject(assumpte);
            message.setContent(multiParte);
            Transport.send(message);
           
            
        } catch (Exception e) {
            LoggerOut.sendLogger(null, e, EnviarEmail.class, "EnviarEmail - Error: " + e.getMessage());
            return false;
        }
        return true;
    }
}