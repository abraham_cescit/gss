/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dto;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import models.Imatges;
import util.commons.Utils;

/**
 *
 * @author Abraham M
 */
public class RegistreDiariDTO {
    
    public Integer idEspera;
    public Integer idRegistre;
    public Integer idEmpresa;
    public String nomEmpresa;
    public String idFiscal;
    public String telefon;
    public Integer idVehicle;
    public String matricula;
    public Timestamp entrada;
    public Double pesEntrada;
    public Timestamp sortida;
    public Double pesSortida;
    public Integer temps;
    public Double pesDiferencia;
    public ArrayList<String> fotos = new ArrayList<String>();
    public ArrayList<Imatges> fotosArray = new ArrayList<Imatges>();

    public RegistreDiariDTO() {
    
    }    

    public RegistreDiariDTO(Integer idRegistre, Integer idEmpresa, String nomEmpresa, 
            String idFiscal, String telefon, Integer idVehicle, String matricula, 
            Timestamp entrada, Double pesEntrada) {
        this.idRegistre = idRegistre;
        this.idEmpresa = idEmpresa;
        this.idFiscal = idFiscal;
        this.telefon = telefon;
        this.idVehicle = idVehicle;
        this.matricula = matricula;
        this.entrada = entrada;
        this.pesEntrada = pesEntrada;
    }

    public RegistreDiariDTO(Integer idRegistre, Integer idEmpresa, String nomEmpresa, 
            String idFiscal, String telefon, Integer idVehicle, String matricula, 
            Timestamp entrada, Double pesEntrada, Timestamp sortida, Double pesSortida, 
            Integer temps, Double pesDiferencia) {
        this.idRegistre = idRegistre;
        this.idEmpresa = idEmpresa;
        this.nomEmpresa = nomEmpresa;
        this.idFiscal = idFiscal;
        this.telefon = telefon;
        this.idVehicle = idVehicle;
        this.matricula = matricula;
        this.entrada = entrada;
        this.pesEntrada = pesEntrada;
        this.sortida = sortida;
        this.pesSortida = pesSortida;
        this.temps = temps;
        this.pesDiferencia = pesDiferencia;
    }
    
    public RegistreDiariDTO(HttpServletRequest request) {
        this.idRegistre = Utils.getParameterInteger(request, "idRegistre");
        this.idEmpresa = Utils.getParameterInteger(request, "idEmpresa");
        this.nomEmpresa = Utils.getParameterString(request, "nomempresa");
        this.idVehicle = Utils.getParameterInteger(request, "idVehicle");
        this.matricula = Utils.getParameterString(request, "matricula");
        this.entrada = Utils.getParameterTimestampSQL(request, "hentrada");
        this.pesEntrada = Utils.getParameterDouble(request, "pesentrada");
        this.sortida = Utils.getParameterTimestampSQL(request, "hsortida");
        this.pesSortida = Utils.getParameterDouble(request, "pessortida");
        this.idFiscal = Utils.getParameterString(request, "idFiscal");
        this.telefon = Utils.getParameterString(request, "telefon");
    }

    public Integer getIdRegistre() {
        return idRegistre;
    }

    public void setIdRegistre(Integer idRegistre) {
        this.idRegistre = idRegistre;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getNomEmpresa() {
        return nomEmpresa;
    }

    public void setNomEmpresa(String nomEmpresa) {
        this.nomEmpresa = nomEmpresa;
    }

    public String getIdFiscal() {
        return idFiscal;
    }

    public void setIdFiscal(String idFiscal) {
        this.idFiscal = idFiscal;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public Integer getIdVehicle() {
        return idVehicle;
    }

    public void setIdVehicle(Integer idVehicle) {
        this.idVehicle = idVehicle;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Timestamp getEntrada() {
        return entrada;
    }

    public void setEntrada(Timestamp entrada) {
        this.entrada = entrada;
    }

    public Double getPesEntrada() {
        return pesEntrada;
    }

    public void setPesEntrada(Double pesEntrada) {
        this.pesEntrada = pesEntrada;
    }

    public Timestamp getSortida() {
        return sortida;
    }

    public void setSortida(Timestamp sortida) {
        this.sortida = sortida;
    }

    public Double getPesSortida() {
        return pesSortida;
    }

    public void setPesSortida(Double pesSortida) {
        this.pesSortida = pesSortida;
    }

    public Integer getTemps() {
        return temps;
    }

    public void setTemps(Integer temps) {
        this.temps = temps;
    }

    public Double getPesDiferencia() {
        return pesDiferencia;
    }

    public void setPesDiferencia(Double pesDiferencia) {
        this.pesDiferencia = pesDiferencia;
    }

    public ArrayList<String> getFotos() {
        return fotos;
    }

    public void setFotos(ArrayList<String> fotos) {
        this.fotos = fotos;
    }

    public Integer getIdEspera() {
        return idEspera;
    }

    public void setIdEspera(Integer idEspera) {
        this.idEspera = idEspera;
    }

    public ArrayList<Imatges> getFotosArray() {
        return fotosArray;
    }

    public void setFotosArray(ArrayList<Imatges> fotosArray) {
        this.fotosArray = fotosArray;
    }
    
    
    
}
