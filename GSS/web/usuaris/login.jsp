<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.Locale"%>
<%@page import="util.commons.Utils"%>
<%@page import="controladors.base.Idioma"  %>
<% Idioma idioma = (Idioma) request.getAttribute("idioma");%>
<%
    String fonspantalla = util.commons.Utils.getProperty("FONS_PANTALLA");
    String iconoapache = util.commons.Utils.getProperty("ICONO_LOGIN");
%>
<style>
    h2 {
        text-align:center;
    }
    body {
         /*background-image: url(<%=util.commons.Utils.getProperty("FONS_PANTALLA")%>); url("paper.gif");*/
        background-image: url('imatges/logo_01.png');
        /*background-color: <%=util.commons.Utils.getProperty("FONS_LOGIN")%>;*/
        background-position: center center;
        background-repeat: no-repeat;
        background-attachment: inherit;
        background-size: cover;

    }
    #divlogin {
        background-color: <%=util.commons.Utils.getProperty("FONS_DIV_LOGIN")%>;
        padding:50px;
        margin-top:80px;
        border-radius: 10px;
    }
    .logo {
        margin-left: auto;
        margin-right: auto;
        display: block;
        margin-top: -30px;
        width: 100%;
    }
</style>
<body>
    <div id="divlogin" class="container" style="border: 5px solid #ac9867;">

    <form action="login" method="post" class="form-signin">
<!--        <img class="logo" src="../assets/images/logo/Logo_WEB.png"</>-->
        <img class="logo" src="imatges/imatges-get?a=<%=util.commons.Utils.getProperty("ICONO_LOGIN")%>"</>
        </br>
        <label for="inputEmail" class="sr-only"><%= idioma.get("USUARI")%>Usuario</label>
        <input type="text" name='login'  class="form-control" placeholder="<%= idioma.get("USUARI")%>" required autofocus>
        <label for="inputPassword" class="sr-only"><%= idioma.get("CONTRASENYA")%></label>
        <input type="password" name='password' class="form-control" placeholder="<%= idioma.get("CONTRASENYA")%>" required>
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Accedir</button>
        <br>
        <%
            if (request.getParameter("error") != null && request.getParameter("error").equals("1")) {
        %>
        
        <div class="alert alert-danger" role="alert">
            <%= idioma.get("LOGIN_INCORRECTE")%>
        </div>
        <% } 
        %>
        
    </form>

</div> <!-- /container -->
</body>
