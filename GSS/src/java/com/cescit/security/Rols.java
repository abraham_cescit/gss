/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cescit.security;

import controladors.base.Idioma;
import java.util.ArrayList;

/**
 *
 * @author fernando
 */
public class Rols {
    
    Integer id;
    String nom;

    public Rols(Integer id, String nom) {
        this.id = id;
        this.nom = nom;
    }
    
    public static Rols desdeInt(int valor) {
        Rols r = null;
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        switch (valor) {
            case 1:
                r = new Rols(1,idioma.get("USUARI"));
                break;
            case 4:
                r = new Rols(4,idioma.get("ADMINISTRADOR"));
                break;
        }
        return r;
    }
    
    public static ArrayList<Rols> listaRoles(){
        Idioma idioma = new Idioma(ControlAcces.getAtributoSession("lang"));
        ArrayList<Rols> lista = new ArrayList<Rols>();
        lista.add(new Rols(1,idioma.get("USUARI")));
        lista.add(new Rols(4,idioma.get("ADMINISTRADOR")));
        return lista;        
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
    
}
