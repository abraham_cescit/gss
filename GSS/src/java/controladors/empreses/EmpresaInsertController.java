/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.empreses;

import controladors.base.CITServlet;
import static controladors.base.CITServlet.MISSATGE;
import static controladors.base.CITServlet.RESULTAT;
import static controladors.base.CITServlet.TEMPS;
import static controladors.base.CITServlet.TIPUS_BREU_XML;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Empreses;
import models.services.EmpresesService;

/**
 *
 * @author Abraham M
 */
public class EmpresaInsertController extends CITServlet {

    public static final String DESAT_EXIT = "DESAT_EXIT";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {
                request.setCharacterEncoding("UTF-8");
                setTipus(TIPUS_BREU_XML);
                addDiccionari("empreses");

                Empreses empresa = new Empreses(request);
                try {

                    if (empresa.getIdEmpresa() == null) {
                        EmpresesService.insertarEmpresa(empresa);
                    } else {
                        EmpresesService.updateEmpresa(empresa);
                    }

                    afegirRespostaBreu(RESULTAT, "ok");
                    afegirRespostaBreu(MISSATGE, getTexte("EMPRESA_CREAT_EXIT"));
                    afegirRespostaBreu(TEMPS, "6000");

                } catch (Exception expa) {
                    afegirRespostaBreu(RESULTAT, "error");
                    afegirRespostaBreu(MISSATGE, getTexte("EMPRESA_CREAT_ERROR"));
                    afegirRespostaBreu(TEMPS, "0");
                    Logger.getLogger(EmpresaInsertController.class.getName()).log(Level.SEVERE, null, expa);
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(EmpresaInsertController.class.getName()).log(Level.SEVERE, null, ex);
        }

        super.processRequest(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
