/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Date;
import javax.servlet.http.HttpServletRequest;
import util.commons.Utils;

/**
 *
 * @author Abraham M
 */
public class RegistreDiari {
    
    public Integer idRegistre;
    public Integer idEmpresa;
    public Integer idVehicle;
    public Date entrada;
    public Double pesEntrada;
    public Date sortida;
    public Double pesSortida;
    public Date temps;
    public Double pesDiferencia;

    public RegistreDiari() {
        
    }

    public RegistreDiari(Integer idRegistre, Integer idEmpresa, Integer idVehicle, Date entrada, Double pesEntrada) {
        this.idRegistre = idRegistre;
        this.idEmpresa = idEmpresa;
        this.idVehicle = idVehicle;
        this.entrada = entrada;
        this.pesEntrada = pesEntrada;
    }

    public RegistreDiari(HttpServletRequest request) {
        this.idRegistre = Utils.getParameterInteger(request, "idRegistre");
        this.idEmpresa = Utils.getParameterInteger(request, "idEmpresa");
        this.idVehicle = Utils.getParameterInteger(request, "idVehicle");
        this.entrada = Utils.getParameterDateSQL(request, "hentrada");
        this.pesEntrada = Utils.getParameterDouble(request, "pesentrada");
    }

    public Integer getIdRegistre() {
        return idRegistre;
    }

    public void setIdRegistre(Integer idRegistre) {
        this.idRegistre = idRegistre;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdVehicle() {
        return idVehicle;
    }

    public void setIdVehicle(Integer idVehicle) {
        this.idVehicle = idVehicle;
    }

    public Date getEntrada() {
        return entrada;
    }

    public void setEntrada(Date entrada) {
        this.entrada = entrada;
    }

    public Double getPesEntrada() {
        return pesEntrada;
    }

    public void setPesEntrada(Double pesEntrada) {
        this.pesEntrada = pesEntrada;
    }

    public Date getSortida() {
        return sortida;
    }

    public void setSortida(Date sortida) {
        this.sortida = sortida;
    }

    public Double getPesSortida() {
        return pesSortida;
    }

    public void setPesSortida(Double pesSortida) {
        this.pesSortida = pesSortida;
    }

    public Date getTemps() {
        return temps;
    }

    public void setTemps(Date temps) {
        this.temps = temps;
    }

    public Double getPesDiferencia() {
        return pesDiferencia;
    }

    public void setPesDiferencia(Double pesDiferencia) {
        this.pesDiferencia = pesDiferencia;
    }
    
    
    
}
