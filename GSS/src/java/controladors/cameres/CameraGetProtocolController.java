/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.cameres;

import com.google.gson.Gson;
import controladors.base.CITServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Cameres;
import models.services.CameresService;
import util.commons.Utils;

/**
 *
 * @author Abraham M
 */
public class CameraGetProtocolController extends CITServlet {

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Gson gson = new Gson();
        try {
            if (inicialitzar(request, response, this)) {
                setNua();
                String posicioCamera = Utils.getParameterString(request, "nom");
                Cameres camera = CameresService.getProtocol(posicioCamera);

                PrintWriter out = response.getWriter();

                if (camera != null){
                    String JSON = gson.toJson(camera.getProtocol().toString());
                    out.print(JSON);
                } else {
                    String JSON = gson.toJson("ERROR");
                    out.print(JSON);
                }

                super.processRequest(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CameraGetProtocolController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CameraGetProtocolController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}