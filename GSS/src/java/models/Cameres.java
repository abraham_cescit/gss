/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.servlet.http.HttpServletRequest;
import util.commons.Utils;

/**
 *
 * @author Abraham M
 */
public class Cameres {
    
    public Integer idCamera;
    public String nom;
    public Integer posicio;
    public String descripcio;
    public String marca;
    public String model;
    public String protocol;

    public Cameres() {
    }

    public Cameres(Integer idCamera, String nom, Integer posicio, String descripcio, String marca, String model, String protocol) {
        this.idCamera = idCamera;
        this.nom = nom;
        this.posicio = posicio;
        this.descripcio = descripcio;
        this.marca = marca;
        this.model = model;
        this.protocol = protocol;
    }
    
    public Cameres(HttpServletRequest request) {
        this.idCamera = Utils.getParameterInteger(request, "idCamera");
        this.nom = Utils.getParameterString(request, "nom");
        this.posicio = Utils.getParameterInteger(request, "posicio");
        this.descripcio = Utils.getParameterString(request, "descripcio");
        this.marca = Utils.getParameterString(request, "marca");
        this.model = Utils.getParameterString(request, "model");
        this.protocol = Utils.getParameterString(request, "protocol");
    }

    public Integer getIdCamera() {
        return idCamera;
    }

    public void setIdCamera(Integer idCamera) {
        this.idCamera = idCamera;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getPosicio() {
        return posicio;
    }

    public void setPosicio(Integer posicio) {
        this.posicio = posicio;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
    
    @Override
    public String toString() {
        String s = "Id:  " + idCamera
                + "\nNom: " + nom
                + "\nPosició: " + posicio
                + "\nDescripció: " + descripcio
                + "\nMarca: " + marca
                + "\nModel: " + model
                + "\nProtocol: " + protocol;
        return s;
    }
    
}
