/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dto;

/**
 *
 * @author pc103
 */
public class HistoricUsuariDTO {
    
    // atributs
    
    private int idUsuari;
    private String nomUsuari;

    // constructors
    
    public HistoricUsuariDTO() {
    }

    public HistoricUsuariDTO(int idUsuari, String nomUsuari) {
        this.idUsuari = idUsuari;
        this.nomUsuari = nomUsuari;
    }
    
    // getters i setters

    public int getIdUsuari() {
        return idUsuari;
    }

    public void setIdUsuari(int idUsuari) {
        this.idUsuari = idUsuari;
    }

    public String getNomUsuari() {
        return nomUsuari;
    }

    public void setNomUsuari(String nomUsuari) {
        this.nomUsuari = nomUsuari;
    }
    
}
