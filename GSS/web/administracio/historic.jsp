<%@page import="util.commons.Utils"%>
<%@ page import="controladors.base.Idioma" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<% Idioma idioma = (Idioma) request.getAttribute("idioma");%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script>
    
    //=========
    // VARIABLES
    //=========
    var layoutBase, formCerca, gridHistoricCanvis;
    var params = ""; //Variable para guardar la búsqueda
    
    //============
    // FORMULARIS
    //============
    var contentFormCerca = [
        {type: "settings", position: "label-left", labelWidth: WIDTH_LABEL_FORM, inputWidth: "auto", offsetLeft: OFFSET_LEFT_FORM},
        {type: "calendar", name: "dataPetita", label: "<%=idioma.get("DESDE")%> : ", dateFormat: "%Y-%m-%d", serverDateFormat: "%Y-%m-%d", calendarPosition: "right"},
        {type: "calendar", name: "dataGran", label: "<%=idioma.get("FINS")%> : ", dateFormat: "%Y-%m-%d", serverDateFormat: "%Y-%m-%d", calendarPosition: "right"},
        {type: "combo", readonly: true, name: "nomTaula", id: "nomTaula", label: "<%=idioma.get("NOM_TAULA")%> : ",  connector: "historic-get-taulas", inputWidth: "125"},
        {type: "combo", readonly: true, name: "nomUsuari", id: "nomUsuari", label: "<%=idioma.get("NOM_USUARI")%> : ", connector: "historic-get-usuari", inputWidth: "125"},
        {type: "combo", readonly: true, name: "tipus", id: "tipus", label: "<%=idioma.get("TIPUS")%> : ", connector: "historic-get-tipus", inputWidth: "125"},
        {type: "button", name: "cercar", value: "<i class='fa fa-search iconboton'></i><%=idioma.get("CERCAR")%>", offsetTop: OFFSET_LEFT_FORM}
    ];
    
    //===============
    // CONTENT GRIDS
    //===============
    var contentGridHistoricCanvis = {
        image_path: "../codebase-pro/imgs/",
        columns: [
            {label: "<%=idioma.get("ID_FILA")%>", id: "id", width: 100, type: "ro", sort: "int", align: "right"},
            {label: "<%=idioma.get("DATA")%>", id: "data", width: 135, type: "ro", sort: "str", align: "left"},
            {label: "<%=idioma.get("ID_TAULA")%>", id: "idTaula", width: 100, type: "ro", sort: "int", align: "right"},
            {label: "<%=idioma.get("NOM_TAULA")%>", id: "nomTaula", width: 100, type: "ro", sort: "str", align: "left"},
            {label: "<%=idioma.get("ID_USUARI")%>", id: "idUsuari", width: 100, type: "ro", sort: "int", align: "left"},
            {label: "<%=idioma.get("NOM_USUARI")%>", id: "nomUsuari", width: 100, type: "ro", sort: "str", align: "left"},
            {label: "<%=idioma.get("TIPUS")%>", id: "tipus", width: 100, type: "ro", sort: "str", align: "left"},
            {label: "<%=idioma.get("VALORS")%>", id: "valors", width: '*', type: "ro", sort: "str", align: "left"}
        ],
        multiselect: false
    };
    
    //CUERPO
    function doOnload() {
        
        layoutBase = new dhtmlXLayoutObject({
           parent: "cuerpo",
           pattern : "2U",
           cells: [
               {id: "a", text:"<%=idioma.get("FORM_CERCA") %>"},
               {id: "b", text:"<%=idioma.get("GRID_HISTORIC") %>"}
           ]
        });
        
        layoutBase.cells("a").setWidth(WIDTH_LAYOUT_FORM_20);
        
        //FORM CERCA
        formCerca = layoutBase.cells("a").attachForm();
        formCerca.loadStruct(contentFormCerca);
        
        //EVENTS FORM CERCA
        
        formCerca.attachEvent("onButtonClick", function(name){
            if (name === "cercar"){
                var desde = formCerca.getItemValue("dataPetita",true);
                var fins = formCerca.getItemValue("dataGran",true);
                var taula = formCerca.getItemValue("nomTaula");
                var usuari = formCerca.getItemValue("nomUsuari");
                var tipus = formCerca.getItemValue("tipus");
                
                params = "?desde="+desde;
                params += "&fins="+fins;
                params += "&taula="+taula;
                params += "&usuari="+usuari;
                params += "&tipus="+tipus;
                
                progressOn(layoutBase);
                
                gridHistoricCanvis.clearAndLoad("historic-get"+params, function () {                    
                    progressOff(layoutBase);
                    if(gridHistoricCanvis.getRowsNum() > 0)
                        gridHistoricCanvis.filterByAll();
                    dhtmlx.message({type: "informacio", text: gridHistoricCanvis.getRowsNum()+" <%=idioma.get("RESULTATS_CERCA")%>", expire: "6000 "});                    
                });
            }
        });
        
        // DATE FORM CERCA
        
        var hoy = new Date();
        
        var anterior = new Date();
        anterior.setMonth(anterior.getMonth()-1);
        
        formCerca.setItemValue("dataPetita",anterior);
        formCerca.setItemValue("dataGran",hoy);
        
        //GRID
        gridHistoricCanvis = layoutBase.cells("b").attachGrid(contentGridHistoricCanvis);
        gridHistoricCanvis.attachHeader(",#text_filter,#numeric_filter,#select_filter,,#text_filter,#select_filter,#text_filter");
        gridHistoricCanvis.enableRowsHover(true, "hover");
        gridHistoricCanvis.enableMultiline(true);
        gridHistoricCanvis.setColumnHidden(0,true);
        gridHistoricCanvis.setColumnHidden(4,true);
        
    }
    
    //=========
    // FUNCIONS
    //=========
    
    document.addEventListener("DOMContentLoaded", doOnload);
</script>

<div id="cuerpo" style="width:100%;height:96%;overflow:hidden">
</div>