/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.servlet.http.HttpServletRequest;
import util.commons.Utils;

/**
 *
 * @author Abraham M
 */
public class Vehicles {
    
    public Integer idVehicle;
    public String matricula;
    public String tipus;
    public Double tara;

    public Vehicles() {
        
    }

    public Vehicles(Integer idVehicle, String matricula, String tipus, Double tara) {
        this.idVehicle = idVehicle;
        this.matricula = matricula;
        this.tipus = tipus;
        this.tara = tara;
    }
    
    public Vehicles(HttpServletRequest request) {
        this.idVehicle = Utils.getParameterInteger(request, "idVehicle");
        this.matricula = Utils.getParameterString(request, "matricula");
        this.tipus = Utils.getParameterString(request, "tipus");
        this.tara = Utils.getParameterDouble(request, "tara");
    }

    public Integer getIdVehicle() {
        return idVehicle;
    }

    public void setIdVehicle(Integer idVehicle) {
        this.idVehicle = idVehicle;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getTipus() {
        return tipus;
    }

    public void setTipus(String tipus) {
        this.tipus = tipus;
    }

    public Double getTara() {
        return tara;
    }

    public void setTara(Double tara) {
        this.tara = tara;
    }
    
    @Override
    public String toString() {
        String s = "Id:  " + idVehicle
                + "\nMatricula: " + matricula
                + "\nTipus: " + tipus
                + "\nTara: " + tara;
        return s;
    }
    
}
