/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.services;

import controladors.base.Idioma;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Empreses;
import models.Usuaris;
import persistencia.Bdades;

/**
 *
 * @author Abraham M
 */
public class EmpresesService {
    
    private static String NOM_TAULA = "empresa";

    public static ArrayList<Empreses> getEmpreses() {
        ArrayList<Empreses> r = new ArrayList<Empreses>();

        String q = "SELECT idempresa, nom, idfiscal, adreca, cpostal, poblacio, pais, telefon, contacte, email, web FROM empresa; ";

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                r.add(EmpresesService.obtenir(
                        rs.getInt("idempresa"),
                        rs.getString("nom"),
                        rs.getString("idfiscal"),
                        rs.getString("adreca"),
                        rs.getString("cpostal"),
                        rs.getString("poblacio"),
                        rs.getString("pais"),
                        rs.getString("telefon"),
                        rs.getString("contacte"),
                        rs.getString("email"),
                        rs.getString("web")));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(EmpresesService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }

    public static boolean insertarEmpresa(Empreses e) {
        String sql = "INSERT INTO empresa (nom,idfiscal,adreca,cpostal,poblacio,pais,telefon," 
                + "contacte,email,web) VALUES (?,?,?,?,?,?,?,?,?,?)";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        boolean r = false;
        int id = 0;

        try {
            
            int i = 1;
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstm.setString(i++, e.getNom());
            pstm.setString(i++, e.getIdFiscal());
            pstm.setString(i++, e.getAdreca());
            pstm.setString(i++, e.getCpostal());
            pstm.setString(i++, e.getPoblacio());
            pstm.setString(i++, e.getPais());
            pstm.setString(i++, e.getTelefon());
            pstm.setString(i++, e.getContacte());
            pstm.setString(i++, e.getEmail());
            pstm.setString(i++, e.getWeb());
            pstm.executeUpdate();
            rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }

            if (id != 0) {
                r = true;
                HistoricCanvisService.insertHistoric(id, HistoricCanvisService.TIPUS_INSERT, NOM_TAULA, e.toString());
            }

        } catch (SQLException sqle) {
            Logger.getLogger(EmpresesService.class.getName()).log(Level.SEVERE, null, sqle);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }

        if (r) {

        }

        return r;
    }

    public static boolean updateEmpresa(Empreses e) {
        boolean r = false;
        String sql = "UPDATE empresa SET " 
                + "nom = ?," 
                + "idfiscal = ?," 
                + "adreca = ?," 
                + "cpostal = ?," 
                + "poblacio = ?," 
                + "pais = ?," 
                + "telefon = ?," 
                + "contacte = ?," 
                + "email = ?," 
                + "web = ? " 
                + "WHERE idempresa = ?";
        
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            Integer i = 1;
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(i++, e.getNom());
            pstm.setString(i++, e.getIdFiscal());
            pstm.setString(i++, e.getAdreca());
            pstm.setString(i++, e.getCpostal());
            pstm.setString(i++, e.getPoblacio());
            pstm.setString(i++, e.getPais());
            pstm.setString(i++, e.getTelefon());
            pstm.setString(i++, e.getContacte());
            pstm.setString(i++, e.getEmail());
            pstm.setString(i++, e.getWeb());
            pstm.setInt(i++, e.getIdEmpresa());
            pstm.executeUpdate();

            r = true;
            if (true) {
                HistoricCanvisService.insertHistoric(e.getIdEmpresa(), HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, e.toString());
            }

        } catch (SQLException ex) {
            Logger.getLogger(EmpresesService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    public static boolean deleteEmpresa(int idEmpresa) {
        boolean r = false;

        String sql = "DELETE FROM empresa WHERE idempresa=?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idEmpresa);
            pstm.executeUpdate();
            r = true;
            HistoricCanvisService.insertHistoric(idEmpresa, HistoricCanvisService.TIPUS_DELETE, NOM_TAULA, "Empresa donada de baixa");
        } catch (SQLException ex) {
            Logger.getLogger(EmpresesService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    /**
     * Comprova si existeix a la base de dades un usuari amb aquest login
     *
     * @param login Login de l'usuari a comprovar.
     * @return true si existeix i false si no existeix a la base de dades.
     */
    public static boolean existeix(String login) {
        boolean r = false;
        String sql = "select login from usuari where login=?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, login);
            rs = pstm.executeQuery();
            r = rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    public static ArrayList<Usuaris> getUsuari() {
        ArrayList<Usuaris> r = new ArrayList<Usuaris>();

        String q = "select idusuari,login,nom,tipus,email,telefon,datanaixement,databaixa,idioma from usuari ";

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                r.add(UsuariService.obtenir(
                        rs.getInt("idusuari"),
                        rs.getString("login"),
                        rs.getString("nom"),
                        rs.getInt("tipus"),
                        rs.getString("email"),
                        rs.getString("telefon"),
                        rs.getDate("datanaixement"),
                        rs.getDate("databaixa"),
                        rs.getString("idioma")));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }

    public static Usuaris findEmailUsuari(int idUsuari) {
        Usuaris u = new Usuaris();
        String sql = "select email from usuari where idusuari=? ";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idUsuari);
            pstm.executeQuery();
            rs = pstm.getResultSet();
            if (rs.first()) {
                u.setEmail(rs.getString("email"));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return u;
    }

    public static ArrayList<Usuaris> findNomUsuari(String idioma) {
        idioma = Idioma.CAT;
        ArrayList<Usuaris> u = new ArrayList<Usuaris>();

        String sql = "select idusuari,nom,login "
                + "from usuari ";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            while (rs.next()) {
                Usuaris us = new Usuaris();
                us.setIdUsuari(rs.getInt("idusuari"));
                us.setNom(rs.getString("nom"));
                us.setLogin(rs.getString("login"));
                u.add(us);
            }

        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }

        return u;
    }

    public static Empreses obtenir(Integer idEmpresa, String nom, String idFiscal, String adreca, String cpostal, String poblacio, String pais, String telefon, String contacte, String email, String web) {
        return new Empreses(idEmpresa, nom, idFiscal, adreca, cpostal, poblacio, pais, telefon, contacte, email, web);
    }

}
