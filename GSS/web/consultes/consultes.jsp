<%-- 
    Document   : consultes
    Created on : 03-ago-2020, 16:34:29
    Author     : Abraham M
--%>

<%@ page import="com.cescit.security.Rols" %>
<%@ page import="java.util.Locale" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="util.commons.Utils" %>
<%@ page import="controladors.base.Idioma" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="models.*" %>
<%@ page import="models.services.*" %>
<% Idioma idioma = (Idioma) request.getAttribute("idioma");%>
<% String urlAPI = Utils.getProperty("URL");%>
<% String urlImatges = Utils.getProperty("​CARPETA_IMATGES_REGISTRES");%>
<% String rutaexcel = Utils.getProperty("RUTA_EXCEL");%>

<script>

    // =========================================================================
    // Variables
    // =========================================================================
    var pageLayout;
    var gridHistoric, formCerca;
    
    var foto_matricula = null;
    var foto_cmr = "";
    var foto_dni = "";
    var foto_conductor = "";
    var foto_cargaent = "";
    var foto_cargasort = "";
    
    var URL_CONSULTES_GET = "consultes-get";
    var URL_CERCA_ENTRE_DATES = "consultes-cerca-dates";

    // =========================================================================
    // Formularios
    // =========================================================================
    var contentFormConsultes = [
        {type: "settings", position: "label-left", labelWidth: 150, inputWidth: INPUT_SMALL, offsetLeft: OFFSET_LEFT_FORM},
        {type: "calendar", name: "dataInici", label: "DATA INICI",required: "true"},
        {type: "calendar", name: "dataFinal", label: "DATA FINAL", required: "true"},
        {type: "button", name: "cercar", label: "CERCAR", value: "Cercar", align: "center", offsetTop: OFFSET_LEFT_FORM, width: "80"}
    ];
    
    var contentFormRegistre = [
        {type: "settings", position: "label-left", labelWidth: WIDTH_LABEL_FORM, inputWidth: "auto", offsetLeft: 0},
        {type: "hidden", name: "idRegistre", label: "<%= idioma.get("ID_REGISTRE")%>", inputWidth: "auto"},
        {type: "block", width: 470, list:[
            {type: "hidden", name: "idEmpresa", label: "<%=idioma.get("ID_EMPRESA")%>", inputWidth: "auto"},
            {type: "input", name: "nomempresa", label: "<%= idioma.get("NOM_EMPRESA")%>", inputWidth: "auto"}
        ]},
        {type: "block", width: 470, list:[
            {type: "hidden", name: "idVehicle", label: "<%=idioma.get("ID_VEHICLE")%>", inputWidth: "auto"},
            {type: "input", name: "matricula", label: "<%=idioma.get("MATRICULA")%>", inputWidth: "auto"}
        ]},
        {type: "block", width: 470, list:[
            {type: "calendar", name: "hentrada", label: "<%= idioma.get("HORA_ENTRADA")%>", dateFormat: "%Y-%m-%d %H:%i:%s"}
        ]},
        {type: "block", width: 470, list:[
            {type: "input", name: "pesentrada", label: "<%= idioma.get("PES_ENTRADA")%>", inputWidth: "auto"}
        ]},
        {type: "block", width: 470, list:[
            {type: "calendar", name: "hsortida", label: "<%= idioma.get("HORA_SORTIDA")%>", dateFormat: "%Y-%m-%d %H:%i:%s"}
        ]},
        {type: "block", width: 470, list:[
            {type: "input", name: "pessortida", label: "<%= idioma.get("PES_SORTIDA")%>", inputWidth: "auto"}
        ]},
        {type: "hidden", name: "foto_mtr", label: "<%= idioma.get("ID_REGISTRE")%>", inputWidth: "auto"},
        {type: "hidden", name: "foto_cmr", label: "<%= idioma.get("ID_REGISTRE")%>", inputWidth: "auto"},
        {type: "hidden", name: "foto_dni", label: "<%= idioma.get("ID_REGISTRE")%>", inputWidth: "auto"},
        {type: "hidden", name: "foto_conductor", label: "<%= idioma.get("ID_REGISTRE")%>", inputWidth: "auto"},
        {type: "hidden", name: "foto_carga", label: "<%= idioma.get("ID_REGISTRE")%>", inputWidth: "auto"}
    ];
    
    var contentFormRegistreFotos = [
        {type: "settings", position: "label-left", labelWidth: "200", inputWidth: "auto", offsetLeft: 20},
        {type: "label", name: "fotoEntrada", label: "<%= idioma.get("FOTO_MATRICULA")%>", inputWidth: "auto"},
        {type: "container", name:"contenidorMatricula", inputWidth: 320, inputHeight: 240},
        {type: "label", name: "fotoSortida", label: "<%= idioma.get("FOTO_CMR")%>", inputWidth: "auto"},
        {type: "container", name:"contenidorCMR", inputWidth: 320, inputHeight: 240},
        {type: "newcolumn"},
        {type: "label", name: "fotoSortida", label: "<%= idioma.get("FOTO_DNI")%>", inputWidth: "auto"},
        {type: "container", name:"contenidorDNI", inputWidth: 320, inputHeight: 240},
        {type: "label", name: "fotoSortida", label: "<%= idioma.get("FOTO_CONDUCTOR")%>", inputWidth: "auto"},
        {type: "container", name:"contenidorConductor", inputWidth: 320, inputHeight: 240},
        {type: "newcolumn"},
        {type: "label", name: "fotoSortida", label: "<%= idioma.get("FOTO_CARGA_ENTRADA")%>", inputWidth: "auto"},
        {type: "container", name:"contenidorCargaEnt", inputWidth: 320, inputHeight: 240},
        {type: "label", name: "fotoSortida", label: "<%= idioma.get("FOTO_CARGA_SORTIDA")%>", inputWidth: "auto"},
        {type: "container", name:"contenidorCargaSort", inputWidth: 320, inputHeight: 240}
    ];

    // =========================================================================
    // Grids
    // =========================================================================
    var contentGridHistoric = {
        image_path: "codebase-pro/imgs/",
        columns: [
            {label: "<%= idioma.get("ID_REGISTRE")%>", id: "idRegistre", width: '100', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("HORA_ENTRADA")%>", id: "hentrada", width: '80', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("ID_VEHICLE")%>", id: "idVehicle", width: '100', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("MATRICULA")%>", id: "matricula", width: '100', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("ID_EMPRESA")%>", id: "idEmpresa", width: '100', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("NOM_EMPRESA")%>", id: "nomempresa", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("ID_FISCAL")%>", id: "idFiscal", width: '100', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("PES_ENTRADA")%>", id: "pesentrada", width: '100', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("HORA_SORTIDA")%>", id: "hsortida", width: '80', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("PES_SORTIDA")%>", id: "pessortida", width: '100', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("DIF_TEMPS")%>", id: "difTemps", width: '100', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("DIF_PES")%>", id: "difPes", width: '100', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_mtr", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_cmr", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_dni", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_conductor", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("FOTO")%>", id: "foto_carga", width: '*', type: "ro", sort: "str", align: "right"}
            ],
        multiselect: false
    };

    // =========================================================================
    // Carga
    // =========================================================================    
    function doOnLoad() {

        principalLayout = new dhtmlXLayoutObject({
            parent: "layoutConsultes",
            pattern: "3W",
            cells: [
                {id: "a", text: "CERCA", width: 150},
                {id: "b", text: "HISTÒRIC"},
                {id: "c", text: "REGISTRE", width: 300}
            ]
        });
        formCerca = principalLayout.cells("a").attachForm();
	formCerca.loadStruct(contentFormConsultes);
        
        // Grid reserves -------------------------------------------------------
        gridHistoric = principalLayout.cells("b").attachGrid(contentGridHistoric);
        gridHistoric.attachHeader(",#text_filter,,#combo_filter,,#combo_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
        gridHistoric.load(URL_CONSULTES_GET);
        gridHistoric.setColumnHidden(0, true);
        gridHistoric.setColumnHidden(2, true);
        gridHistoric.setColumnHidden(4, true);
        gridHistoric.setColumnHidden(12, true);
        gridHistoric.setColumnHidden(13, true);
        gridHistoric.setColumnHidden(14, true);
        gridHistoric.setColumnHidden(15, true);
        gridHistoric.setColumnHidden(16, true);
        gridHistoric.attachFooter(",#stat_count,,,,,,,,,,#stat_total",[ "", "text-align:right;font-size:16px;font-weight: bold;", "", "", "", "", "", "", "", "", "", "text-align:center;font-size:16px;font-weight: bold;"]);
        inicialitzarGrid(gridHistoric);
        
        gridHistoric.attachEvent("onRowSelect", function (id, ind) {
            foto_matricula = gridHistoric.cells(id, gridHistoric.getColIndexById("foto_mtr")).getValue();
            foto_cmr = gridHistoric.cells(id, gridHistoric.getColIndexById("foto_cmr")).getValue();
            foto_dni = gridHistoric.cells(id, gridHistoric.getColIndexById("foto_dni")).getValue();
            foto_conductor = gridHistoric.cells(id, gridHistoric.getColIndexById("foto_conductor")).getValue();
            foto_cargaent = gridHistoric.cells(id, gridHistoric.getColIndexById("foto_carga")).getValue();
        });
        
//        gridHistoric.attachEvent("onRowSelect", function (id,ind){
//            if (gridHistoric.cells(id,gridHistoric.getColIndexById("tipusReserva")).getValue() === "D") {
//                //updateFormReserva();
//            } else {
//                //resetFormReserva();
//                //formReserva.disableItem("desa");
//            }
//        });
        
        // Grid reserves - toolbar ---------------------------------------------
        var toolBar = principalLayout.cells("b").attachToolbar();
        toolBar.setIconset("awesome");
        toolBar.addButton("exportar", 1, "Exportar", "fas fa-file-excel", null);
        toolBar.setIconSize(18);
        toolBar.setAlign("right");
        
        // Grid reserves - toolbar - events ------------------------------------
        toolBar.attachEvent("onClick", function (id) {
            if (id == "exportar") {
//                gridHistoric.toExcel("<%=rutaexcel%>");
            }

        });
        
        // Form reserves - events ----------------------------------------------
        formCerca.attachEvent("onButtonClick", function (name) {
            if (name == "cercar"){
                mostraCercarDates();
            }
        });
        
        // =======================================
        // FORMULARI REGISTRES
        // =======================================
        formRegistre = principalLayout.cells("c").attachForm();
        formRegistre.loadStruct(contentFormRegistre);
        formRegistre.bind(gridHistoric);
        formRegistre.attachEvent("onButtonClick", function (nom) {
            if (nom === "recollirMatricula") {
                getProtocol(4);
            } else if (nom === "recollirCMR") {
                getProtocol(4);
            } else if (nom === "recollirDni") {
                getProtocol(4);
            } else if (nom === "recollirFotoCara") {
                getProtocol(4);
            } else if (nom === "recollirFotoCarga") {
                getProtocol(4);
            }
        });
        
        // =======================================
        // TOOLBAR REGISTRES
        // =======================================
        
        toolBar = principalLayout.cells("c").attachToolbar();
        toolBar.setIconset("awesome");
        toolBar.addButton("fotos", 1, "", "fas fa-camera", null);
        toolBar.setItemToolTip("fotos", "<%= idioma.get("FOTOS_REGISTRE_TOOLTIP")%>");
        
        toolBar.setIconSize(18);
        toolBar.setAlign("left");

        toolBar.attachEvent("onClick", function (id) {
            if (id === "fotos") {
                var idRegistre = formRegistre.getItemValue("idRegistre");
                if(idRegistre === ""){
                    alert("Registre buit");
                } else {
                    obrirWindowFotosRegistre(idRegistre);
                }
            }
        });

        // Funcions ------------------------------------------------------------
        function mostraCercarDates() {
            var inici = formCerca.getItemValue("dataInici", true);
            var final = formCerca.getItemValue("dataFinal", true);
            if (final >= inici){
                principalLayout.progressOn();
                gridHistoric.clearAndLoad(URL_CERCA_ENTRE_DATES + "?dataInici=" 
                + inici + "&dataFinal=" + final, function (r) {
                    if(gridHistoric.getRowsNum() > 0) {
                        gridHistoric.filterByAll();
                    } else {
                        dhtmlx.message({text: 'No hi han registres', expire: "6000 "});
                    }
                    principalLayout.progressOff();
                });
            } else {
                dhtmlx.message({type:"error", text: 'La data final es menor que la inicial', expire: "6000 "});
                formCerca.setItemValue("dataInici", "");
                formCerca.setItemValue("dataFinal", "");
            }
        }
        
        function getProtocol(camera) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var opt = this.responseText;
                    if (opt === "ERROR") {
                        alert("ERROR");
                    } else {
                        obrirWindowVideo(opt);
                    }
                }
            };
            param = "idCamera=" + camera;

            xhttp.open("POST", URL_BUSCAR_PROTOCOL, true);
            xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhttp.send(param);
        }
        
        function obrirWindowFotosRegistre(){
            var imatge_matricula = null;
            var imatge_cmr = null;
            var imatge_dni = null;
            var imatge_conductor = null;
            var imatge_carga_entrada = null;
            var imatge_carga_sortida = null;
            if(foto_matricula !== ""){
                imatge_matricula = "imatgesregistres/imatges-get?a=" + foto_matricula;
            } else {
                imatge_matricula = "imatges/imatges-get?a=logo_07.png";
            }
            if(foto_cmr !== ""){
                imatge_cmr = "imatgesregistres/imatges-get?a=" + foto_cmr;
            } else {
                imatge_cmr = "imatges/imatges-get?a=logo_07.png";
            }
            if(foto_dni !== ""){
                imatge_dni = "imatgesregistres/imatges-get?a=" + foto_dni;
            } else {
                imatge_dni = "imatges/imatges-get?a=logo_07.png";
            }
            if(foto_conductor !== ""){
                imatge_conductor = "imatgesregistres/imatges-get?a=" + foto_conductor;
            } else {
                imatge_conductor = "imatges/imatges-get?a=logo_07.png";
            }
            if(foto_cargaent !== ""){
                imatge_carga_entrada = "imatgesregistres/imatges-get?a=" + foto_cargaent;
            } else {
                imatge_carga_entrada = "imatges/imatges-get?a=logo_07.png";
            }
            if(foto_cargasort !== ""){
                imatge_carga_sortida = "imatgesregistres/imatges-get?a=" + foto_cargasort;
            } else {
                imatge_carga_sortida = "imatges/imatges-get?a=logo_07.png";
            }
            
            winFotos = new dhtmlXWindows().createWindow("winFotos", 0, 0, 1060, 640);
            winFotos.setText("<%= idioma.get("TITOL_FOTOS_REGISTRE")%>");
            inicializarWindowWithClose(winFotos);
            formFotos = winFotos.attachForm();
            formFotos.loadStruct(contentFormRegistreFotos);
            var containerMatricula = formFotos.getContainer("contenidorMatricula");
            containerMatricula.innerHTML = '<img id="camera" width=" '+ 320 + '" height="'+ 240 + '" src="' + imatge_matricula + '" frameborder="0" allowfullscreen></img>';
            var containerCMR = formFotos.getContainer("contenidorCMR");
            containerCMR.innerHTML = '<img id="camera" width=" '+ 320 + '" height="'+ 240 + '" src="' + imatge_cmr + '" frameborder="0" allowfullscreen></img>';
            var containerDNI = formFotos.getContainer("contenidorDNI"); 
            containerDNI.innerHTML = '<img id="camera" width=" '+ 320 + '" height="'+ 240 + '" src="' + imatge_dni + '" frameborder="0" allowfullscreen></img>';
            var containerConductor = formFotos.getContainer("contenidorConductor");
            containerConductor.innerHTML = '<img id="camera" width=" '+ 320 + '" height="'+ 240 + '" src="' + imatge_conductor + '" frameborder="0" allowfullscreen></img>';
            var containerCargaEnt = formFotos.getContainer("contenidorCargaEnt");
            containerCargaEnt.innerHTML = '<img id="camera" width=" '+ 320 + '" height="'+ 240 + '" src="' + imatge_carga_entrada + '" frameborder="0" allowfullscreen></img>';
            var containerCargaSort = formFotos.getContainer("contenidorCargaSort");
            containerCargaSort.innerHTML = '<img id="camera" width=" '+ 320 + '" height="'+ 240 + '" src="' + imatge_carga_sortida + '" frameborder="0" allowfullscreen></img>';
            
        }
        
}

    document.addEventListener("DOMContentLoaded", doOnLoad);
</script>
<div id="layoutConsultes" style="width:99.8%;height:90%;overflow:hidden" ></div>

