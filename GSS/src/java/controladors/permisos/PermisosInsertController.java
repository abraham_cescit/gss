package controladors.permisos;

import controladors.base.CITServlet;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.UsuarisPrivilegis;
import models.services.UsuarisPrivilegisService;

public class PermisosInsertController extends CITServlet {

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        boolean res;
        try {
            if (inicialitzar(request, response, this)) {
                setBreuXML();
                UsuarisPrivilegis usuariprivilegi = new UsuarisPrivilegis(request);
                res = UsuarisPrivilegisService.insertUsuariPrivilegi(usuariprivilegi);

                if (res) {
                    afegirRespostaBreu(RESULTAT, "ok");
                    afegirRespostaBreu(MISSATGE, getTexte("DESAT_EXIT"));
                    afegirRespostaBreu(TEMPS, "6000");
                } else {
                    afegirRespostaBreu(RESULTAT, "error");
                    afegirRespostaBreu(MISSATGE, getTexte("DESAT_ERROR"));
                }
            } else {
                afegirRespostaBreu(RESULTAT, "error");
                afegirRespostaBreu(MISSATGE, getTexte("NO_TENS_PERMISOS"));
            }
        } catch (Exception ex) {
            Logger.getLogger(PermisosInsertController.class.getName()).log(Level.SEVERE, null, ex);
            afegirRespostaBreu(RESULTAT, "error");
            afegirRespostaBreu(MISSATGE, getTexte("DESAT_ERROR") + ": " + ex.getCause());
        } finally {
            super.processRequest(request, response);
        }
    }

}
