/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Timestamp;
import javax.servlet.http.HttpServletRequest;
import util.commons.Utils;

/**
 *
 * @author Abraham M
 */
public class Espera {
    
    public Integer idEspera;
    public String matricula;
    public Integer linea;
    public String hentrada;
    public Timestamp arribada;
    public String foto;
    public Timestamp sortida;
    public String registrat;
    

    public Espera() {
    }
    
    public Espera(String matricula, Integer linea, String foto) {
        this.matricula = matricula;
        this.linea = linea;
        this.foto = foto;
    }
    
    public Espera(Integer idEspera, String matricula, Integer linea) {
        this.idEspera = idEspera;
        this.matricula = matricula;
        this.linea = linea;
    }

    public Espera(HttpServletRequest request) {
        this.idEspera = Utils.getParameterInteger(request, "idespera");
        this.matricula = Utils.getParameterString(request, "matricula");
        this.linea = Utils.getParameterInteger(request, "linea");
    }

    public Integer getidEspera() {
        return idEspera;
    }

    public void setidEspera(Integer idEspera) {
        this.idEspera = idEspera;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public String getArribadaString() {
        return hentrada;
    }

    public void setArribadaString(String arribadaString) {
        this.hentrada = arribadaString;
    }

    public Timestamp getArribada() {
        return arribada;
    }

    public void setArribada(Timestamp arribada) {
        this.arribada = arribada;
    }

    public Timestamp getSortida() {
        return sortida;
    }

    public void setSortida(Timestamp sortida) {
        this.sortida = sortida;
    }

    public String getRegistrat() {
        return registrat;
    }

    public void setRegistrat(String registrat) {
        this.registrat = registrat;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
    
}
