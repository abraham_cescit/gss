/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.servlet.http.HttpServletRequest;
import util.commons.Utils;

/**
 *
 * @author Abraham M
 */
public class Params {
    
    private Integer idParam;
    private String clau;
    private String valor;

    public Params() {
    }

    public Params(Integer idParam, String clau, String valor) {
        this.idParam = idParam;
        this.clau = clau;
        this.valor = valor;
    }
    
    public Params(HttpServletRequest request) {
        this.idParam = Utils.getParameterInteger(request, "idParam");
        this.clau = Utils.getParameterString(request, "clau");
        this.valor = Utils.getParameterString(request, "valor");
    }

    public Integer getIdParam() {
        return idParam;
    }

    public void setIdParam(Integer idParam) {
        this.idParam = idParam;
    }

    public String getClau() {
        return clau;
    }

    public void setClau(String clau) {
        this.clau = clau;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
    @Override
    public String toString() {
        return "Param{" + "idParam=" + idParam + ", clau=" + clau + ", valor=" + valor + '}';
    }
    
}
