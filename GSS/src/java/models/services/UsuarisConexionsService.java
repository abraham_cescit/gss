/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.UsuarisConexions;
import persistencia.Bdades;

/**
 *
 * @author fernando
 */
public class UsuarisConexionsService {
    
    public static boolean insertConexio(UsuarisConexions uc){
        String sql = "insert into usuarisconexions "
                + "(login, ip, pais) "
                + "values (?,?,?)";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        boolean r = false;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, uc.getLogin());
            pstm.setString(2, uc.getIp());
            pstm.setString(3, uc.getPais());
            pstm.executeUpdate();
            r = true;
        } catch (SQLException sqle) {
            Logger.getLogger(UsuarisConexionsService.class.getName()).log(Level.SEVERE, null, sqle);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }
    
}
