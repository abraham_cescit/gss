<%-- 
    Document   : empreses
    Created on : 18-sep-2020, 18:19:04
    Author     : Abraham M
--%>

<%@page import="com.cescit.security.Rols"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="util.commons.Utils"%>
<%@page import="controladors.base.Idioma"%>
<%@page import="java.util.ArrayList" %>
<%@page import="models.*" %>
<%@page import="models.services.*" %>
<% Idioma idioma = (Idioma) request.getAttribute("idioma");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script>
    // =======================================
    // VARIABLES GLOBALS
    // =======================================
    
    var registreLayout, principalLayout, gridEmpreses;
    var formEmpresa;
    
    // CONTROLADORS
    var URL_DESA_EMPRESA = "empresa-insert";
    var URL_BUSCAR_EMPRESES = "empreses-get";
    var URL_ESBORRA_EMPRESA = "empresa-delete";
    
    // =======================================
    // DEFINICIÓN FORMULARIOS
    // =======================================
    var contentFormEdicio = [
        {type: "settings", position: "label-left", labelWidth: WIDTH_LABEL_FORM, inputWidth: "auto", offsetLeft: OFFSET_LEFT_FORM},
        {type: "hidden", name: "idEmpresa", label: "<%=idioma.get("ID_EMPRESA")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "nom", label: "<%=idioma.get("NOM")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "idFiscal", label: "<%=idioma.get("ID_FISCAL")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "adreca", label: "<%=idioma.get("ADRECA")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "cpostal", label: "<%=idioma.get("CPOSTAL")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "poblacio", label: "<%=idioma.get("POBLACIO")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "pais", label: "<%=idioma.get("PAIS")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "telefon", label: "<%=idioma.get("TELEFON")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "contacte", label: "<%=idioma.get("CONTACTE")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "email", label: "<%=idioma.get("EMAIL")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "web", label: "<%=idioma.get("WEB")%>", inputWidth: INPUT_MEDIUM},
        {type: "button", name: "desa", value: "<%=idioma.get("DESA")%>", offsetTop: OFFSET_LEFT_FORM}
    ];
    
    // =======================================
    // DEFINICI�N GRID
    // =======================================
    var contentGridEmpreses = {
        image_path: "codebase-pro/imgs/",
        columns: [
            {label: "<%= idioma.get("ID_EMPRESA")%>", id: "idEmpresa", width: '50', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("NOM")%>", id: "nom", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("ID_FISCAL")%>", id: "idFiscal", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("ADRECA")%>", id: "adreca", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("CPOSTAL")%>", id: "cpostal", width: INPUT_SMALL, type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("POBLACIO")%>", id: "poblacio", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("PAIS")%>", id: "pais", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("TELEFON")%>", id: "telefon", width: '*', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("CONTACTE")%>", id: "contacte", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("EMAIL")%>", id: "email", width: '*', type: "ro", sort: "int", align: "left"},
            {label: "<%= idioma.get("WEB")%>", id: "web", width: '*', type: "ro", sort: "str", align: "left"}
        ],
        multiselect: false
    };

    function doOnLoad() {
        principalLayout = new dhtmlXLayoutObject({
            parent: "layoutEmpreses",
            pattern: "2U",
            cells: [
                {id: "a", text: "<%= idioma.get("LLISTAT_D_EMPRESES")%>"},
                {id: "b", text: "<%= idioma.get("EDICIO_D_EMPRESA")%>"}
            ]
        });
        
        principalLayout.cells("b").setWidth(350);
        
        gridEmpreses = principalLayout.cells("a").attachGrid(contentGridEmpreses);
        gridEmpreses.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
        gridEmpreses.load(URL_BUSCAR_EMPRESES);
        inicialitzarGrid(gridEmpreses);
        
        formEmpresa = principalLayout.cells("b").attachForm();
        formEmpresa.loadStruct(contentFormEdicio);
        
        <%
            String rutaexcel = Utils.getProperty("RUTA_EXCEL");
        %>

        var toolBar = principalLayout.cells("a").attachToolbar();
        toolBar.setIconset("awesome");
        toolBar.addButton("exportar", 1, "Exportar", "fas fa-file-excel", null);
        toolBar.setIconSize(18);
        toolBar.setAlign("right");
        toolBar.attachEvent("onClick", function (id) {
            if (id == "exportar") {
//                gridEmpreses.toExcel("<%=rutaexcel%>");
            }
        });

        formEmpresa.bind(gridEmpreses);
        formEmpresa.attachEvent("onButtonClick", function (nom) {
            if (nom == "desa") {
                enviar();
            }
        });

        function enviar() {
            if (formEmpresa.validate()) {
                principalLayout.progressOn();
                formEmpresa.send(URL_DESA_EMPRESA, "POST", function (r) {
                    dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: getAjax(r, TEMPS)});
                    gridEmpreses.clearAndLoad(URL_BUSCAR_EMPRESES, function () {
                        principalLayout.progressOff();
                        formEmpresa.clear();
                    });
                });
            }
        }
        
        toolBar = principalLayout.cells("b").attachToolbar();
        toolBar.setIconset("awesome");
        toolBar.addButton("nou", 1, "", "fas fa-plus", null);
        toolBar.setItemToolTip("nou", "<%= idioma.get("CREAR_EMPRESA_TOOLTIP")%>");
        toolBar.addButton("esborrar", 2, "", "fas fa-trash", null);
        toolBar.setItemToolTip("esborrar", "<%= idioma.get("ESBORRAR_EMPRESA_TOOLTIP")%>");
        toolBar.setIconSize(18);
        toolBar.setAlign("left");
        
        toolBar.attachEvent("onClick", function (id) {
            if (id == "nou") {
                formEmpresa.clear();
            }
            if (id == "esborrar") {
                var id = gridEmpreses.getSelectedRowId();
                if (id != null) {
                    dhtmlx.confirm({
                        title: "<%= idioma.get("CONFIRMAR")%>",
                        type: "confirm-warning",
                        text: "<%= idioma.get("CONFIRMACIO_ESBORRAT_EMPRESA")%>",
                        ok: "<%= idioma.get("ACCEPTAR")%>",
                        cancel: "<%= idioma.get("CANCELAR")%>",
                        callback: function (r) {
                            if (r) {
                                window.dhx.ajax.post(URL_ESBORRA_EMPRESA, "id=" + id, function (r) {
                                    var resultat = getAjax(r, RESULTAT);
                                    if (resultat == ESBORRAT_EXIT) {
                                        gridEmpreses.clearAndLoad(URL_BUSCAR_EMPRESES, function () {
                                            gridEmpreses.filterByAll();
                                        });
                                        formEmpresa.clear();
                                        dhtmlx.message({text: '<%= idioma.get("ESBORRAT_EXIT")%>', expire: "6000 "});
                                    }
                                });
                            }
                        }
                    });
                } else {
                    dhtmlx.message({
                        text: "<%= idioma.get("ELIMINAR_SELECCIO")%>",
                        expire: "10000"
                    });
                }
            }
        });
        
    }
    
    document.addEventListener("DOMContentLoaded", doOnLoad);
</script>

<div id="layoutEmpreses" style="width:99.8%;height:90%;overflow:hidden" ></div>


