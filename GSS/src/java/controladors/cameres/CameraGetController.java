/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.cameres;

import controladors.base.CITServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Cameres;
import models.services.CameresService;
import util.commons.UtilsGrid;

/**
 *
 * @author Abraham M
 */
public class CameraGetController extends CITServlet {

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {
                response.setContentType("text/xml;charset=UTF-8");
                response.setContentType("application/xml");
                addDiccionari("cameres");
                setNua();

                ArrayList<Cameres> llista = CameresService.getCameres();

                PrintWriter out = response.getWriter();
                out.print("<?xml version='1.0' encoding='UTF-8'?>");
                out.print("<rows>");
                for (Cameres c : llista) {
                    out.print(UtilsGrid.toXMLPK(c.getIdCamera()+ ""));
                    out.print(UtilsGrid.toXML(c.getIdCamera() + ""));
                    out.print(UtilsGrid.toXML(c.getNom() + ""));
                    out.print(UtilsGrid.toXML(c.getPosicio() + ""));
                    out.print(UtilsGrid.toXML(c.getDescripcio() + ""));
                    out.print(UtilsGrid.toXML(c.getMarca() + ""));
                    out.print(UtilsGrid.toXML(c.getModel() + ""));
                    out.print(UtilsGrid.toXML(c.getProtocol() + ""));
                    out.print(UtilsGrid.toXMLend());
                }
                out.print("</rows>");

                super.processRequest(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CameraGetController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CameraGetController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}