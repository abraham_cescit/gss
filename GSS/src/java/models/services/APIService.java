/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Espera;
import persistencia.Bdades;
import util.commons.Utils;

/**
 *
 * @author Abraham M
 */
public class APIService {
    
    private static String NOM_TAULA = "espera";

    public static ArrayList<Espera> getEntrades() {
        ArrayList<Espera> r = new ArrayList<Espera>();

        String q = "SELECT idespera, matricula, linea, DATE_FORMAT(TIMESTAMP(arribada), '%Y-%m-%d %H:%i:%s') as horaArribada, sortida, foto " +
            "FROM espera WHERE sortida IS NULL";

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                Espera ent = new Espera();
                ent.setidEspera(rs.getInt("idespera"));
                ent.setMatricula(rs.getString("matricula"));
                ent.setLinea(rs.getInt("linea"));
                ent.setArribadaString(rs.getString("horaArribada"));
                ent.setSortida(rs.getTimestamp("sortida"));
                if ( VehiclesService.existeix(rs.getString("matricula"))){
                    ent.setRegistrat("SI");
                } else {
                    ent.setRegistrat("NO");
                }
                ent.setFoto(rs.getString("foto"));
                r.add(ent);
            }
        } catch (SQLException sqle) {
            Logger.getLogger(APIService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }
    
    public static ArrayList<Espera> getTotesEntrades() {
        ArrayList<Espera> r = new ArrayList<Espera>();

        String q = "SELECT idespera, matricula, linea, arribada, sortida FROM espera";

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                Espera ent = new Espera();
                ent.setidEspera(rs.getInt("idespera"));
                ent.setMatricula(rs.getString("matricula"));
                ent.setLinea(rs.getInt("linea"));
                ent.setArribada(rs.getTimestamp("arribada"));
                ent.setSortida(rs.getTimestamp("sortida"));
                r.add(ent);
            }
        } catch (SQLException sqle) {
            Logger.getLogger(APIService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }

    public static boolean insertarEntrada(Espera espera) {
        String sql = "INSERT INTO espera (matricula,linea,foto) VALUES (?,?,?)";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        boolean r = false;
        int id = 0;

        try {
            
            int i = 1;
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstm.setString(i++, espera.getMatricula());
            pstm.setInt(i++, espera.getLinea());
            pstm.setString(i++, espera.getFoto());
            pstm.executeUpdate();
            rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }

            if (id != 0) {
                r = true;
                HistoricCanvisService.insertHistoric(id, HistoricCanvisService.TIPUS_INSERT, NOM_TAULA, espera.toString());
            }

        } catch (SQLException sqle) {
            Logger.getLogger(APIService.class.getName()).log(Level.SEVERE, null, sqle);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }

        return r;
    }

    public static boolean updateEntrada(Espera e) {
        boolean r = false;
        String sql = "UPDATE espera SET " 
                + "matricula = ?," 
                + "arribada = ?" 
                + "WHERE idespera = ?";
        
        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            Integer i = 1;
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(i++, e.getMatricula());
            pstm.setTimestamp(i++, e.getArribada());
            pstm.setInt(i++, e.getidEspera());
            pstm.executeUpdate();

            r = true;
            if (true) {
                HistoricCanvisService.insertHistoric(e.getidEspera(), HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, e.toString());
            }

        } catch (SQLException ex) {
            Logger.getLogger(APIService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    public static boolean deleteEntrada(int idEspera) {
        boolean r = false;

        String sql = "DELETE FROM espera WHERE idespera=?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idEspera);
            pstm.executeUpdate();
            r = true;
            HistoricCanvisService.insertHistoric(idEspera, HistoricCanvisService.TIPUS_DELETE, NOM_TAULA, "Entrada donada de baixa");
        } catch (SQLException ex) {
            Logger.getLogger(APIService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }
    
    public static Espera getEntrada() {
        Espera r = new Espera();

        String q = "select idespera,matricula,linea,arribada,sortida from espera ";

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                r.setidEspera(rs.getInt("idespera"));
                r.setMatricula(rs.getString("matricula"));
                r.setLinea(rs.getInt("linea"));
                r.setArribada(rs.getTimestamp("arribada"));
                r.setSortida(rs.getTimestamp("sortida"));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(APIService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }

}
