<%-- 
    Document   : vehicles
    Created on : 21-sep-2020, 11:18:29
    Author     : Abraham M
--%>

<%@page import="com.cescit.security.Rols"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="util.commons.Utils"%>
<%@page import="controladors.base.Idioma"%>
<%@page import="java.util.ArrayList" %>
<%@page import="models.*" %>
<%@page import="models.services.*" %>
<% Idioma idioma = (Idioma) request.getAttribute("idioma");%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script>
    // =======================================
    // VARIABLES GLOBALS
    // =======================================
    
    var principalLayout, gridVehicles;
    var formVehicle;
    
    // CONTROLADORS
    var URL_DESA_VEHICLE = "vehicle-insert";
    var URL_BUSCAR_VEHICLE = "vehicles-get";
    var URL_ESBORRA_VEHICLE = "vehicle-delete";
    
    // =======================================
    // DEFINICIÓN FORMULARIOS
    // =======================================
    var contentFormEdicio = [
        {type: "settings", position: "label-left", labelWidth: WIDTH_LABEL_FORM, inputWidth: "auto", offsetLeft: OFFSET_LEFT_FORM},
        {type: "hidden", name: "idVehicle", label: "<%=idioma.get("ID_VEHICLE")%>", inputWidth: INPUT_MEDIUM},
        {type: "input", name: "matricula", label: "<%=idioma.get("MATRICULA")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "tipus", label: "<%=idioma.get("TIPUS")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "input", name: "tara", label: "<%=idioma.get("TARA")%>", inputWidth: INPUT_MEDIUM, required: true},
        {type: "button", name: "desa", value: "<%=idioma.get("DESA")%>", offsetTop: OFFSET_LEFT_FORM}
    ];
    
    // =======================================
    // DEFINICI�N GRID
    // =======================================
    var contentGridVehicles = {
        image_path: "codebase-pro/imgs/",
        columns: [
            {label: "<%= idioma.get("ID_VEHICLE")%>", id: "idVehicle", width: '80', type: "ro", sort: "int", align: "right"},
            {label: "<%= idioma.get("MATRICULA")%>", id: "matricula", width: '200', type: "ro", sort: "str", align: "right"},
            {label: "<%= idioma.get("TIPUS")%>", id: "tipus", width: '*', type: "ro", sort: "str", align: "left"},
            {label: "<%= idioma.get("TARA")%>", id: "tara", width: '400', type: "ro", sort: "str", align: "right"}
        ],
        multiselect: false
    };

    function doOnLoad() {
        principalLayout = new dhtmlXLayoutObject({
            parent: "layoutVehicles",
            pattern: "2U",
            cells: [
                {id: "a", text: "<%= idioma.get("LLISTAT_DE_VEHICLES")%>"},
                {id: "b", text: "<%= idioma.get("EDICIO_DE_VEHICLE")%>"}
            ]
        });
        
        principalLayout.cells("b").setWidth(350);
        
        gridVehicles = principalLayout.cells("a").attachGrid(contentGridVehicles);
        gridVehicles.attachHeader("#text_filter,#text_filter,#combo_filter,#text_filter");
        gridVehicles.load(URL_BUSCAR_VEHICLE);
        inicialitzarGrid(gridVehicles);
        
        formVehicle = principalLayout.cells("b").attachForm();
        formVehicle.loadStruct(contentFormEdicio);
        
        <%
            String rutaexcel = Utils.getProperty("RUTA_EXCEL");
        %>

        var toolBar = principalLayout.cells("a").attachToolbar();
        toolBar.setIconset("awesome");
        toolBar.addButton("exportar", 1, "Exportar", "fas fa-file-excel", null);
        toolBar.setIconSize(18);
        toolBar.setAlign("right");
        toolBar.attachEvent("onClick", function (id) {
            if (id == "exportar") {
//                gridVehicles.toExcel("<%=rutaexcel%>");
            }
        });

        formVehicle.bind(gridVehicles);
        formVehicle.attachEvent("onButtonClick", function (nom) {
            if (nom == "desa") {
                enviar();
            }
        });

        function enviar() {
            if (formVehicle.validate()) {
                principalLayout.progressOn();
                formVehicle.send(URL_DESA_VEHICLE, "POST", function (r) {
                    dhtmlx.message({type: getAjax(r, RESULTAT), text: getAjax(r, MISSATGE), expire: getAjax(r, TEMPS)});
                    gridVehicles.clearAndLoad(URL_BUSCAR_VEHICLE, function () {
                        principalLayout.progressOff();
                        formVehicle.clear();
                    });
                });
            }
        }
        
        toolBar = principalLayout.cells("b").attachToolbar();
        toolBar.setIconset("awesome");
        toolBar.addButton("nou", 1, "", "fas fa-plus", null);
        toolBar.setItemToolTip("nou", "<%= idioma.get("CREAR_VEHICLE_TOOLTIP")%>");
        toolBar.addButton("esborrar", 2, "", "fas fa-trash", null);
        toolBar.setItemToolTip("esborrar", "<%= idioma.get("ESBORRAR_VEHICLE_TOOLTIP")%>");
        toolBar.setIconSize(18);
        toolBar.setAlign("left");
        
        toolBar.attachEvent("onClick", function (id) {
            if (id == "nou") {
                formVehicle.clear();
            }
            if (id == "esborrar") {
                var id = gridVehicles.getSelectedRowId();
                if (id != null) {
                    dhtmlx.confirm({
                        title: "<%= idioma.get("CONFIRMAR")%>",
                        type: "confirm-warning",
                        text: "<%= idioma.get("CONFIRMACIO_ESBORRAT_VEHICLE")%>",
                        ok: "<%= idioma.get("ACCEPTAR")%>",
                        cancel: "<%= idioma.get("CANCELAR")%>",
                        callback: function (r) {
                            if (r) {
                                window.dhx.ajax.post(URL_ESBORRA_VEHICLE, "id=" + id, function (r) {
                                    var resultat = getAjax(r, RESULTAT);
                                    if (resultat == ESBORRAT_EXIT) {
                                        gridVehicles.clearAndLoad(URL_BUSCAR_VEHICLE, function () {
                                            gridVehicles.filterByAll();
                                        });
                                        formVehicle.clear();
                                        dhtmlx.message({text: '<%= idioma.get("ESBORRAT_EXIT")%>', expire: "6000 "});
                                    }
                                });
                            }
                        }
                    });
                } else {
                    dhtmlx.message({
                        text: "<%= idioma.get("ELIMINAR_SELECCIO")%>",
                        expire: "10000"
                    });
                }
            }
        });
        
    }
    
    document.addEventListener("DOMContentLoaded", doOnLoad);
</script>

<div id="layoutVehicles" style="width:99.8%;height:90%;overflow:hidden" ></div>



