/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.commons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

/**
 *
 * @author fernando
 */
public class Utils extends UtilsBase {
    
    /**
     * Coge una propiedad del fichero de propiedades situado en la carpeta bin del tomcat
     * @param name Nombre de la propiedad
     * @return Devuelve el valor de la propiedad
     */
    public static String getProperty(String name){
        String p = "";
        Properties prop;
        FileInputStream input = null;
        
        try{
            prop = new Properties();
            input = new FileInputStream("gss.properties");
            prop.load(new InputStreamReader(input, Charset.forName("UTF-8")));
            p = prop.getProperty(name);
        } catch(IOException e) {
            LoggerOut.sendLogger(null, e, Utils.class, "Error al recoger la propiedad de BD: "+name);
        }finally{
            try {
                prop = null;
                if(input != null)
                    input.close();
            } catch (IOException ex) {
                LoggerOut.sendLogger(null, ex, Utils.class, "Error al cerrar el fichero de propiedades");
            }
        }
        return p;
    }
    
    /**
     * Coge una propiedad del fichero de propiedades situado en la carpeta bin del tomcat
     * @param name Nombre de la propiedad
     * @return Devuelve el valor de la propiedad
     */
    public static String[] getArrayProperty(String name){
        String p = "";
        String [] retorn = null;
        Properties prop;
        FileInputStream input = null;
        
        try{
            prop = new Properties();
            input = new FileInputStream("gss.properties");
            prop.load(new InputStreamReader(input, Charset.forName("UTF-8")));
            p = prop.getProperty(name);
            retorn = p.split("#");
        } catch(IOException e) {
            LoggerOut.sendLogger(null, e, Utils.class, "Error al recoger la propiedad de BD: "+name);
        }finally{
            try {
                prop = null;
                if(input != null)
                    input.close();
            } catch (IOException ex) {
                LoggerOut.sendLogger(null, ex, Utils.class, "Error al cerrar el fichero de propiedades");
            }
        }
        return retorn;
    }
    
    /**
     * Redondea un double
     * @param numero numero a arrodonir
     * @return Double numero arrodonit
     */
    public static Double round(Double numero) {
        return Math.rint(numero * 100) / 100;
    }
    
    /**
     * Redondea un double
     * @param numero numero a arrodonir
     * @return Double numero arrodonit
     */
    public static Double round3Dec(Double numero) {
        return Math.rint(numero * 1000) / 1000;
    }

    /**
     * Devuelve la fecha-hora de ahora en formato String
     * @return String d'ara
     */
    public static String now() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }
    
    /**
     * Devuelve la fecha-hora de ahora en formato String
     * @return String amb data hora
     */
    public static String avuiString() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return sdf.format(cal.getTime());
    }
    
    public static String avuiLot() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String date = sdf.format(cal.getTime());
        return date.substring(2, date.length());
    }

    /**
     * Devuelve la fecha de hoy en formato sql.Date
     * @return Date d'avui
     */
    public static java.sql.Date avui() {
        java.sql.Date sqlDate = new java.sql.Date(System.currentTimeMillis());
        return sqlDate;
    }
    
    public static String avuiMes() {
        String[] mesos = {"GENER","FEBRER","MARC","ABRIL","MAIG","JUNY","JULIOL","AGOST","SETEMBRE","OCTUBRE","NOVEMBRE","DESEMBRE"};
        return mesos[Calendar.getInstance().get(Calendar.MONTH)];
    }
    
    /**
     * Devuelve la fechahora de hoy en formato sql.Date
     * @return timestamp en format sql.Date
     */
    public static java.sql.Timestamp avuiTimestamp() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return timestamp;
    }
    
    public static String convertirFechaString(java.util.Date date) {
        Format formatter = new SimpleDateFormat(DATE_FORMAT_NOW);
        return formatter.format(date);
    }
    
    public static String convertirFechaEspanyolString(java.util.Date date) {
        Format formatter = new SimpleDateFormat(DATE_FORMAT_ESPANYOL);
        return formatter.format(date);
    }
    
    public static java.sql.Date convertirStringToSqlDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_NOW);
        java.util.Date parsed = null;
        if(date != null && !date.equals("")){
            try {
                parsed = format.parse(date);
            } catch (ParseException ex) {
                LoggerOut.sendLogger(null, ex, Utils.class, date);
            }
            return new java.sql.Date(parsed.getTime());
        }else
            return null;
    }
    
    public static java.sql.Timestamp convertirStringToSqlTimestamp(String datetime) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_NOW_HORA);
        java.util.Date parsed = null;
        if(datetime != null && !datetime.equals("")){
            try {
                parsed = format.parse(datetime);
            } catch (ParseException ex) {
                LoggerOut.sendLogger(null, ex, Utils.class, datetime);
            }
            return new java.sql.Timestamp(parsed.getTime());
        }else
            return null;
    }
    
    public static java.sql.Timestamp extreureHora(Timestamp a){
        SimpleDateFormat format = new SimpleDateFormat(HOUR_FORMAT);
        java.util.Date parsed = null;
        if(a != null && !a.equals("")){
            try {
                parsed = format.parse(a.toString());
            } catch (ParseException ex) {
                LoggerOut.sendLogger(null, ex, Utils.class, a.toString());
            }
            return new java.sql.Timestamp(parsed.getTime());
        }else
            return null;
    }
    
    public static java.sql.Timestamp convertirArrayStringToSqlTimestamp(String date, String time) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_NOW_HORA);
        java.util.Date parsed = null;
        if((date != null && !date.equals("")) && (time != null && !time.equals(""))){
            try {
                parsed = format.parse(date + " " + time);
            } catch (ParseException ex) {
                LoggerOut.sendLogger(null, ex, Utils.class, date + " " + time);
            }
            return new java.sql.Timestamp(parsed.getTime());
        }else
            return null;
    }
        
    /**
     * Coge un parámetro String y lo limpia.
     * @param request objecte request del controlador
     * @param parametro nom del paràmetre
     * @return El valor de parámetro o vacío.
     */
    public static String getParameterString(HttpServletRequest request, String parametro){
        String retorno = "";
        retorno = request.getParameter(parametro);
        if(StringUtils.isEmpty(retorno))
            retorno = "";
        retorno = retorno.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").trim();    
        return retorno;
    }
    
    /**
     * Coge un parámetro String y lo limpia.
     * @param request objecte request del controlador
     * @param parametro nom del paràmetre
     * @return El valor de parámetro o vacío.
     */
    public static String[] getParameterArrayString(HttpServletRequest request, String parametro){
        String[] retorno;
        retorno = request.getParameterValues(parametro);
        return retorno;
    }
    
    /**
     * Coge un parámetro Integer y lo limpia.
     * @param request objecte request del controlador
     * @param parametro nom del paràmetre
     * @return El valor numérico o null.
     */
    public static Integer getParameterInteger(HttpServletRequest request, String parametro){
        Integer retorno = null;
        String temp = request.getParameter(parametro);        
        try{
            if(temp != null)
                retorno = Integer.parseInt(temp);
        }catch(NumberFormatException ex){
            retorno = null;
        }catch(Exception ex){
            retorno = null;
        }        
        return retorno;
    }
    
    /**
     * Coge un parámetro Integer y lo limpia.
     * @param request objecte request del controlador
     * @param parametro nom del paràmetre
     * @return El valor numérico o null.
     */
    public static Integer getParameterIntegerZeroOnNull(HttpServletRequest request, String parametro){
        Integer retorno = 0;
        String temp = request.getParameter(parametro);        
        try {
            if(temp != null)
                retorno = Integer.parseInt(temp);
        }catch(NumberFormatException ex){
            retorno = 0;
        }catch(Exception ex){
            retorno = 0;
        }        
        return retorno;
    }
    
    /**
     * Coge un parámetro Integer y lo limpia o devuelve -1.
     * @param request objecte request del controlador
     * @param parametro nom del paràmetre
     * @return El valor numérico o null.
     */
    public static Integer getParameterIntegerNegatiu(HttpServletRequest request, String parametro){
        Integer retorno = null;
        String temp = request.getParameter(parametro);        
        try{
            if(temp != null)
                retorno = Integer.parseInt(temp);
            else 
                retorno = -1;
        }catch(NumberFormatException ex){
            retorno = -1;
        }catch(Exception ex){
            retorno = -1;
        }        
        return retorno;
    }
    
    /**
     * Coge un parámetro Integer y lo limpia.
     * @param request objecte request del controlador
     * @param parametro nom del paràmetre
     * @return El valor numérico o null.
     */
    public static Double getParameterDouble(HttpServletRequest request, String parametro){
        Double retorno = 0.0;
        String temp = request.getParameter(parametro);        
        try{
            if(temp != null && temp != "")
                retorno = round(Double.parseDouble(temp.replace(",",".")));
        }catch(NumberFormatException ex){
            retorno = null;
        }catch(Exception ex){
            retorno = null;
        }
        return retorno;
    }
    
    /**
     * Coge un parámetro Integer y lo limpia.
     * @param request objecte request del controlador
     * @param parametro nom del paràmetre
     * @return El valor numérico o null.
     */
    public static Double getParameterDouble3Dec(HttpServletRequest request, String parametro){
        Double retorno = 0.0;
        String temp = request.getParameter(parametro);        
        try{
            if(temp != null && temp != "")
                retorno = round3Dec(Double.parseDouble(temp));
        }catch(NumberFormatException ex){
            retorno = null;
        }catch(Exception ex){
            retorno = null;
        }
        return retorno;
    }
    
    /**
     * Coge un parámetro Integer y lo limpia.
     * @param request objectre request del controlador
     * @param parametro nom del paràmetre
     * @return El valor numérico o null.
     */
    public static Double getParameterDoubleZeroOnNull(HttpServletRequest request, String parametro){
        Double retorno = 0.0;
        String temp = request.getParameter(parametro);        
        try{
            if(temp != null)
                retorno = round(Double.parseDouble(temp));
        }catch(NumberFormatException ex){
            retorno = 0.0;
        }catch(Exception ex){
            retorno = 0.0;
        }
        return retorno;
    }
    
    /**
     * Coge un parámetro Integer y lo limpia.
     * @param request objecte request del paràmetre
     * @param parametro nom del paràmetre
     * @return El valor numérico o null.
     */
    public static Boolean getParameterBoolean(HttpServletRequest request, String parametro){
        Boolean retorno = false;
        String temp = request.getParameter(parametro); 
        Integer temp2 = null;
        try{
            if(temp != null)
                temp2 = Integer.parseInt(temp);
        }catch(NumberFormatException ex){
            retorno = null;
        }catch(Exception ex){
            retorno = null;
        } 
        if(temp2 != null && temp2 == 1)
            retorno = true;
            
        return retorno;
    }
    
    /**
     * Convierte un parámetro a java.util.Date
     * @param request objecte request del controlador
     * @param parametro nom del paràmetre
     * @return una fecha java.util.Date o un null
     */
    public static java.util.Date getParameterDateUtil(HttpServletRequest request, String parametro){
        java.util.Date retorno = null;
        String temp = request.getParameter(parametro); 
        DateFormat df = new SimpleDateFormat(DATE_FORMAT_NOW);
        try {
            retorno = df.parse(temp);
        } catch (ParseException ex) {
            retorno = null;
        } catch (Exception e){
            retorno = null;
        }
        
        return retorno;
    }
    
    /**
     * Convierte un parámetro a java.sql.Date
     * @param request request del controlador
     * @param parametro nombre del parametre
     * @return una fecha java.sql.Date o un null
     */
    public static java.sql.Date getParameterDateSQL(HttpServletRequest request, String parametro){
        java.sql.Date retorno = null;
        String temp = request.getParameter(parametro); 
        DateFormat df = new SimpleDateFormat(DATE_FORMAT_NOW);
        java.util.Date tempDate = null;
        try {
            tempDate = df.parse(temp);
            retorno = new java.sql.Date(tempDate.getTime());
        } catch (ParseException ex) {
            retorno = null;
        } catch (Exception ex) {
            retorno = null;
        }            
        return retorno;
    }

    /**
     * Convierte un parámetro a java.sql.Timestamp
     * @param request request del controlador
     * @param parametro nombre del parametre
     * @return un timestamp java.sql.Timestamp o un null
     */
    public static java.sql.Timestamp getParameterTimestampSQL(HttpServletRequest request, String parametro){
        java.sql.Timestamp retorno = null;
        String temp = request.getParameter(parametro); 
        DateFormat df = new SimpleDateFormat(DATE_FORMAT_NOW_HORA);
        java.util.Date tempDate = null;
        try {
            tempDate = df.parse(temp);
            retorno = new java.sql.Timestamp(tempDate.getTime());
        } catch (ParseException ex) {
            retorno = null;
        } catch (Exception ex) {
            retorno = null;
        }            
        return retorno;
    }
    
    public static String escapeXML(String xml){
        return StringEscapeUtils.escapeXml10(xml);
    }
    
    public static String objectToJSON(Object o){
        String r = "";
        try {
            ObjectMapper mapper = new ObjectMapper();
            r = mapper.writeValueAsString(o);            
        } catch (JsonProcessingException ex) {
            LoggerOut.sendLogger(null, ex, Utils.class, "Fallo al convertir un objeto a JSON");
        }
        return r;
    }
    
    public static String[] toArrayString(String separador, String variable){
        if(variable != null && StringUtils.isNotEmpty(variable))
            return variable.split(separador);
        else
            return null;
    }
    
    public static String toTimestampHoraMinut(Timestamp a) {
        String b = "";
        if (a == null) {
            b = "";
        } else {
            b = new SimpleDateFormat("HH:mm").format(a);
        }
        return b;
    }
    
    public static Timestamp toTimestampDHTMLX(String data_dhtmlx) {
        String data[] = data_dhtmlx.split("T");
        String hora[] = data[1].split("\\.");
        
        return convertirStringToSqlTimestamp(data[0] + " " + hora[0]);
    }
    
    public static boolean isNumeric (String cadena){
	try {
            Integer.parseInt(cadena);
            return true;
	} catch (NumberFormatException nfe){
            return false;
	}
    }
}
