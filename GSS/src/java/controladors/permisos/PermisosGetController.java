package controladors.permisos;

import controladors.base.CITServlet;
import controladors.usuaris.UsuarisGetController;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.UsuarisPrivilegis;
import models.services.UsuarisPrivilegisService;
import util.commons.Utils;
import util.commons.UtilsGrid;

public class PermisosGetController extends CITServlet {
    
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            if (inicialitzar(request, response, this)) {
                setXML();
                
                ArrayList<UsuarisPrivilegis> llista = new ArrayList<UsuarisPrivilegis>();
                
                String tipus = Utils.getParameterString(request, "tipus");
                String controlador = Utils.getParameterString(request, "controlador");
                
                if(!tipus.equals("") ||
                   !controlador.equals("")
                  ) {
                    llista = UsuarisPrivilegisService.getUsuarisPrivilegis(tipus,controlador);
                } else {
                    llista = UsuarisPrivilegisService.getUsuarisPrivilegis();                   
                }
                
                PrintWriter out = response.getWriter();
                out.print("<?xml version='1.0' encoding='UTF-8'?>");
                out.print("<rows>");
                for (UsuarisPrivilegis h : llista) {
                    out.print(UtilsGrid.toXMLPK(h.getId()+""));
                    out.print(UtilsGrid.toXML(h.getId()+""));
                    out.print(UtilsGrid.toXML(h.getTipus()+""));                 
                    out.print(UtilsGrid.toXML(h.getClasse()));                   
                    out.print(UtilsGrid.toXMLend());
                }
                out.print("</rows>");
                
                super.processRequest(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarisGetController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}