/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.dto;

import java.sql.Date;

/**
 *
 * @author alex
 */
public class HistoricDTO {
    
    int id;
    Date data;
    int idtaula;
    String nomtaula;
    int idusuari;
    String nomusuari;
    String tipus;
    String valors;
    
    public HistoricDTO(){
        
    }
    
    public HistoricDTO(int id, Date data, int idtaula, String nomtaula, int idusuari, String nomusuari, String tipus, String valors) {
        this.id = id;
        this.data = data;
        this.idtaula = idtaula;
        this.nomtaula = nomtaula;
        this.idusuari = idusuari;
        this.nomusuari = nomusuari;
        this.tipus = tipus;
        this.valors = valors;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getIdtaula() {
        return idtaula;
    }

    public void setIdtaula(int idtaula) {
        this.idtaula = idtaula;
    }

    public String getNomtaula() {
        return nomtaula;
    }

    public void setNomtaula(String nomtaula) {
        this.nomtaula = nomtaula;
    }
    
    public int getIdusuari() {
        return idusuari;
    }

    public void setIdusuari(int idusuari) {
        this.idusuari = idusuari;
    }

    public String getNomusuari() {
        return nomusuari;
    }

    public void setNomusuari(String nomusuari) {
        this.nomusuari = nomusuari;
    }

    public String getTipus() {
        return tipus;
    }

    public void setTipus(String tipus) {
        this.tipus = tipus;
    }

    public String getValors() {
        return valors;
    }

    public void setValors(String valors) {
        this.valors = valors;
    }
    
}
