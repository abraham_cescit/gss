/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.historic;

import controladors.base.CITServlet;
import controladors.usuaris.UsuarisGetController;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.dto.HistoricDTO;
import models.services.HistoricCanvisService;
import util.commons.UtilsGrid;
import util.commons.Utils;

/**
 *
 * @author Sònia
 */
public class HistoricGetController extends CITServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (inicialitzar(request, response, this)) {

                setXML();
                
                ArrayList<HistoricDTO> llista = new ArrayList<HistoricDTO>();
                
                String desde = Utils.getParameterString(request, "desde");
                String fins = Utils.getParameterString(request, "fins");
                String taula = Utils.getParameterString(request, "taula");
                String usuari = Utils.getParameterString(request, "usuari");
                String tipus = Utils.getParameterString(request, "tipus");
                
                if(!desde.equals("") ||
                   !fins.equals("") ||
                   !taula.equals("") ||
                   !usuari.equals("") ||
                   !tipus.equals("")
                  ){
                    llista = HistoricCanvisService.getHistoricCanvis(desde,fins,taula,usuari,tipus);
                } else {
                    llista = HistoricCanvisService.getHistoricCanvis();                   
                }
                
                PrintWriter out = response.getWriter();
                out.print("<?xml version='1.0' encoding='UTF-8'?>");
                out.print("<rows>");
                for (HistoricDTO h : llista) {
                    out.print(UtilsGrid.toXMLPK(h.getId()+""));
                    out.print(UtilsGrid.toXML(h.getId()+""));                   
                    out.print(UtilsGrid.toXML(h.getData()+""));                   
                    out.print(UtilsGrid.toXML(h.getIdtaula()+""));                   
                    out.print(UtilsGrid.toXML(h.getNomtaula()));
                    out.print(UtilsGrid.toXML(h.getIdusuari()+""));
                    out.print(UtilsGrid.toXML(h.getNomusuari()));                   
                    out.print(UtilsGrid.toXML(h.getTipus()));                   
                    out.print(UtilsGrid.toXML(h.getValors()));                   
                    out.print(UtilsGrid.toXMLend());
                }
                out.print("</rows>");
                
                super.processRequest(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarisGetController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
