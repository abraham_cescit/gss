package models.services;

import controladors.base.Idioma;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import persistencia.Bdades;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Usuaris;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Sha256Hash;
import util.commons.Utils;
// *****

/**
 *
 * @author fernando
 *
 * Métodos de la clase Usuari
 */
public class UsuarisService {

    private static String NOM_TAULA = "persona";

    /**
     * Deuvelve la contraseña encriptada de un usuario para compararla con la
     * que llega para iniciar sesión
     *
     * @param login nombre de usuario
     * @return Devuelve la contraseña encriptada en formato String
     */
    public static String getContrasenya(String login) {
        String contrasenya = "";
        String sql = "select contrasenya from usuari where login=? ";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, login);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            if (rs.first()) {
                contrasenya = rs.getString("contrasenya");
            }

        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return contrasenya;
    }

    /**
     * Método para devolver el salt de un usuario. Se utiliza para iniciar
     * sesión en CustomSecurityRealm
     *
     * @param login nombre de usuario
     * @return Devuelve el salt de un usuario en formato String
     */
    public static String getSalt(String login) {
        String salt = "";
        String sql = "select salt from usuari where login=? ";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, login);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            if (rs.first()) {
                salt = rs.getString("salt");
            }

        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return salt;
    }

    /**
     * Devuelve los datos necesarios para la sesión
     *
     * @param login nombre de usuario
     * @return Devuelve un Usuari con idioma, idusuari, nom y tipus
     */
    public static Usuaris getDadesSessio(String login) {
        Usuaris u = new Usuaris();
        String sql = "select idioma, idusuari, nom, tipus from usuari where login=? ";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, login);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            if (rs.first()) {
                u.setIdioma(rs.getString("idioma"));
                u.setIdUsuari(rs.getInt("idusuari"));
                u.setNom(rs.getString("nom"));
                u.setTipus(rs.getInt("tipus"));
                u.setLogin(login);
            }

        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return u;
    }

    public static ArrayList<Usuaris> getUsuaris() {
        ArrayList<Usuaris> r = new ArrayList<Usuaris>();

        String q = "select idusuari,login,nom,tipus,email,telefon,datanaixement,databaixa,idioma from usuaris ";

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                r.add(UsuariService.obtenir(
                        rs.getInt("idusuari"),
                        rs.getString("login"),
                        rs.getString("nom"),
                        rs.getInt("tipus"),
                        rs.getString("email"),
                        rs.getString("telefon"),
                        rs.getDate("datanaixement"),
                        rs.getDate("databaixa"),
                        rs.getString("idioma")));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }

    public static boolean addUsuari(Usuaris u) {
        String sql = "insert into usuari "
                + "(login,email,nom,tipus,contrasenya,idioma,salt) "
                + "values (?,?,?,?,?,?,?,?)";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        boolean r = false;
        int id = 0;

        try {
            String contrasenya = "";
            Object salt = generarSalt();
            contrasenya = encriptarContrasenya(u.getContrasenya(), salt);

            int i = 1;
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstm.setString(i++, u.login);
            pstm.setString(i++, u.getEmail());
            pstm.setString(i++, u.getNom());
            pstm.setInt(i++, u.getTipus());
            pstm.setString(i++, contrasenya);
            pstm.setString(i++, u.getIdioma());
            pstm.setString(i++, salt.toString());
            pstm.executeUpdate();
            rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }

            if (id != 0) {
                r = true;
                HistoricCanvisService.insertHistoric(id, HistoricCanvisService.TIPUS_INSERT, NOM_TAULA, u.toString());
            }

        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }

        if (r) {

        }

        return r;
    }

    public static boolean updateUsuari(Usuaris u) {
        boolean r = false;
        String sql = "update usuari set "
                + "email=?,"
                + "nom=?,"
                + "tipus=?,"
                + "idioma=? ";
        sql += "where idusuari=?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            Integer i = 1;
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(i++, u.getEmail());
            pstm.setString(i++, u.getNom());
            pstm.setInt(i++, u.getTipus());
            pstm.setString(i++, u.getIdioma());
            pstm.setInt(i++, u.getIdUsuari());
            pstm.executeUpdate();

            if (!u.getContrasenya().equals("")) {
                r = updateContrasenya(u.getContrasenya(), u.getIdUsuari());
            }
            r = true;
            if (true) {
                HistoricCanvisService.insertHistoric(u.getIdUsuari(), HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, u.toString());
            }

        } catch (SQLException ex) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    public static boolean updateContrasenya(String contrasenya, Integer idusuari) {
        boolean r = false;
        Object salt = generarSalt();
        String contrasenyaEncriptada = encriptarContrasenya(contrasenya, salt);

        String sql = "update usuaris set "
                + "contrasenya=?,"
                + "salt=? "
                + "where idusuari=?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            Integer i = 1;
            pstm.setString(i++, contrasenyaEncriptada);
            pstm.setString(i++, salt.toString());
            pstm.setInt(i++, idusuari);
            pstm.executeUpdate();
            r = true;
            if (true) {
                HistoricCanvisService.insertHistoric(idusuari, HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, "Contrasenya actualitzada");
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }

        return r;
    }

    public static boolean deleteUsuari(int idUsuari) {
        boolean r = false;

        String sql = "update usuaris set databaixa = ? where idusuari=?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setDate(1, Utils.avui());
            pstm.setInt(2, idUsuari);
            pstm.executeUpdate();
            r = true;
            HistoricCanvisService.insertHistoric(idUsuari, HistoricCanvisService.TIPUS_UPDATE, NOM_TAULA, "Usuari donat de baixa");
        } catch (SQLException ex) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, ex);
            r = false;
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    /**
     * Comprova si existeix a la base de dades un usuari amb aquest login
     *
     * @param login Login de l'usuari a comprovar.
     * @return true si existeix i false si no existeix a la base de dades.
     */
    public static boolean existeix(String login) {
        boolean r = false;
        String sql = "select login from usuari where login=?";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setString(1, login);
            rs = pstm.executeQuery();
            r = rs.next();
        } catch (SQLException ex) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return r;
    }

    public static ArrayList<Usuaris> getUsuari() {
        ArrayList<Usuaris> r = new ArrayList<Usuaris>();

        String q = "select idusuari,login,nom,tipus,email,telefon,datanaixement,databaixa,idioma from usuari ";

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();

            stm = con.createStatement();
            stm.execute(q);
            rs = stm.getResultSet();
            while (rs.next()) {
                r.add(UsuariService.obtenir(
                        rs.getInt("idusuari"),
                        rs.getString("login"),
                        rs.getString("nom"),
                        rs.getInt("tipus"),
                        rs.getString("email"),
                        rs.getString("telefon"),
                        rs.getDate("datanaixement"),
                        rs.getDate("databaixa"),
                        rs.getString("idioma")));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, stm, rs);
        }
        return r;
    }

    public static Usuaris findEmailUsuari(int idUsuari) {
        Usuaris u = new Usuaris();
        String sql = "select email from usuari where idusuari=? ";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.setInt(1, idUsuari);
            pstm.executeQuery();
            rs = pstm.getResultSet();
            if (rs.first()) {
                u.setEmail(rs.getString("email"));
            }
        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }
        return u;
    }

    public static ArrayList<Usuaris> findNomUsuari(String idioma) {
        idioma = Idioma.CAT;
        ArrayList<Usuaris> u = new ArrayList<Usuaris>();

        String sql = "select idusuari,nom,login "
                + "from usuari ";

        Connection con = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;

        try {
            con = Bdades.getDataSource().getConnection();
            pstm = con.prepareStatement(sql);
            pstm.executeQuery();
            rs = pstm.getResultSet();

            while (rs.next()) {
                Usuaris us = new Usuaris();
                us.setIdUsuari(rs.getInt("idusuari"));
                us.setNom(rs.getString("nom"));
                us.setLogin(rs.getString("login"));
                u.add(us);
            }

        } catch (SQLException sqle) {
            Logger.getLogger(UsuariService.class.getName()).log(Level.SEVERE, null, sqle);
        } finally {
            Bdades.closeConnections(con, pstm, rs);
        }

        return u;
    }

    public static Usuaris obtenir(Integer idUsuari, String login, String nom, Integer tipus, String email, String telefon, Date datanaixement, Date databaixa, String idioma) {
        return new Usuaris(idUsuari, login, nom, tipus, email, telefon, datanaixement, databaixa, idioma);
    }

    /**
     * El método genera una cadena única que se utiliza para encriptar la
     * contraseña
     *
     * @return Devuelve un String con el salt
     */
    private static Object generarSalt() {
        RandomNumberGenerator rng = new SecureRandomNumberGenerator();
        Object salt = rng.nextBytes();
        return salt;
    }

    /**
     * Encripta la contraseña en SHA256 junto al salt con 1024 vueltas
     *
     * @param contrasenya contraseña en texto plano
     * @param salt salt generado con el método generarSalt()
     * @return Devuelve la contraseña encriptada
     */
    private static String encriptarContrasenya(String contrasenya, Object salt) {
        return new Sha256Hash(contrasenya, salt, 1024).toBase64();
    }
}
