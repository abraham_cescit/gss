-- MariaDB dump 10.17  Distrib 10.4.11-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: gss
-- ------------------------------------------------------
-- Server version	10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `historiccanvis`
--

DROP TABLE IF EXISTS `historiccanvis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historiccanvis` (
  `idhistoriccanvis` int(11) NOT NULL AUTO_INCREMENT,
  `idusuari` int(11) NOT NULL,
  `data` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tipus` varchar(50) NOT NULL COMMENT 'insert, update, delete, ...',
  `nomtaula` varchar(50) NOT NULL,
  `idtaula` int(11) NOT NULL,
  `valors` text NOT NULL,
  PRIMARY KEY (`idhistoriccanvis`),
  KEY `fk_historiccanvis_usuari1_idx` (`idusuari`),
  CONSTRAINT `fk_historiccanvis_usuari1` FOREIGN KEY (`idusuari`) REFERENCES `usuaris` (`idusuari`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historiccanvis`
--

LOCK TABLES `historiccanvis` WRITE;
/*!40000 ALTER TABLE `historiccanvis` DISABLE KEYS */;
/*!40000 ALTER TABLE `historiccanvis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuaris`
--

DROP TABLE IF EXISTS `usuaris`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuaris` (
  `idusuari` int(11) NOT NULL AUTO_INCREMENT,
  `idempresa` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `tipus` int(11) NOT NULL DEFAULT 1,
  `email` varchar(100) DEFAULT NULL,
  `telefon` varchar(12) NOT NULL,
  `datanaixement` date DEFAULT NULL,
  `idioma` varchar(3) DEFAULT NULL,
  `databaixa` date DEFAULT NULL,
  `contrasenya` varchar(100) DEFAULT NULL,
  `salt` varchar(100) NOT NULL,
  PRIMARY KEY (`idusuari`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuaris`
--

LOCK TABLES `usuaris` WRITE;
/*!40000 ALTER TABLE `usuaris` DISABLE KEYS */;
INSERT INTO `usuaris` (`idusuari`, `idempresa`, `login`, `nom`, `tipus`, `email`, `telefon`, `datanaixement`, `idioma`, `databaixa`, `contrasenya`, `salt`) VALUES (1,1,'fernando','Fernando Melero',4,'fernando@cesc-it.com','123456789',NULL,'CAT',NULL,'YaOrvtWY6i0lAYyLmh1x9McB4AVONGy+Wgy+8qHAzvI=','8pNy1zk+qTc8su8cxIvXeQ==');
/*!40000 ALTER TABLE `usuaris` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarisconexions`
--

DROP TABLE IF EXISTS `usuarisconexions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarisconexions` (
  `idusuarisconexions` int(11) NOT NULL AUTO_INCREMENT,
  `data` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `login` varchar(50) NOT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `pais` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idusuarisconexions`),
  KEY `fk_usuarisconexions_1_idx` (`login`),
  CONSTRAINT `fk_usuarisconexions_1` FOREIGN KEY (`login`) REFERENCES `usuaris` (`login`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarisconexions`
--

LOCK TABLES `usuarisconexions` WRITE;
/*!40000 ALTER TABLE `usuarisconexions` DISABLE KEYS */;
INSERT INTO `usuarisconexions` (`idusuarisconexions`, `data`, `login`, `ip`, `pais`) VALUES (1,'2020-09-14 16:36:03','fernando','0:0:0:0:0:0:0:1','España'),(2,'2020-09-14 16:46:29','fernando','0:0:0:0:0:0:0:1','España'),(3,'2020-09-14 16:52:00','fernando','0:0:0:0:0:0:0:1','España'),(4,'2020-09-14 16:57:18','fernando','0:0:0:0:0:0:0:1','España'),(5,'2020-09-14 16:58:52','fernando','0:0:0:0:0:0:0:1','España');
/*!40000 ALTER TABLE `usuarisconexions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarisprivilegis`
--

DROP TABLE IF EXISTS `usuarisprivilegis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarisprivilegis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipus` int(11) NOT NULL,
  `classe` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarisprivilegis`
--

LOCK TABLES `usuarisprivilegis` WRITE;
/*!40000 ALTER TABLE `usuarisprivilegis` DISABLE KEYS */;
INSERT INTO `usuarisprivilegis` (`id`, `tipus`, `classe`) VALUES (9,4,'controladors.inici.IniciController'),(10,4,'controladors.usuaris.UsuarisController'),(11,4,'controladors.usuaris.UsuarisGetController'),(12,4,'controladors.usuaris.GetTipusUsuarisComboController'),(13,4,'controladors.usuaris.UsuariInsertController'),(14,4,'controladors.usuaris.Logout'),(15,4,'controladors.usuaris.Login'),(16,4,'idiomes.GetIdiomesComboController'),(17,4,'controladors.usuaris.UsuariDeleteController'),(18,4,'controladors.permisos.PermisosController'),(23,4,'controladors.permisos.PermisosInsertController'),(24,4,'controladors.permisos.PermisosUpdateController'),(25,4,'controladors.permisos.PermisosDeleteController'),(26,4,'controladors.permisos.PermisosGetTipusComboController'),(27,4,'controladors.permisos.PermisosGetController'),(55,4,'util.commons.ImatgesGetController'),(57,4,'controladors.registrediari.RegistreDiariController');
/*!40000 ALTER TABLE `usuarisprivilegis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'gss'
--

--
-- Dumping routines for database 'gss'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-14 19:11:37
