/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.registrediari;

import controladors.base.CITServlet;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Abraham M
 */
public class RegistreDiariController extends CITServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if(inicialitzar(request, response,this)){
                addDiccionari("registres");
                addDiccionari("usuaris");
                addDiccionari("empreses");
                addDiccionari("vehicles");
                setTitol("Registre Diari");
                setVista("/registrediari.jsp");
                super.processRequest(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RegistreDiariController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
