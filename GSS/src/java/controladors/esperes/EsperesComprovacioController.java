/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.esperes;

import com.google.gson.Gson;
import controladors.base.CITServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Espera;
import models.services.APIService;
import util.commons.UtilsGrid;

/**
 *
 * @author Abraham M
 */
public class EsperesComprovacioController extends CITServlet {

    public static final String DESAT_EXIT = "DESAT_EXIT";
    public static final String TOKEN = "token";
    public static final String MATRICULA = "matr";
    public static final String LINEA = "lane";
    public static final String FOTO = "picture";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Gson gson = new Gson();
            if (inicialitzar(request, response, this, false)) {
                request.setCharacterEncoding("UTF-8");
                
                addDiccionari("vehicles");
                addDiccionari("registres");
                Integer numEsperes = null;
                ArrayList<Espera> esperes = APIService.getEntrades();
                PrintWriter out = response.getWriter();
                if ( esperes.size() > 0){
                    response.setContentType("text/xml;charset=UTF-8");
                    response.setContentType("application/xml");
                    setNua();
                    out.print("<?xml version='1.0' encoding='UTF-8'?>");
                    out.print("<rows>");
                    for (Espera e : esperes) {
                        out.print(UtilsGrid.toXMLPK(e.getidEspera() + ""));
                        out.print(UtilsGrid.toXML(e.getidEspera() + ""));
                        out.print(UtilsGrid.toXML(e.getLinea() + ""));
                        out.print(UtilsGrid.toXML(e.getMatricula() + ""));
                        out.print(UtilsGrid.toXML(e.getArribadaString() + ""));
                        out.print(UtilsGrid.toXML(e.getRegistrat() + ""));
                        out.print(UtilsGrid.toXML(e.getFoto() + ""));
                        out.print(UtilsGrid.toXMLend());
                    }
                    out.print("</rows>");

                    // super.processRequest(request, response);
//                        String JSON = gson.toJson(esperes);
//                        out.print(JSON);
                } else {
                    out.print("no");
//                    afegirRespostaBreu(RESULTAT, "no");
//                    afegirRespostaBreu(MISSATGE, getTexte("VEHICLE_ERROR_TOKEN"));
//                    afegirRespostaBreu(TEMPS, "0");
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(EsperesComprovacioController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(EsperesComprovacioController.class.getName()).log(Level.SEVERE, null, ex);
        }

        super.processRequest(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
