<%@page import="util.commons.Utils"%>
     
<style>
    
    html, body {
                width: 100% !important;
                height: 100% !important;
                margin: 0px !important;
                overflow: hidden !important;
                background-color:white ;
                font-family: "Gill Sans Extrabold", Helvetica, sans-serif
            }
            /*Layouts*/

.dhxlayout_base_material div.dhx_cell_layout div.dhx_cell_hdr{
    background-color: <%=Utils.getProperty("COLOR_PRIMARI")%> !important;
}
.dhxacc_base_material div.dhx_cell_acc div.dhx_cell_hdr {
    color:<%=Utils.getProperty("COLOR_PRIMARI")%> !important;
}

.dhtmlx-error, .dhtmlx-error div{
    background-color: #e74c3c !important ;
    font-weight: bold;
    color:white !important;
}

.dhxacc_base_material div.dhx_cell_acc.dhx_cell_closed div.dhx_cell_hdr {
    color: <%=Utils.getProperty("COLOR_PRIMARI")%> !important;
}


.dhxtabbar_base_material div.dhxtabbar_tabs {
    line-height: 42px;
    white-space: nowrap;
    font-size: 14px;
    font-family: Roboto,Arial,Helvetica;
    color: #FBFBEF;

}
.dhxtabbar_base_material div.dhxtabbar_tabs div.dhxtabbar_tabs_base div.dhxtabbar_tab.dhxtabbar_tab_actv{
    background-color:<%=Utils.getProperty("COLOR_PRIMARI")%> !important;
}
.dhxtabbar_base_material div.dhxtabbar_tabs div.dhxtabbar_tabs_base div.dhxtabbar_tab div.dhxtabbar_tab_text{
    color: <%=Utils.getProperty("COLOR_PRIMARI")%> !important;
}
.dhxtabbar_base_material div.dhxtabbar_tabs div.dhxtabbar_tabs_base div.dhxtabbar_tabs_cont_left div.dhxtabbar_tab{
}
.dhxlayout_base_material div.dhx_cell_layout div.dhx_cell_hdr{background-color:<%=Utils.getProperty("COLOR_PRIMARI")%>;color:#FBFBEF;border:1px solid #f8e0a4;}
.dhxlayout_base_material div.dhxlayout_resize_sep{background-color:<%=Utils.getProperty("COLOR_PRIMARI")%>;color:black;}
.dhtmlxMenu_material_Middle img.dhtmlxMenu_TopLevel_Item_Icon {
    float: left;
    margin: 1px 0px 0 3px;
    width: 40px;
    height: 26px;
    cursor: default;
}

.dhtmlx-dialogoGSS{
    font-weight:bold !important;
    color: #fff !important;
    background-color: white!important;
}

.dhtmlx_popup_button{
    color: #fff;
    text-align: center;
    background-color: <%=Utils.getProperty("COLOR_BOTO")%> !important;
    border-radius: 12px;
    width: 150px;
    font-size: 14px;
}

.dhtmlx_popup_title{
    background-color: <%=Utils.getProperty("COLOR_PRIMARI")%> !important;
}

a {
    color: <%=Utils.getProperty("COLOR_PRIMARI")%> !important;
}
a:hover {
    color: <%=Utils.getProperty("COLOR_PRIMARI")%> !important;
    background-color: #f8e0a4;
}
/*Boton acceder de loggin*/
.btn-primary:hover {
    color: #fff;
    background-color: <%=Utils.getProperty("COLOR_BOTO")%> !important;
    border-color: #f8e0a4;
}
.btn-primary:focus {
    color: #fff;
    background-color: <%=Utils.getProperty("COLOR_BOTO")%> !important;
    border-color: #f8e0a4;
}
.btn {
    color: #fff;
    background-color: <%=Utils.getProperty("COLOR_BOTO")%> !important;
    border-color: #f8e0a4;
}
.btn btn-lg btn-primary btn-block{
    background-color: <%=Utils.getProperty("COLOR_BOTO")%> !important;
}
.btn-primary{
    background-color: <%=Utils.getProperty("COLOR_BOTO")%> !important;
}
.btn btn-lg btn-primary btn-block:focus{
    background-color: <%=Utils.getProperty("COLOR_BOTO")%> !important;
}

/*Inputs seleccionados*/
.dhxform_obj_material input.dhxform_textarea:focus, .dhxform_obj_material textarea.dhxform_textarea:focus {
    border-bottom-color: #f8e0a4;
    
}

.dhxform_cameras {
    background-color: <%=Utils.getProperty("FONS_DIV_LOGIN")%> !important;
}

.dhxform_obj_material div.dhxform_btn:focus {
    // border-color:#f8e0a4;
    border-color: <%=Utils.getProperty("COLOR_BOTO")%> !important;
    
}

/*div.dhxform_btn {
    color: #fff;
    background-color: <%=Utils.getProperty("COLOR_BOTO")%> !important;
}*/

.dhxform_btn {
    color: #fff;
    text-align: center;
    background-color: <%=Utils.getProperty("COLOR_BOTO")%> !important;
    border-radius: 12px;
    width: 150px;
    font-size: 14px;
}

.dhxform_textarea:focus {
    border-bottom:1px solid #f8e0a4;
}
.btn btn-lg btn-primary btn-block{
    background-color: <%=Utils.getProperty("COLOR_PRIMARI")%> !important;
}
/*borde ventana winds */
.dhxwins_vp_material div.dhxwin_hdr{
    background-color:<%=Utils.getProperty("COLOR_PRIMARI")%> !important;
    border:1px solid #f8e0a4;
}

.dhxwins_vp_material div.dhxwin_brd{
    border-bottom:1px solid #f8e0a4;
    border-left:1px solid #f8e0a4;
    border-right: 1px solid #f8e0a4;
}

.form-control:focus{
    border:1px solid #f8e0a4;
}

.dhxform_control:focus{
    border-width: 0 0 1px 0;
    border-style: solid;
    border-color: red;
}

div.dhxcombo_material.dhxcombo_actv {
    border-bottom: 2px solid #f8e0a4 !important;
}

div.gridbox_material.gridbox table.obj tr.rowselected {
    background-color: <%=Utils.getProperty("COLOR_SELECCIO_LINIA_GRID")%> !important;
}

table.obj tr.rowselected td {
    background-color: <%=Utils.getProperty("COLOR_SELECCIO_LINIA_GRID")%> !important;
}

.loading_class {
    width: 200px;
}



/*====== ICONS ====*/
i {
    color: <%=Utils.getProperty("COLOR_ICONOS")%> !important;
    padding: 0 5px;
}
i.fa-file-excel{
    color: <%=Utils.getProperty("COLOR_ICONO_EXCEL")%> !important;
}



/*ALERTAS*/
.dhtmlx-error, .dhtmlx-error div{
    background-color:#f2dede !important;
    border-color: #ebccd1 !important;
    color:#b94a48 !important;
}
.dhtmlx-ok, .dhtmlx-ok div{
    background-color:#dff0d8 !important;
    border-color: #d6e9c6 !important;
    color:#468847 !important;
}
.dhtmlx-informacio, .dhtmlx-informacio div{
    background-color:#d9edf7 !important;
    border-color: #bce8f1 !important;
    color:#3a87ad !important;
}
.white{
    color: white !important;
}



.hover {
    background-color: #eee;
}


.formBig input{
    font-size:24px !important;
}

 .formBigGrey input{
    font-size:24px !important;
    color:  #a6a6a6  !important;
}

.formSuperBig label{
    font-size: 2.5vh !important;
}

.formSuperBig input{
    font-size: 2.5vh !important;
}

.formUltraBig label{
    font-size: 2.5vh !important;
}

.formUltraBig input{
    margin: 100px;
    font-size: 5vh !important;
}

.formSmall label{
    font-size: 1.5vh !important;
}

.formSmall input{
    margin: 100px;
    font-size: 1.5vh !important;
}

/*====== MENU ====*/



div.dhtmlxMenu_material_Middle {
    height: 40px !important;
    background-color: <%=Utils.getProperty("COLOR_MENU_FONS")%> !important;
    color: white
}
.dhtmlxMenu_material_Middle div.dhtmlxMenu_material_TopLevel_Item_Normal, .dhtmlxMenu_material_Middle div.dhtmlxMenu_material_TopLevel_Item_Disabled, .dhtmlxMenu_material_Middle div.dhtmlxMenu_material_TopLevel_Item_Selected {
    height: 40px !important;
}

.dhtmlxMenu_material_TopLevel_Item_Normal {
    padding: 5px 10px !important;
}

.dhtmlxMenu_material_TopLevel_Item_Selected {
    background-color: <%=Utils.getProperty("COLOR_MENU_HOVER")%> !important;
    color : white !important;
    padding: 5px 10px !important;
    font-weight: bold !important;
}

.dhtmlxMenu_material_Middle img.dhtmlxMenu_TopLevel_Item_Icon {
    width: 100% !important;
}

.dhtmlxMenu_material_Middle div.dhtmlxMenu_material_TopLevel_Item_Normal div.top_level_text, .dhtmlxMenu_material_Middle div.dhtmlxMenu_material_TopLevel_Item_Disabled div.top_level_text, .dhtmlxMenu_material_Middle div.dhtmlxMenu_material_TopLevel_Item_Selected div.top_level_text{
    height: 50px;
}





div.gridbox_material.gridbox table.obj tr.rowselected td:first-child {
    border-left: 2px solid <%=Utils.getProperty("COLOR_FORMULARIO_UNDERLINE")%> !important;
}
.dhxform_obj_material input.dhxform_textarea:focus, .dhxform_obj_material textarea.dhxform_textarea:focus {
    border-bottom-color: <%=Utils.getProperty("COLOR_FORMULARIO_UNDERLINE")%> !important;
}

/*.dhxform_obj_material {
    background-color: <%=Utils.getProperty("FONS_DIV_LOGIN")%> !important;
}*/

.dhxwins_vp_material div.dhxwin_brd {
    border-color: white !important;
}

.dhxwins_vp_material div.dhxwin_active {
    background-color: white !important;
}

.group_row {
    color: <%=Utils.getProperty("COLOR_FORMULARIO_UNDERLINE")%> !important;
}

</style>


