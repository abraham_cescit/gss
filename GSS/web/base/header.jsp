<%@page import="util.commons.Utils"%>
<%@page import="com.cescit.security.ControlAcces"%>
<%@page import="controladors.base.Idioma"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% Idioma idioma = (Idioma) request.getAttribute("idioma");%>
<div id="logoObj" style="background-color: #414140">
    <img height="100px" src="./icons/logo_menu.png"  />
</div>
<hr style="background-color: #ac9867; padding: 0px; margin: 0px; border: 0px;" size = "10"  />

<div id="menuObj"></div>
<script>
    
    document.addEventListener("DOMContentLoaded", onldMenu);
    function onldMenu() {
        myMenu = new dhtmlXMenuObject({parent: "menuObj"});
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            var contingut = [
                {id: "altres", img: "fas fa-bars white",
                    items: [
                        {id: "usuaris", text: "USUARIS"},
                        {id: "empreses", text: "EMPRESES"},
                        {id: "vehicles", text: "VEHICLES"},
                        {id: "consultes", text: "CONSULTES"},
                        {id: "cameres", text: "CAMERES"},
                        {id: "diari", text: "DIARI"},
                        {id: "sortir", text: ""}
                    ]
                }
            ];
        } else {
            var contingut = [
                {id: "usuaris", text: "USUARIS"},
                {id: "empreses", text: "EMPRESES"},
                {id: "vehicles", text: "VEHICLES"},
                {id: "consultes", text: "CONSULTES"},
                {id: "cameres", text: "CAMERES"},
                {id: "diari", text: "DIARI"},
                {id: "sortir", text: "", img: "fas fa-sign-out-alt white"}
            ];
        }
        myMenu.setIconset("awesome");
        myMenu.loadStruct(contingut);
        myMenu.setHref("usuaris", "usuaris");
        myMenu.setHref("empreses", "empreses");
        myMenu.setHref("vehicles", "vehicles");
        myMenu.setHref("consultes", "consultes");
        myMenu.setHref("cameres", "cameres");
        myMenu.setHref("diari", "registrediari");
        myMenu.setHref("sortir", "logout");
        
    }

</script>

