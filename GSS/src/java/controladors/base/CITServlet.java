/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladors.base;

import com.cescit.security.ControlAcces;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import util.commons.Utils;

/**
 *
 * @author jose
 */
public class CITServlet extends HttpServlet {

    public static final int TIPUS_NORMAL = 0;
    public static final int TIPUS_NUA = 1;
    public static final int TIPUS_BREU_XML = 2;
    public static final int TIPUS_XML = 3;

    //Constats per enviar resltats a les peticions Ajax. Tenen la seua contrapartida al Javascript
    public static final String RESULTAT = "RESULTAT";
    public static final String MISSATGE = "MISSATGE";
    public static final String PARAMETRE = "PARAMETRE";
    public static final String TEMPS = "TEMPS";
    public static final String CREACIO_FACTURA_VISUAL = "CREACIO_FACTURA_VISUAL";

    public static final String OK = "OK";
    public static final String ERROR = "ERROR";
    public static final String INDEFINIDA = "INDEFINIDA";

    private ThreadLocal<HttpServletRequest> req = new ThreadLocal<HttpServletRequest>();
    private ThreadLocal<ArrayList<String>> missatges = new ThreadLocal<ArrayList<String>>();
    private ThreadLocal<Integer> tipus = new ThreadLocal<Integer>();
    private ThreadLocal<String> language = new ThreadLocal<String>();
    private ThreadLocal<ArrayList<String>> jss = new ThreadLocal<ArrayList<String>>();
    private ThreadLocal<ArrayList<String>> csss = new ThreadLocal<ArrayList<String>>();
    private ThreadLocal<ArrayList<ValorRespostaBreu>> respostesBreusXML = new ThreadLocal<ArrayList<ValorRespostaBreu>>();
    private ThreadLocal<Idioma> idioma = new ThreadLocal<Idioma>();

    /**
     * Procesa Accions d'inicialitzacio per la pagina. Incloent comprobacio de
     * que l;usuari s'hagi autenticat i tinga privbilegis d'acces a l
     * controlador
     *
     * @param request Objecte Request d-aquesta peticio
     * @param response Objevte Response d-aquesta peticio
     * @param c Objecte controlador que maneja la peticio
     * @param autenticacioRequerida true si es requereix autenticacio i
     * comprovacio de permisos per aquestas pagina y false si no es aixi
     * @return true si s'autoritza la peticio i false si no.
     * @throws SQLException excepcio
     */
    protected boolean inicialitzar(HttpServletRequest request, HttpServletResponse response, Object c, boolean autenticacioRequerida) throws SQLException {
        //autenticacioRequerida=false;

        boolean r = true;
        try {
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(CITServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Per a qeust thread esborrar els valor que ja hi havíen tal volta per la petició anterior.
        if (tipus.get() == null) {
            tipus.set(TIPUS_NORMAL);
        }
        missatges.set(null);
        jss.set(null);
        csss.set(null);
        respostesBreusXML.set(null);
        idioma.set(null);
        language.set(null);
        req.set(request);

        String lang;
        Subject currentUser = SecurityUtils.getSubject();
        Session session = currentUser.getSession();
        lang = (String) session.getAttribute("lang");
        if (lang == null) {
            lang = request.getParameter("lang");
            if (lang == null) {
                lang = "CAT";
            }
        }
        setIdioma(lang);

        request.setAttribute("cap", "header.jsp");
        request.setAttribute("peu", "footer.jsp");
        request.setAttribute("base", "base/base.jsp");

        if (autenticacioRequerida && !(ControlAcces.estaAutenticat() && ControlAcces.tePrivilegi(c.getClass()))) {
            r = false;
        }
        return r;

    }

    /**
     * Procesa Accions d'inicialitzacio per la pagina. Incloent comprobacio de
     * que l;usuari s'hagi autenticat i tinga privilegis d'acces a l
     * controlador. Es considera que es requereix autenticacio i comprovacio de
     * permisos
     *
     * @param request Objecte Request d'aquesta peticio
     * @param response Objevte Response d-aquesta peticio
     * @param c Objecte controlador que maneja la peticio
     * @return true si s'autoritza la peticio i false si no.
     * @throws SQLException excepcio
     */
    protected boolean inicialitzar(HttpServletRequest request, HttpServletResponse response, Object c) throws SQLException {
        return inicialitzar(request, response, c, true);
    }

    /**
     * Establir l'objecte requeste de la peticio actual
     *
     * @param request Objecte request de la petició actual
     */
    protected void setRequest(HttpServletRequest request) {
        req.set(request);
    }

    /**
     * Estableix la vista a utilitzar per la peticio actual.
     *
     * @param request Requeste de la peticio
     * @param vista Nom de l'arxiu vista a utilitzar
     */
    protected void setVista(HttpServletRequest request, String vista) {
        request.setAttribute("vista", vista);
    }

    /**
     * Estableix la vista a utilitzar per la peticio actual.
     *
     * @param vista Nom de l'arxiu vista a utilitzar
     */
    protected void setVista(String vista) {
        setVista(req.get(), vista);
    }

    /**
     * Estableix l'arxui a utilitzar com a cap de pagina
     *
     * @param request Requeste de la peticio
     * @param cap Nom de l'arxiu del cap de la pagina.
     */
    public void setCap(HttpServletRequest request, String cap) {
        request.setAttribute("cap", cap);
    }

    /**
     * Estableix l'arxui a utilitzar com a cap de pagina
     *
     * @param cap Nom de l'arxiu del cap de la pagina.
     */
    public void setCap(String cap) {
        setCap(req.get(), cap);
    }

    public void setPeu(HttpServletRequest request, String peu) {
        request.setAttribute("peu", peu);
    }

    /**
     * Estableix el peu de pagina a utilitzar
     *
     * @param peu Nom de l'arxiu del peu de la pagina.
     */
    public void setPeu(String peu) {
        setCap(req.get(), peu);
    }

    /**
     * Estableix el tipus de pagina que s'esta servint. Els tipus posibles son:
     * TIPUS_NORMAL es una pagina normal HTML amb cap, cos principal i peu i
     * s'utilitza la pagina jsp de base per carregart-los, TIPUS_NUA no utilitza
     * la pagina jsp de base ni cap de pagina ni peu, sino sols la vista que
     * s'especifica, TIPUS_BREU_XML Es una resposta breu en format XML
     * especialment feta per respondre a una peticio AJAX del DHTMLX
     *
     * @param tipus Tipus de la pagina
     */
    protected void setTipus(int tipus) {
        this.tipus.set(tipus);
        if (tipus == TIPUS_BREU_XML) {
            if (respostesBreusXML.get() == null) {
                respostesBreusXML.set(new ArrayList<ValorRespostaBreu>());
            }
        }
    }

    /**
     * Alies de setTipus(TIPUS_NUA)
     *
     */
    public void setNua() {
        tipus.set(TIPUS_NUA);
    }

    public void setXML() {
        setTipus(TIPUS_XML);
    }

    public void setBreuXML() {
        setTipus(TIPUS_BREU_XML);
    }

    /**
     * Estableix l'idioma de la resposta
     *
     * @param lang Idioma de la resposta. CAT, ES, EN, etc
     */
    protected void setIdioma(String lang) {
        lang = lang.toUpperCase();
        Idioma i = new Idioma(lang);
        idioma.set(i);
        language.set(lang);
        Subject currentUser = SecurityUtils.getSubject();
        Session session = currentUser.getSession();
        session.setAttribute("lang", lang);
    }

    public Idioma getIdioma() {
        return idioma.get();
    }

    /**
     * Afegeix un arxiu de diccionari de idioma. El format de l'arxiu es un
     * arxiu de propietats de Java en XML o en format .properties. L'arxiu ha
     * d'estar situat dintre de una carpeta anomenada amb l'identificador de
     * l'idioma dintre del paquet idioma. Si l'arxiu es diu factures.xml i
     * l'idioma establert es CAT es trobara a la ruta idioma.CAT.factures.xml
     *
     * @param arxiu Nom de l'arxiu sense la ruta
     */
    protected void addDiccionari(String arxiu) {
        idioma.get().afegir(arxiu);
    }

    /**
     * Obte un texte dels diccionaris d'idiomes carregats.
     *
     * @param clau Clau a buscar
     * @return Texte corresponent a la clau
     */
    protected String getTexte(String clau) {
        return idioma.get().get(clau);
    }

    /**
     * Estableix el titol de la pagina.
     *
     * @param t Titol
     */
    public void setTitol(String t) {
        req.get().setAttribute("titol", t);
    }

    /**
     * Afegeix un missatge a mostrar a l'usuari quant es carregue la pagina.
     *
     * @param missatge MIssatge a mostrar
     */
    protected void setMissatge(String missatge) {
        ArrayList<String> missatges = this.missatges.get();
        if (missatges == null) {
            missatges = new ArrayList<String>();
            this.missatges.set(missatges);
        }
        missatges.add(missatge);
    }

    /**
     * Afegeix una llibreria JavaScript que ha de carregar el navegador. Ha
     * d'esser la ruta completa
     *
     * @param js Llibreria JavaScript a carregar (ruta completa)
     */
    protected void addJS(String js) {
        ArrayList<String> llistaJs = jss.get();
        if (llistaJs == null) {
            llistaJs = new ArrayList<String>();
            jss.set(llistaJs);
        }
        llistaJs.add(js);
    }

    /**
     * Afegeix un arxiu CSS a carregar pel navegador.
     *
     * @param css Arxiu CSS (ruta completa)
     */
    protected void addCSS(String css) {
        ArrayList<String> llistaCss = csss.get();
        if (llistaCss == null) {
            llistaCss = new ArrayList<String>();
            csss.set(llistaCss);
        }
        llistaCss.add(css);
    }

    /**
     * Afegeix un parell nom-valor per la resposta XML que s'enviara al
     * navegador a resultat d'una peticio AJAX per ser processada pel DHTMLX
     *
     * @param nom Nom
     * @param valor Valor corresponent al nom
     */
    protected void afegirRespostaBreu(String nom, String valor) {
        ValorRespostaBreu v = new ValorRespostaBreu(nom, valor);
        if (respostesBreusXML.get() == null) {
            respostesBreusXML.set(new ArrayList<ValorRespostaBreu>());
        }
        respostesBreusXML.get().add(v);
    }

    /**
     * Afegeix un parell nom-valor per la resposta XML que s'enviara al
     * navegador a resultat d'una peticio AJAX per ser processada pel DHTMLX
     *
     * @param nom Nom
     * @return valor Valor corresponent al nom
     */
    protected String agafarRespostaBreu(String nom) {
        for (ValorRespostaBreu p : respostesBreusXML.get()) {
            if (p.nom.equals("SecondParty")) {
                return p.valor;
            }
        }
        return "";
    }

    /**
     * Carrega un arxiu de recurs en format de texte (qualsevol format de texte,
     * HTML, XML, Javascript, etc) i subtitueix els comentaris que contenen
     * claus d'idioma pel seu texte en l'idioma actual de la peticio.
     *
     * @param arxiu Arxiu a carregar especificant el paquet s;hi troba.
     * @return Texte de l'arxiu carregat amb els claus substituides per les
     * lligendes de l'idioma.
     */
    protected String carregarRecurs(String arxiu) {
        String r = "";
        try {
            InputStream fis = Thread.currentThread().getContextClassLoader().getResourceAsStream(arxiu);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            String linea = br.readLine();
            while (linea != null) {
                r = r + linea;
                linea = br.readLine();
            }
            isr.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        Idioma idi = idioma.get();
        r = idi.substituir(r);

        return r;
    }

    /**
     * Processa peticions per ambdosHTTP <code>GET</code> y  <code>POST</code>
     * També tanca la conexió a la base de dades 8si es qué s'havía obert. Per
     * tant si s'utilitza una conexió a la base de dades des de les vistes será
     * la vista la responsable de tancarla, malgrat que açò es desaconsella
     * força.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws java.io.IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String lang = language.get();
        if (lang == null) {
            lang = request.getParameter("lang");
            if (lang == null) {
                lang = "CAT";
            }
            setIdioma(lang);
        }
        request.setAttribute("idioma", idioma.get());

        RequestDispatcher rd;

        int t = tipus.get();

        if (t == TIPUS_NUA) {
            if (request.getAttribute("vista") != null) {
                rd = request.getRequestDispatcher((String) request.getAttribute("vista"));
                //  Bdades.close();
                rd.forward(request, response);
            }
        } else if (t == TIPUS_NORMAL) {
            response.setContentType("text/html;charset=UTF-8");
            if (request.getAttribute("vista") != null) {
                request.setAttribute("missatges", missatges.get());
                request.setAttribute("jss", jss.get());
                request.setAttribute("csss", csss.get());
                rd = request.getRequestDispatcher((String) request.getAttribute("base"));
                //  Bdades.close();
                rd.forward(request, response);
            }
        } else if (t == TIPUS_BREU_XML) {
            response.setContentType("text/xml;charset=UTF-8");
            response.setContentType("application/xml");
            PrintWriter out = response.getWriter();
            try {
                out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                out.println("<items>");
                for (ValorRespostaBreu i : respostesBreusXML.get()) {
                    out.println("<item name='" + i.nom + "' value='" + Utils.escapeXML(i.valor) + "'/>");
                }
                out.println("</items>");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                out.close();
            }
        } else if (t == TIPUS_XML) {
            response.setContentType("text/xml;charset=UTF-8");
            response.setContentType("application/xml");
        }

    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws java.io.IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws java.io.IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
