// Retorna el valor associat a un element XML de la resposta amb aquest nom
function getAjax(r, nom) {
    var xml = r.xmlDoc.responseXML;
    var retorno = "ERROR";
    //alert(new XMLSerializer().serializeToString(xml));

    var nodes = xml.getElementsByTagName("item");
    for (i = 0; i < nodes.length; i++) {
        if (nodes[i].getAttribute("name") == nom)
            retorno = nodes[i].getAttribute("value");
    }

    return retorno;
}

// Retorna el percentatge demanat de la mida de la pantalla en px
function getWidthWindow(percent) {
    return window.innerWidth * percent / 100;
}

// Retorna el percentatge demanat de la mida de la pantalla en px
function getHeightWindow(percent) {
    return window.innerHeight * percent / 100;
}

// Retorna el percentatge demanat de la mida del layout en px
function getPercentLayout(layout, id, percent) {
    return layout.cells(id).getWidth() * percent / 100;
}

//Cuando haya un Layout general (que ocupe toda la pantalla), mejor usar este método
function progressOn(layout) {
    layout.progressOn();
}
function progressOff(layout) {
    layout.progressOff();
}

//Cerrar ventana con ESC
$(document).on('keydown', function (e) {
    var target = $(event.target);
    if (event.which == 27) {
        var topwin = dhxWins.getTopmostWindow(false);
        if (topwin == null){
            topwin = dhxWinsLiniesAlbarans.getTopmostWindow(false);
        }
        if (topwin != null)
        {
            topwin.setModal(false);
            topwin.hide();
        }
    }
});


function inicializarWindowWithClose(window) {
    window.button("minmax").hide();
    window.button("park").hide();
    window.center();
    window.setModal(true);
}

function inicializarWindowNoModalNoCenterWithClose(window) {
    window.button("minmax").hide();
    window.button("park").hide();
}

function isDateBeforeToday(date) {

    var mydate = new Date(date);
    var now = new Date();
    mydate.setHours(0, 0, 0, 0);
    now.setHours(0, 0, 0, 0);
    if (mydate < now) {
        return true;
    } else {
        return false;
    }
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function today() {
    var today = new Date();
    var fecha = today.getFullYear() + "-" + checkTime(today.getMonth() + 1) + "-" + checkTime(today.getDate());
    return fecha;
}

function dataAvui() {
    var today = new Date();
    var fecha = checkTime(today.getDate()) + "/" + checkTime(today.getMonth() + 1) + "/" + today.getFullYear();
    return fecha;
}

function thisYear() {
    var today = new Date();
    var fecha = today.getFullYear();
    return fecha;
}

function thisMonth() {
    var today = new Date();
    var fecha = checkTime(today.getMonth() + 1);
    return fecha;
}

function monthAgo() {
    var today = new Date();
    var fecha = today.getFullYear() + "-" + checkTime(today.getMonth()) + "-" + checkTime(today.getDate());
    return fecha;
}


function validateComma(form, id) {
    form.setValidation(id, function (v) {
        if (v.indexOf(',') != -1) {
            form.setNote(id, {text: "La separació de decimals s'ha de fer amb el punt"});
        } else {
            form.clearNote(id);
        }
        return v.indexOf(',') == -1;
    });
}


function opcionFilterCombo(form, combo, filtrarBetween) {
    if (filtrarBetween == "true" || filtrarBetween == "TRUE") {
        var dhxCombo = form.getCombo(combo);
        dhxCombo.enableFilteringMode("between");
    }
}



function getSelectedValueGrid(grid, name) {
    var colName = grid.getColIndexById(name);
    var idSelected = grid.getSelectedRowId();
    if (idSelected == null)
        return null
    else
        return grid.cells(idSelected, colName).getValue();
}


// función que muestra la hora en forma de cronómetro 
function reloj() {
    var today = new Date();
    var newDate = new Date(today.getTime() + today.getTimezoneOffset() * 60 * 1000);
    var hours = today.getHours() - 1;
    newDate.setHours(hours + 2);
    var hora = newDate.getUTCHours() + ":" + checkTime(newDate.getMinutes()) + ":" + checkTime(newDate.getSeconds());
    return hora;
}


function inicialitzarGrid(grid) {
    if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
// Afegir el menu nomes amb les files que es veuen
        var colsMenu = "";
        for (var i = 0; i < grid.getColumnsNum(); i++) {
            if (grid.getColWidth(i) == "1") {
                colsMenu += "false,";
            } else {
                colsMenu += "true,";
            }
        }
        grid.enableHeaderMenu(colsMenu);
        // Treure el ordenar de la segona fila del header on hi ha els combos
        grid.attachEvent("onHeaderClick", function (col, e) {
            const target = e.target;
            const row = this.getFirstParentOfType(target, "TR");
            if (row && row.rowIndex > 1)
                return false;
            return true;
        });
        grid.enableAutoWidth(true);
        grid.enableRowsHover(true, "hover");
    }
}

function validarCombo(form, arrayCombos) {
    var valid = "";
    for (var i = 0; i < arrayCombos.length; i++) {
        combo = form.getCombo(arrayCombos[i]);
        var selected = combo.getSelectedIndex();
        var text = combo.getComboText();
        if (selected == -1 && text != "") {
            valid = form.getItemLabel(arrayCombos[i]);
            ;
            form.setItemFocus(arrayCombos[i]);
        }
        ;
    }
    return valid;
}

                
var conectado = false;              
function conexionImpresora() {
        qz.security.setCertificatePromise(function(resolve, reject) {
                resolve("-----BEGIN CERTIFICATE-----\n" +
                        "MIIFAzCCAuugAwIBAgICEAIwDQYJKoZIhvcNAQEFBQAwgZgxCzAJBgNVBAYTAlVT\n" +
                        "MQswCQYDVQQIDAJOWTEbMBkGA1UECgwSUVogSW5kdXN0cmllcywgTExDMRswGQYD\n" +
                        "VQQLDBJRWiBJbmR1c3RyaWVzLCBMTEMxGTAXBgNVBAMMEHF6aW5kdXN0cmllcy5j\n" +
                        "b20xJzAlBgkqhkiG9w0BCQEWGHN1cHBvcnRAcXppbmR1c3RyaWVzLmNvbTAeFw0x\n" +
                        "NTAzMTkwMjM4NDVaFw0yNTAzMTkwMjM4NDVaMHMxCzAJBgNVBAYTAkFBMRMwEQYD\n" +
                        "VQQIDApTb21lIFN0YXRlMQ0wCwYDVQQKDAREZW1vMQ0wCwYDVQQLDAREZW1vMRIw\n" +
                        "EAYDVQQDDAlsb2NhbGhvc3QxHTAbBgkqhkiG9w0BCQEWDnJvb3RAbG9jYWxob3N0\n" +
                        "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtFzbBDRTDHHmlSVQLqjY\n" +
                        "aoGax7ql3XgRGdhZlNEJPZDs5482ty34J4sI2ZK2yC8YkZ/x+WCSveUgDQIVJ8oK\n" +
                        "D4jtAPxqHnfSr9RAbvB1GQoiYLxhfxEp/+zfB9dBKDTRZR2nJm/mMsavY2DnSzLp\n" +
                        "t7PJOjt3BdtISRtGMRsWmRHRfy882msBxsYug22odnT1OdaJQ54bWJT5iJnceBV2\n" +
                        "1oOqWSg5hU1MupZRxxHbzI61EpTLlxXJQ7YNSwwiDzjaxGrufxc4eZnzGQ1A8h1u\n" +
                        "jTaG84S1MWvG7BfcPLW+sya+PkrQWMOCIgXrQnAsUgqQrgxQ8Ocq3G4X9UvBy5VR\n" +
                        "CwIDAQABo3sweTAJBgNVHRMEAjAAMCwGCWCGSAGG+EIBDQQfFh1PcGVuU1NMIEdl\n" +
                        "bmVyYXRlZCBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQUpG420UhvfwAFMr+8vf3pJunQ\n" +
                        "gH4wHwYDVR0jBBgwFoAUkKZQt4TUuepf8gWEE3hF6Kl1VFwwDQYJKoZIhvcNAQEF\n" +
                        "BQADggIBAFXr6G1g7yYVHg6uGfh1nK2jhpKBAOA+OtZQLNHYlBgoAuRRNWdE9/v4\n" +
                        "J/3Jeid2DAyihm2j92qsQJXkyxBgdTLG+ncILlRElXvG7IrOh3tq/TttdzLcMjaR\n" +
                        "8w/AkVDLNL0z35shNXih2F9JlbNRGqbVhC7qZl+V1BITfx6mGc4ayke7C9Hm57X0\n" +
                        "ak/NerAC/QXNs/bF17b+zsUt2ja5NVS8dDSC4JAkM1dD64Y26leYbPybB+FgOxFu\n" +
                        "wou9gFxzwbdGLCGboi0lNLjEysHJBi90KjPUETbzMmoilHNJXw7egIo8yS5eq8RH\n" +
                        "i2lS0GsQjYFMvplNVMATDXUPm9MKpCbZ7IlJ5eekhWqvErddcHbzCuUBkDZ7wX/j\n" +
                        "unk/3DyXdTsSGuZk3/fLEsc4/YTujpAjVXiA1LCooQJ7SmNOpUa66TPz9O7Ufkng\n" +
                        "+CoTSACmnlHdP7U9WLr5TYnmL9eoHwtb0hwENe1oFC5zClJoSX/7DRexSJfB7YBf\n" +
                        "vn6JA2xy4C6PqximyCPisErNp85GUcZfo33Np1aywFv9H+a83rSUcV6kpE/jAZio\n" +
                        "5qLpgIOisArj1HTM6goDWzKhLiR/AeG3IJvgbpr9Gr7uZmfFyQzUjvkJ9cybZRd+\n" +
                        "G8azmpBBotmKsbtbAU/I/LVk8saeXznshOVVpDRYtVnjZeAneso7\n" +
                        "-----END CERTIFICATE-----\n" +
                        "--START INTERMEDIATE CERT--\n" +
                        "-----BEGIN CERTIFICATE-----\n" +
                        "MIIFEjCCA/qgAwIBAgICEAAwDQYJKoZIhvcNAQELBQAwgawxCzAJBgNVBAYTAlVT\n" +
                        "MQswCQYDVQQIDAJOWTESMBAGA1UEBwwJQ2FuYXN0b3RhMRswGQYDVQQKDBJRWiBJ\n" +
                        "bmR1c3RyaWVzLCBMTEMxGzAZBgNVBAsMElFaIEluZHVzdHJpZXMsIExMQzEZMBcG\n" +
                        "A1UEAwwQcXppbmR1c3RyaWVzLmNvbTEnMCUGCSqGSIb3DQEJARYYc3VwcG9ydEBx\n" +
                        "emluZHVzdHJpZXMuY29tMB4XDTE1MDMwMjAwNTAxOFoXDTM1MDMwMjAwNTAxOFow\n" +
                        "gZgxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJOWTEbMBkGA1UECgwSUVogSW5kdXN0\n" +
                        "cmllcywgTExDMRswGQYDVQQLDBJRWiBJbmR1c3RyaWVzLCBMTEMxGTAXBgNVBAMM\n" +
                        "EHF6aW5kdXN0cmllcy5jb20xJzAlBgkqhkiG9w0BCQEWGHN1cHBvcnRAcXppbmR1\n" +
                        "c3RyaWVzLmNvbTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBANTDgNLU\n" +
                        "iohl/rQoZ2bTMHVEk1mA020LYhgfWjO0+GsLlbg5SvWVFWkv4ZgffuVRXLHrwz1H\n" +
                        "YpMyo+Zh8ksJF9ssJWCwQGO5ciM6dmoryyB0VZHGY1blewdMuxieXP7Kr6XD3GRM\n" +
                        "GAhEwTxjUzI3ksuRunX4IcnRXKYkg5pjs4nLEhXtIZWDLiXPUsyUAEq1U1qdL1AH\n" +
                        "EtdK/L3zLATnhPB6ZiM+HzNG4aAPynSA38fpeeZ4R0tINMpFThwNgGUsxYKsP9kh\n" +
                        "0gxGl8YHL6ZzC7BC8FXIB/0Wteng0+XLAVto56Pyxt7BdxtNVuVNNXgkCi9tMqVX\n" +
                        "xOk3oIvODDt0UoQUZ/umUuoMuOLekYUpZVk4utCqXXlB4mVfS5/zWB6nVxFX8Io1\n" +
                        "9FOiDLTwZVtBmzmeikzb6o1QLp9F2TAvlf8+DIGDOo0DpPQUtOUyLPCh5hBaDGFE\n" +
                        "ZhE56qPCBiQIc4T2klWX/80C5NZnd/tJNxjyUyk7bjdDzhzT10CGRAsqxAnsjvMD\n" +
                        "2KcMf3oXN4PNgyfpbfq2ipxJ1u777Gpbzyf0xoKwH9FYigmqfRH2N2pEdiYawKrX\n" +
                        "6pyXzGM4cvQ5X1Yxf2x/+xdTLdVaLnZgwrdqwFYmDejGAldXlYDl3jbBHVM1v+uY\n" +
                        "5ItGTjk+3vLrxmvGy5XFVG+8fF/xaVfo5TW5AgMBAAGjUDBOMB0GA1UdDgQWBBSQ\n" +
                        "plC3hNS56l/yBYQTeEXoqXVUXDAfBgNVHSMEGDAWgBQDRcZNwPqOqQvagw9BpW0S\n" +
                        "BkOpXjAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQAJIO8SiNr9jpLQ\n" +
                        "eUsFUmbueoxyI5L+P5eV92ceVOJ2tAlBA13vzF1NWlpSlrMmQcVUE/K4D01qtr0k\n" +
                        "gDs6LUHvj2XXLpyEogitbBgipkQpwCTJVfC9bWYBwEotC7Y8mVjjEV7uXAT71GKT\n" +
                        "x8XlB9maf+BTZGgyoulA5pTYJ++7s/xX9gzSWCa+eXGcjguBtYYXaAjjAqFGRAvu\n" +
                        "pz1yrDWcA6H94HeErJKUXBakS0Jm/V33JDuVXY+aZ8EQi2kV82aZbNdXll/R6iGw\n" +
                        "2ur4rDErnHsiphBgZB71C5FD4cdfSONTsYxmPmyUb5T+KLUouxZ9B0Wh28ucc1Lp\n" +
                        "rbO7BnjW\n" +
                        "-----END CERTIFICATE-----\n");
        });

        var privateKey = "-----BEGIN PRIVATE KEY-----\n" +
                        "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC9IJUXbPtG1f9p\n" +
                        "kQTeu8tXZPrTIWieCYsKsDM6iKHKqbyU1mU+BPSj5DchdMEBuKZCvO15j4FoOPlS\n" +
                        "tdGmdypBsRPygiaLssu/r/w+cLCCotPyraEQub7Y35STuMn6l8S/4Y6aiQJ49WwK\n" +
                        "aEVKxUOPrLDsril3hcrAH/Xe5yR4Ow7uuJCD0uc0CwLQoYlm1TXSLpBSyW8FFudo\n" +
                        "R+Oir00jilxX4axATVdMbYkp8xBElPteqMZSfxFNE/k5Hhz21PSx3zgu6tl+pBvT\n" +
                        "0zqwRtHvQDyjCMgIeKv+P7uSithdHzvsSUippvDDpXYx5ZN0aPGy3AlE1gCJIFWM\n" +
                        "4rpU5jhHAgMBAAECggEAR/+AYoOdgRD0g8auWPCa73FEv11ni0pMwUvkpLLhDNrj\n" +
                        "LLIVpq+SRlYZ2sWsuDAIMz3jFbTY6clOuPr4jeutjYaQ9nAvplzxjc68MzUJLSI5\n" +
                        "l5gbydc8XSl4UWg2lAx03StL826XH18muyuIvXctSiPO0rtWk+a+EbfZDPtnedEy\n" +
                        "AMYw+X4xc9bnzpIsMwZV/m0k+S/IHXlgsAwd/dI6+m8XlV++lsi6V4tTOjE0VpgE\n" +
                        "1zHswwXw71p1YpqSETK7lglXIxOcWW1CTJHc4r1hWnL08UJLRMnSL7klTBmSGk+C\n" +
                        "2EW6WbR7VHs8ynE0bwFpaVR7daMylDFU9aQCKdwAGQKBgQDphU+mQu8PiWLN4bZu\n" +
                        "NsQYKRbYlZ0dPjtF9kTpcqR16doiJO/IlzZW24ttgBHW7MxFiXr2qQ1VcNKNdvyf\n" +
                        "lWZbFqJf5eZ1jGz4e9vFMpQCXMehM/6rpM6JSiz+o6ouEulRO5PWrzEWWNfC+48t\n" +
                        "P82vl3sqdj405lUQTvG/cRX3EwKBgQDPVUagJvq8MqGaNCtskLBF4sSW/jBb8Vfo\n" +
                        "zkep4EBFfiYLDjUWqYR+rawjTXxlBpiKf6Vu/oKs0JhYDoN+zImmahasI28W6BzF\n" +
                        "MOlwFmtiLp5qKJXj4ar8yz/Nx2+ZECCfrMCfjZAUbDF0O+OCRFksbaGRU1JWRHIX\n" +
                        "VbdbTY+cfQKBgQDbpjVBIfxTFNsG5KQADytrvlpq5m+eLgb6y6YOH3NqJci0DSIt\n" +
                        "/21xTZhcKvacvqd1UNeEY4bTMwgZYC5GvPRGAYhBw7F+J9Pn9jjFx8lIX5S2oH2P\n" +
                        "XbmXkVm5OqC8LbWgXvW0HZ2Cc5afJJeL7FPAMi/7xLTGdVDVwivJ8sjCWQKBgC7V\n" +
                        "gIKky3a/qSmAJ176wBbJRsck3B+owMbh8eY5tcr81/MfXnakDD3hVUUUN8lKWCLQ\n" +
                        "+rm73CDVbdRWUvXNljp1NHHl/y4ZeIZCqjibl94x0a8BQ/qZ+1nnP55oMplMv1HS\n" +
                        "YCCqhaVXW0R6UsYWUl5q1VhSjgLWqz8CKc0CFDKxAoGBAOgpTFooFDELLDfw6L68\n" +
                        "swKcgCURK+4SAfn+bPQjpP8vvD6G9TbAmD8yjdx6X1i8hyLPzbkWsnn5vtV1mtkd\n" +
                        "WITTJ1VJ6nQGktb4CAaTT4vbRsj78Q5F2Ofy3PIE6newDuIMVuqBJWaoqGeQlDRd\n" +
                        "R2SvdnEArwwKS26tAMCDuB+U\n" +
                        "-----END PRIVATE KEY-----";

        qz.security.setSignaturePromise(function(toSign) {
            return function(resolve, reject) {
                try {
                    var pk = KEYUTIL.getKey(privateKey);
                    var sig = new KJUR.crypto.Signature({"alg": "SHA1withRSA"});
                    sig.init(pk);
                    sig.updateString(toSign);
                    var hex = sig.sign();
                    console.log("DEBUG: \n\n" + stob64(hextorstr(hex)));
                    resolve(stob64(hextorstr(hex)));
                } catch (err) {
                    console.error(err);
                    reject(err);
                }
            };
        });


        qz.websocket.connect().then(function() {
            console.log("Conectado!");
            conectado = true;
        }).catch(function(e) { 
            console.error(e); 
        });
        
}     
                

    function configuracionImpresora() {
        var ip = document.getElementById("ip").value;
        var port = document.getElementById("port").value;
        var config = qz.configs.create( {host:ip,port:port});
        var data = ['INPUT ON\n' +
                        'LAYOUT INPUT "/tmp/nut72.lay"\n' +
                        'CLIP ON\n' +
                        'CLIP BARCODE ON\n' +
                        'LBLCOND 3,2\n' +
                        'CLL\n' +
                        'OPTIMIZE "BATCH" ON\n' +
                        'PP330,250:AN2\n' +
                        'DIR3\n' +
                        'NASC 8\n' +
                        'FT "Monospace 821 Bold BT"\n' +
                        'FONTSIZE 12\n' +
                        'FONTSLANT 0\n' +
                        'PT VAR1$\n' +
                        'PP330,282:PT VAR2$\n' +
                        'PP610,296:FT "Monospace 821 BT"\n' +
                        'FONTSIZE 5\n' +
                        'FONTSLANT 0\n' +
                        'AN8\n' +
                        'PP330,277:PT VAR3$\n' +
                        'PP330,299:PT VAR4$\n' +
                        'PP330,321:PT VAR5$\n' +
                        'PP330,343:PT VAR6$\n' +
                        'PP330,365:PT VAR7$\n' +
                        'PP330,387:PT VAR8$\n' +
                        'PP330,411:FT "Monospace 821 Bold BT"\n' +
                        'FONTSIZE 8\n' +
                        'FONTSLANT 0\n' +
                        'PT VAR9$\n' +
                        'PP46,533:AN1\n' +
                        'DIR1\n' +
                        'PX229,190,3\n' +
                        'PP253,762:DIR2\n' +
                        'PL3,1\n' +
                        'PP395,433:AN7\n' +
                        'DIR3\n' +
                        'FONTSIZE 6\n' +
                        'PT VAR10$\n' +
                        'PP207,433:PT VAR11$\n' +
                        'PP395,450:PT VAR12$\n' +
                        'PP208,450:PT VAR13$\n' +
                        'PP395,466:PT VAR14$\n' +
                        'PP208,466:PT VAR15$\n' +
                        'PP395,482:PT VAR16$\n' +
                        'PP208,482:PT VAR17$\n' +
                        'PP395,498:PT VAR18$\n' +
                        'PP208,498:PT VAR19$\n' +
                        'PP315,512:PT VAR20$\n' +
                        'PP208,512:PT VAR21$\n' +
                        'PP49,559:AN1\n' +
                        'DIR1\n' +
                        'PL185,3\n' +
                        'PP220,539:AN7\n' +
                        'DIR3\n' +
                        'FT "Monospace 821 BT",5,0,59\n' +
                        'PT "Informació nutricional per 100g"\n' +
                        'PP144,760:AN1\n' +
                        'DIR2\n' +
                        'PL200,3\n' +
                        'PP48,592:DIR1\n' +
                        'PL186,3\n' +
                        'PP48,647:PL186,3\n' +
                        'PP48,711:PL186,3\n' +
                        'PP48,736:PL186,3\n' +
                        'PP144,675:PL90,3\n' +
                        'PP144,617:PL90,3\n' +
                        'PP231,562:AN7\n' +
                        'DIR3\n' +
                        'FT "Monospace 821 BT"\n' +
                        'FONTSIZE 5\n' +
                        'FONTSLANT 0\n' +
                        'PT "Valor"\n' +
                        'PP231,575:PT "energetic"\n' +
                        'PP143,564:FT "Monospace 821 Bold BT"\n' +
                        'FONTSIZE 6\n' +
                        'FONTSLANT 0\n' +
                        'PT VAR22$\n' +
                        'PP231,596:FT "Monospace 821 BT"\n' +
                        'FONTSIZE 5\n' +
                        'FONTSLANT 0\n' +
                        'PT "Greixos"\n' +
                        'PP231,617:PT "Dels quals"\n' +
                        'PP231,630:PT "saturats"\n' +
                        'PP135,593:FT "Monospace 821 Bold BT"\n' +
                        'FONTSIZE 6\n' +
                        'FONTSLANT 0\n' +
                        'PT VAR23$\n' +
                        'PP134,620:PT VAR24$\n' +
                        'PP231,648:FT "Monospace 821 BT"\n' +
                        'FONTSIZE 5\n' +
                        'FONTSLANT 0\n' +
                        'PT "Hidrats de"\n' +
                        'PP231,661:PT "carboni"\n' +
                        'PP133,651:FT "Monospace 821 Bold BT"\n' +
                        'FONTSIZE 6\n' +
                        'FONTSLANT 0\n' +
                        'PT VAR25$\n' +
                        'PP231,678:FT "Monospace 821 BT"\n' +
                        'FONTSIZE 5\n' +
                        'FONTSLANT 0\n' +
                        'PT "Dels quals"\n' +
                        'PP231,691:PT "sucres"\n' +
                        'PP133,682:FT "Monospace 821 Bold BT"\n' +
                        'FONTSIZE 6\n' +
                        'FONTSLANT 0\n' +
                        'PT VAR26$\n' +
                        'PP231,717:FT "Monospace 821 BT"\n' +
                        'FONTSIZE 5\n' +
                        'FONTSLANT 0\n' +
                        'PT "Proteïnes"\n' +
                        'PP133,713:FT "Monospace 821 Bold BT"\n' +
                        'FONTSIZE 6\n' +
                        'FONTSLANT 0\n' +
                        'PT VAR27$\n' +
                        'PP231,739:FT "Monospace 821 BT"\n' +
                        'FONTSIZE 5\n' +
                        'FONTSLANT 0\n' +
                        'PT "Sal"\n' +
                        'PP133,735:FT "Monospace 821 Bold BT"\n' +
                        'FONTSIZE 6\n' +
                        'FONTSLANT 0\n' +
                        'PT VAR28$\n' +
                        'PP590,539:FT "Monospace 821 BT"\n' +
                        'FONTSIZE 9\n' +
                        'PT VAR29$\n' +
                        'PP590,563:FT "Monospace 821 Bold BT"\n' +
                        'FONTSIZE 10\n' +
                        'PT VAR30$\n' +
                        'PP405,539:FT "Monospace 821 BT"\n' +
                        'FONTSIZE 9\n' +
                        'PT VAR31$\n' +
                        'PP405,563:FT "Monospace 821 Bold BT"\n' +
                        'FONTSIZE 10\n' +
                        'PT VAR32$\n' +
                        'an8\n' +
                        'PP405,604:FT "Monospace 821 BT"\n' +
                        'FONTSIZE 10\n' +
                        'FONTSLANT 0\n' +
                        'PT VAR33$\n' +
                        'PP405,634:FT "Monospace 821 Bold BT"\n' +
                        'FONTSIZE 16\n' +
                        'FONTSLANT 0\n' +
                        'PT VAR34$\n' +
                        'an7\n' +
                        'PP340,755:FT "Monospace 821 BT"\n' +
                        'FONTSIZE 7\n' +
                        'FONTSLANT 0\n' +
                        'PT VAR36$\n' +
                        'PP340,712:PT VAR35$\n' +
                        'PP445,692:PT "DATA ENVASAT:"\n' +
                        'PP445,736:PT "DATA CADUCITAT:"\n' +
                        'LAYOUT END\n' +
                        'COPY "/tmp/nut72.lay","/c/nut72.lay"'];
           
        if(conectado){
            qz.print(config, data).catch(function(e) { console.error(e); });
            console.log(data);
        }else{
            errorImpresora();
        }
}           
                
                
                
                
function imprimirEtiqueta(value,texto,LLETRA,LLETRA_BOLD) {
    value.ing1 = formatEtiqueta(value.ing1,LLETRA,LLETRA_BOLD);
    value.ing2 = formatEtiqueta(value.ing2,LLETRA,LLETRA_BOLD);
    value.ing3 = formatEtiqueta(value.ing3,LLETRA,LLETRA_BOLD);
    value.ing4 = formatEtiqueta(value.ing4,LLETRA,LLETRA_BOLD);
    value.ing5 = formatEtiqueta(value.ing5,LLETRA,LLETRA_BOLD);
    value.ing6 = formatEtiqueta(value.ing6,LLETRA,LLETRA_BOLD);
    var blanc = (90 - value.conservacio.length) / 2;
    var cadenaBlanc = "";
    for (var k = 0; k < blanc; k++) {
        cadenaBlanc += " ";
    }
    value.conservacio = cadenaBlanc + value.conservacio;
        var ip = getCookie("ip");
        var port = getCookie("port");
        var config = qz.configs.create( {host:ip,port:port});
        var layout = 'INPUT OFF\n'+
                    'FORMAT INPUT "[","]","^"\n'+
                    'INPUT ON\n'+
                    'LAYOUT RUN ""\n'+
                    'LAYOUT RUN \n'+
                    'CLIP ON \n' +
                    'CLIP BARCODE ON \n' + 
                    'LBLCOND 3,2 CLL \n' +
                    'OPTIMIZE "BATCH" ON \n' +
                    'PP330,250:AN2 \n' +
                    'DIR3 \n' +
                    'NASC 8 \n' +
                    'FT "Swiss 721 Bold BT",12 \n' +
                    '\'titulo \n' +
                    'PT "' + value.titol + '" \n' +
                    'PP340,285:AN2 \n' +
                    'DIR3 \n' +
                    'NASC 8 \n' +
                    'FT "Swiss 721 Bold BT",11 \n' +
                    '\'titulo \n' +
                    'PT "' + value.subtitol + '" \n' +
                    //'PP600,296:FT "Swiss 721 BT",5 \n' +
                    'PP800,496:FT "Swiss 721 BT",5 \n' +
                    'AN 7 \n' +
                    '\'Ingredientes \n' +
                    'PP640,297' + value.ing1 + ' \n' +
                    'PP640,314' + value.ing2 + ' \n' +
                    'AN 7 \n' +
                    'PP640,331' + value.ing3 + ' \n' +
                    'PP640,348' + value.ing4 + ' \n' +
                    'PP640,365' + value.ing5 + ' \n' +
                   // 'PP610,387:PT "' + value.ing6 + '" \n' +
                    'PP400,384:FT "Swiss 721 BT",7:PT "' + value.senseGluten + '" \n' +
                    '\'Conservación "CONSERVAR ENTRE 0 I 4º C" o "Congelado -16" \n' +
                    'PP610,406:FT "Swiss 721 BT",7:PT "' + value.conservacio + '" \n';
            
                    
            
            

                    
                    if (!value.hiddenTV) {

                        // Tracabilitat
                    layout +=     '\'Jugamos con la visibilidad \n' +
                        '"VARIABLE_1" \n' + 
                        'PP395,433:FT "Swiss 721 BT",5 :PT "' + value.dibAnimalTitle + '" \n' + 
                        'PP208,433:PT "' + value.dibAnimal + '" \n' +
                        '"VARIABLE_2" \n' + 
                        'PP395,450:PT "' + value.naixementTitle + '" \n' +
                        'PP208,450:PT "' + value.naixement + '" \n' + 
                        '"VARIABLE_3" \n' +
                        'PP395,466:PT "' + value.engreixTitle + '" \n' +
                        'PP208,466:PT "' + value.engreix + '" \n' +
                        '"VARIABLE_4" \n' + 
                        'PP395,482:PT "' + value.sacrificiTitle + '" \n' +
                        'PP208,482:PT "' + value.sacrifici + '" \n' +
                        '"VARIABLE_5" \n' + 
                        'PP395,498:PT "' + value.dataSacrificiTitle + '" \n' +
                        'PP208,498:PT "' + value.dataSacrifici + '" \n' +
                        '"VARIABLE_6" \n' + 
                        'PP315,512:PT "' + value.especejatTitle + '" \n' + 
                        'PP208,512:PT "' + value.especejat + '" \n' ;
                    } else {
                         layout += 'PP315,512:PT "' + value.lotTitle + '" \n' + 
                        'PP208,512:PT "' + value.lot + '" \n' ;
                    }
                    
                    
                    
                   
                    if (!value.hiddenIN) {
                        // Recuadre informació nutricional
                        layout +=  'PP46,533:AN1 \n' + 
                        'DIR1 \n' + 
                        'PX229,190,3 \n' + 
                        'PP253,762:DIR2 \n' +
                        'PL3,1 \n' + 
                        'PP395,433:AN7 \n' + 
                        'DIR3 \n' + 
                        'FONTSIZE 6 \n';
                        // Informació nutricional
                        layout += 'PP49,559:AN1 \n' +
                        'DIR1 \n' + 
                        'PL185,3 \n' +
                        'PP220,539:AN7 \n' +
                        'DIR3 \n' + 
                        'FT "Swiss 721 BT",5,0,59 \n' + 
                        'PT "Informació nutricional per 100g" \n' +
                        'PP144,760:AN1 \n' +
                        'DIR2 \n' + 
                        'PL200,3 \n' +
                        'PP48,592:DIR1 \n' +
                        'PL186,3 \n' + 
                        'PP48,647:PL186,3 \n' +
                        'PP48,711:PL186,3 \n' +
                        'PP48,736:PL186,3 \n' + 
                        'PP144,675:PL90,3 \n' + 
                        'PP144,617:PL90,3 \n' + 
                        'PP231,562:AN7 \n' + 
                        'DIR3 \n' +
                        'FT "Swiss 721 BT",5 \n' +
                        'PT "Valor" \n' +
                        'PP231,575:PT "energetic" \n' +
                        'PP135,564:FT "Swiss 721 Bold BT",6 \n' +
                        'PT "' + value.valorEnergetic + ' kcal" \n' + 
                        'PP231,596:FT "Swiss 721 BT",5 \n' +
                        'PT "Greixos" \n' +
                        'PP231,617:PT "Dels quals" \n' +
                        'PP231,630:PT "saturats" \n' +
                        'PP135,593:FT "Swiss 721 Bold BT",6 \n' + 
                        'PT "' + value.greixos + ' g" \n' +
                        'PP135,620:PT "' + value.saturats + ' g" \n' +
                        'PP231,648:FT "Swiss 721 BT",5 \n' +
                        'PT "Hidrats de" \n' +
                        'PP231,661:PT "carboni" \n' + 
                        'PP135,651:FT "Swiss 721 Bold BT",6 \n' +
                        'PT "' + value.hidrats + ' g" \n' + 
                        'PP231,678:FT "Swiss 721 BT",5 \n' +
                        'PT "Dels quals" \n' +
                        'PP231,691:PT "sucres" \n' +
                        'PP135,682:FT "Swiss 721 Bold BT",6 \n' +
                        'PT "' + value.sucres + ' g" \n' +
                        'PP231,717:FT "Swiss 721 BT",5 \n' +
                        'PT "Proteïnes" \n' +
                        'PP135,713:FT "Swiss 721 Bold BT",6 \n' +
                        'PT "' + value.proteines + ' g" \n' +
                        'PP231,739:FT "Swiss 721 BT",5 \n' +
                        'PT "Sal" \n' +
                        'PP135,735:FT "Swiss 721 Bold BT",6 \n' +
                        'PT "' + value.sal + ' g" \n';
                    }
                    
                    
                    layout += 'PP590,539:FT "Swiss 721 BT",9 \n' +
                    '\'Mostramos texto pes\n' +
                    'PT "' + value.textPes + '" \n' +
                    'PP590,563:FT "Swiss 721 Bold BT",10 \n' +
                    '\'peso \n' +
                    'PT "' + value.pes + '" \n' +
                    'PP405,539:FT "Swiss 721 BT",9 \n' +
                    '\'Mostramos "preu:" \n' + 
                    'PT "' + value.textPreu + '" \n' +
                    'PP405,563:FT "Swiss 721 Bold BT",10 \n' +
                    '\'Precio por kilo \n' +
                    'PT "' + value.preu + '" \n' + 
                    'an8 \n' +
                    'PP405,594:FT "Swiss 721 BT",10 \n' +
                    '\'Mostramos texto "IMPORT:" o "PES:" \n' +
                    'PT "' + value.textPreuGran + '" \n' +
                    'PP405,624:FT "Swiss 721 Bold BT",16 \n' +
                    '\'IMPORTE O PESO \n' +
                    'PT "' + value.preuGran + '" \n' +
                    'an7 \n' +
                    'PP360,742:FT "Swiss 721 BT",7 \n' +
                    '\Fecha envasado \n' +
                    'PT "' + value.dataCaducitat + '" \n' +
                    '\'Fecha caducidad \n' +
                    'PP360,707:PT "' + value.dataEnvasat + '" \n' +
                    'PP445,692:FT "Swiss 721 BT",5 :PT "DATA ENVASAT:" \n' +
                    'PP445,730:PT "DATA CADUCITAT:" \n' +
                    'PP600,700:PM "' + value.imatgeEtiqueta + '.PCX"\n' +
        'PF';
        var layoutBo = [layout];
//        alert("layout" + layout)
//        var data = [ 'INPUT OFF\n'+
//                    'FORMAT INPUT "[","]","^"\n'+
//                    'INPUT ON\n'+
//                    'LAYOUT RUN ""\n'+
//                    'LAYOUT RUN "/c/' + layout +'.lay"\n'+
//                    '[' + texto +
//                    ']\n'+
//                            'PF' ];

        if(conectado){
            qz.print(config, layoutBo).catch(function(e) { console.error(e); });
            console.log(data);
        }else{
            errorImpresora();
        }
//        qz.print(config, layoutBo).catch(function(e) { console.error(e); });
    
//       qz.append("A37,503,0,1,2,3,N,QZ-PRINT TEST PRINT\n");
//       qz.print();
       

//        console.log(layout);
}

function errorImpresora(){
    dhtmlx.message({type: "error", text: "Error al imprimir", expire: "0 "});
}

function imprimirRuta(value,texto,LLETRA,LLETRA_BOLD) {
        var ip = getCookie("ip");
        var port = getCookie("port");
        var config = qz.configs.create( {host:ip,port:port});
        var layout = 'INPUT OFF\n'+
                    'FORMAT INPUT "[","]","^"\n'+
                    'INPUT ON\n'+
                    'LAYOUT RUN ""\n'+
                    'LAYOUT RUN \n'+
                    'CLIP ON \n' +
                    'CLIP BARCODE ON \n' + 
                    'LBLCOND 3,2 CLL \n' +
                    'OPTIMIZE "BATCH" ON \n' +
                    'PP330,250:AN2 \n' +
                    'DIR3 \n' +
                    'NASC 8 \n' +
                    'PP330,250:FT "Swiss 721 BT",10 :PT "RUTA" \n' +
                    'PP330,320:FT "Swiss 721 Bold BT",20 :PT "' + value.ruta + '" \n' +
                    //'PP330,400 :FT "Swiss 721 Bold BT",12 :PT "CLIENT:" \n' +
                    'PP330,430:FT "Swiss 721 BT",15 :PT "' + value.client1 + '" \n' +
                    'PP330,470:FT "Swiss 721 BT",15 :PT "' + value.client2 + '" \n' +
                    //'PP330,500 :FT "Swiss 721 Bold BT",12 :PT "DATA:" \n' +
                    'PP330,570:FT "Swiss 721 BT",10 :PT "' + value.data + '" \n' +
                    //'PP330,600 :FT "Swiss 721 Bold BT",12 :PT "TELÈFON:" \n' +
                    'PP330,620:FT "Swiss 721 BT",10 :PT "' + value.telefon + '" \n' +
                    //'PP330,700 :FT "Swiss 721 Bold BT",12 :PT "DIRECCIÓ:" \n' +
                    'PP330,670:FT "Swiss 721 BT",10 :PT "' + value.direccio1 + '" \n' +
                    'PP330,700:FT "Swiss 721 BT",10 :PT "' + value.direccio2 + '" \n' +
        'PF';
        var layoutBo = [layout];

       qz.print(config, layoutBo).catch(function(e) { console.error(e); });

        console.log(layout);
}



function formatEtiqueta(value,LLETRA,LLETRA_BOLD) {
    var ingArr = value.split(" ");
    value = "";
    var blanc = "";
    for (var i = 0; i < ingArr.length; i++) {
        if (ingArr[i] == "") {
            blanc += " ";
        } else if (ingArr[i].includes("<b>")) {
            value += LLETRA_BOLD + " \" " +  blanc + ingArr[i].replace(/<b>|<\/b>|<div>|<\/div>|<br>/g,"") + "\" ";
            blanc = "";
        } else {
            value += LLETRA + " \" " +  blanc + ingArr[i].replace(/<b>|<\/b>|<div>|<\/div>|<br>/g,"") + "\" ";
            blanc = "";
        }
    }
    return value;
}


function imprimirEtiquetaOld(layout,texto) {
        var ip = getCookie("ip");
        var port = getCookie("port");
        var config = qz.configs.create( {host:ip,port:port});
        var data = [ 'INPUT OFF\n'+
                    'FORMAT INPUT "[","]","^"\n'+
                    'INPUT ON\n'+
                    'LAYOUT RUN ""\n'+
                    'LAYOUT RUN "/c/' + layout +'.lay"\n'+
                    '[' + texto +
                    ']\n'+
                            'PF' ];
        qz.print(config, data).catch(function(e) { console.error(e); });
        console.log(data);
}


                
                
                
                
                
                
                
                
                
function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
  }