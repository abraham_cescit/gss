<Resource
      	name="jdbc/gssDS"
      	type="javax.sql.DataSource"
      	maxTotal="30"
      	maxIdle="30"
      	maxWaitMillis="60000"
      	url="jdbc:mysql://127.0.0.1:3306/gss?useEncoding=true&amp;characterEncoding=UTF-8&amp;serverTimezone=Europe/Madrid"
      	driverClassName="com.mysql.cj.jdbc.Driver"
      	poolPreparedStatements="true"
      	username="root"
      	password=""
      	testOnBorrow="true"
      	validationQuery="SELECT 1"
      	removeAbandoned="true"/>	