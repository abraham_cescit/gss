/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.commons;

import controladors.base.CITServlet;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Abraham M
 */
public class ImatgesRegistresGetController extends CITServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String resposta = "ERROR";
        try {
            if (inicialitzar(request, response, this, false)) {
                request.setCharacterEncoding("UTF-8");

                //addDiccionari("albarans");
                try {

                    String nomCarpeta = util.commons.Utils.getProperty("​CARPETA_IMATGES_REGISTRES");
                    String nomArxiu = Utils.getParameterString(request, "a");

                    //attachment: el fichero se ofrece para descargar
                    //inline: se solicita al navegador que lo abra él mismo si tiene el plugin adecuado
                    //o puede mostrar el tipo de archivo

                    String fitxer = nomCarpeta + nomArxiu;
                    MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();
                    response.setContentType(mimetypesFileTypeMap.getContentType(fitxer));
                    response.getOutputStream().write(Files.readAllBytes(Paths.get(fitxer)));

                    super.processRequest(request, response);

                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                super.processRequest(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ImatgesRegistresGetController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}